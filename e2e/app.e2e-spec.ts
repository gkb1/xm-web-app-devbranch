import { XMPage } from "./app.po";


describe('xm App', () => {
  let page: XMPage;

  beforeEach(() => {
    page = new XMPage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
