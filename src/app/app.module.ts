import { CommonService } from './services/common.service';
import {BrowserModule, HAMMER_GESTURE_CONFIG} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RoutesModule} from './app.router';
import {AuthGuard} from './security/auth.guard';
import {WindowRefService} from './services/window-ref.service';
import {HmacHttpClient} from './utils/app-token-http-client';
import {Http, XHRBackend, RequestOptions, HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, FormControl, ReactiveFormsModule} from '@angular/forms';
import { ConfirmationComponent } from 'app/components/shared/confirmation/confirmation.component';
import { NotificationComponent } from 'app/components/shared/notification/notification.component';
import { LoadingBarComponent } from 'app/components/shared/loading-bar/loading-bar.component';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { ConfirmationService } from 'app/components/shared/confirmation/confirmation.service';
import { AuthenticationModule } from 'app/components/authentication/authentication.module';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { HomeComponent } from 'app/components/customer/home/home.component';
import { MatTabsModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { AlertModule, CarouselModule } from 'ngx-bootstrap';
import {MatFormFieldModule} from '@angular/material/form-field';
import { ProductListComponent } from 'app/components/customer/products/product-list/product-list.component';
import { RoleGuardService } from 'app/security/role-guard.service';
import { CustomerService } from './components/customer/customer-service';
import { MatButtonModule } from '@angular/material/button';
import { ProductDetailComponent } from 'app/components/customer/products/product-detail/product-detail.component';
import { HeaderComponent } from './components/customer/header/header.component';
import { CustomMaterailModule } from './custom-materail/custom-materail.module';
import { SideNavComponent } from 'app/components/customer/side-nav/side-nav.component';
import { CustomerModule } from 'app/components/customer/customer.module';
import { CustomerSharedModule } from 'app/components/customer/customer-shared.module';
import { MyHammerConfig } from 'app/utils/my-hammer.config';

export function httpFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions) {
  return new HmacHttpClient(xhrBackend, requestOptions);
  // return new HmacHttpClient(xhrBackend, requestOptions);
}

@NgModule({
  declarations: [
    AppComponent,
    ConfirmationComponent,
    NotificationComponent,
    LoadingBarComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    HttpModule,
    RoutesModule,
    FormsModule,
    ReactiveFormsModule,
    MatTabsModule,
    HttpClientModule,
    AlertModule.forRoot(),
    MatFormFieldModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    CustomMaterailModule,
    CarouselModule.forRoot(),
    CustomerSharedModule
  ],
  providers: [
    {
      provide: Http,
      useFactory: httpFactory,
      deps: [XHRBackend, RequestOptions],
      multi: false
    },
    {
      provide: HAMMER_GESTURE_CONFIG,
      useClass: MyHammerConfig
    },
    AuthGuard,
    RoleGuardService,
    WindowRefService,
    NotificationService,
    LoadingBarService,
    ConfirmationService,
    CommonService,
    AuthenticationService,
    // CustomerService,
  ],
  exports: [ 
    CustomMaterailModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
