import { environment } from '../../environments/environment';
import { getHtmlTagDefinition } from '@angular/compiler';

// Local storage keys
export const STORAGE_ACCOUNT_TOKEN = 'xm-account';
export const STORAGE_USER_DETAILS = 'xm-user-details';
export const STORAGE_SECURITY_TOKEN = 'xm-security';

// Common http root api
export const BACKEND_API_ROOT_URL: string = environment.url;
export const GET_ALL_USERS_LIST_BY_CONDN = 'users/getList';
export const UPDATE_USER_STATUS = 'users/activateUser/';
export const GET_SEL_USER_BY_COND = 'users/getSelUserByCond';

// product - URL
export const GET_ALL_PRODUCTS_URL = 'product/getAllByCond';
export const APPROVAL_REJECT = 'product/approvOrReject/';
export const CREATE_PRDT_URL = 'product/add';
export const GET_SEL_PRDT_DETAILS_BY_PRDTID_URL = 'product/getByPrdtId/';
export const UPDATE_SEL_PRDTID_URL = 'product/update/';
export const GET_SEL_PRDT_BY_FRAMEID_SEARCH_URL = 'product/getByPrdtframeId/';
export const ADD_SEL_PRDT_ATTR_TO_STORE_URL = 'product/updateProductAttribute';
export const REMOVE_SEL_STORE_PRDT_ATTR_URL = 'product/removeStore';
export const GET_SEL_PRDT_BY_TAGNO_SEARCH_URL = 'product/searchByTagNo/';
export const TRIGGER_S3_FOLDER_UPLOAD_URL = 'file-upload/bucket/readS3Folder';
export const  VERIFY_GENERATE_SKU_URL = 'product/generateSKU/';
export const UPDATE_GKB_PRICE_PRDTID_URL = 'product/updateGKBPrice/';

// brand - URL
export const GET_BRANDS_BY_CONDN_URL = 'brand/getAllBrandsByCond';
export const GET_BRAND_BY_NAME_AND_ID = 'brand/getBrandByNameAndId';
export const UPDATE_BRAND_BY_NAME_AND_ID = 'brand/updateBrand';
export const DELETE_BRAND_BY_NAME_AND_ID = 'brand/deleteBrand';
export const CREATE_BRAND = 'brand/create';
export const PRDT_IMAGE_UPLOAD_URL = 'file-upload/s3-upload';

// category - URL
export const GET_ALL_CAT_WITH_TRENDING_URL = 'category/getAllCategoriesWithTrendingCat';
export const CREATE_CATEGORY = 'category/create';
export const GET_ALL_CATEGORIES_URL = 'category/getAllCategories';
export const GET_CAT_BY_CATID_URL = 'category/getByCategoryId/';

// Frame collector - URL
export const ADD_STORE_TO_FRAME_COLLECTOR = 'users/assignStore';
export const DELETE_STORE_FROM_FRAME_COLLECTOR = '/users/deleteStore/';
export const GET_ALL_FRAME_COLLECTORS_BY_STORE = 'frame/getAllFrames';
export const CREATE_FRAME_ID = 'frame/create/';
export const GET_ALL_FRAME_IDS = 'frame/getAllFrames';
export const UPDATE_FRAME_COLLECTOR_STATUS = 'frame/updateFrame/';
export const UPDATE_SEL_ECP_ALLFRAME_STATUS = 'frame/updateAllFrameStatusOfEcp/';


// Frame shape,material,type - URL
export const GET_FRAME_INFO_URL = 'lookup/';
export const GET_FRAME_MATERIAL_LIST = 'lookup/frameMaterial';
export const GET_FRAME_SHAPE_LIST = 'lookup/frameShape';
export const GET_FRAME_TYPE_LIST = 'lookup/frameType';
export const GET_FRAME_COLOR_LIST = 'lookup/frameColour';
export const GET_FRAME_MODEL_LIST = 'lookup/frameModel';
export const GET_FRAME_PARENT_COLOUR_LIST = 'lookup/getAllParentColour';

export const CREATE_FRAME_MATERIAL = 'lookup/frameMaterial/create';
export const CREATE_FRAME_SHAPE = 'lookup/frameShape/create';
export const CREATE_FRAME_TYPE = 'lookup/frameType/create';
export const CREATE_FRAME_COLOR = 'lookup/frameColour/create';
export const CREATE_FRAME_MODEL = 'lookup/frameModel/create';

export class UrlMatcher {
  public static matches(url: string): boolean {
    const regexp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(url);
  }
}
