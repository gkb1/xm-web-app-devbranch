import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSelectModule, MatTableModule, MatListModule, MatRadioModule, MatDialogModule, 
  MatTooltipModule, MatCheckboxModule, MatInputModule, MatProgressSpinnerModule, MatTabsModule,
   MatPaginatorModule, MatSlideToggleModule, MatExpansionModule, MatButtonToggleModule, 
   MatFormFieldModule, MatIconModule, MatButtonModule, MatSidenavModule } from '@angular/material';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialDesignElementsModule } from 'app/components/shared/material-design-element/material-design-element.module';
import { InputTrimModule } from 'ng2-trim-directive';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [],
  imports: [
    CommonModule, 
    MatSelectModule,
    MatTableModule,
    MatListModule,
    MatRadioModule,
    MatDialogModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatPaginatorModule,
    ScrollingModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatButtonToggleModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialDesignElementsModule,
    InputTrimModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatSelectModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule,
    MatSidenavModule,
  ],
  exports: [
    MatSelectModule,
    MatTableModule,
    MatListModule,
    MatRadioModule,
    MatDialogModule,
    MatTooltipModule,
    MatCheckboxModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatTabsModule,
    MatPaginatorModule,
    ScrollingModule,
    MatSlideToggleModule,
    MatExpansionModule,
    MatButtonToggleModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialDesignElementsModule,
    InputTrimModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatSelectModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule,
    MatSidenavModule
  ]
})
export class CustomMaterailModule { }
