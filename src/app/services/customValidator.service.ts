import {Injectable, OnDestroy} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {FormControl} from '@angular/forms';
// import * as AppUtils from '../utils/app.utils';
// import {UniqueIdentifiers} from '../services/models/common/unique-identifiers';
import {isNullOrUndefined} from 'util';
// import {AccountEventsService} from '../account/account.events.service';
import {Subscription} from 'rxjs/Subscription';
import {CommonService} from './common.service';

// import {PersonService} from '../services/person.service';

interface IValidation {
  [key: string]: boolean;
}

@Injectable()
export class CustomValidatorService implements OnDestroy {
  private userAPI = '/userapi/';
  private personAPI = this.userAPI + 'person/';
  private infrastructureAPI = '/infrastructureapi';
  private orgURI = this.infrastructureAPI + '/organizations';
  public uniqueIdentityObj: any = {};
  private httpSubscriptionUnique: Subscription;

  constructor(private commonService: CommonService) {
  }

  public emailFormat(control: FormControl): IValidation {
    const pattern: RegExp = /\S+@\S+\.\S+/;
    return pattern.test(control.value) ? null : {'emailFormat': true};
  }

  public nameFormat(control: FormControl): IValidation {
    const pattern: RegExp = /^([a-zA-Z]+\s)*[a-zA-Z]+$/;
    if (control.value) {
      return pattern.test(control.value) ? null : { 'nameFormat': true };
    } else {
      return null;
    }
  }

  public mobileFormat(control: FormControl): IValidation {
    const pattern: RegExp = /^[0][1-9]\d{9}$|^[1-9]\d{9}$/;
    return pattern.test(control.value) ? null : {'mobileFormat': true};
  }

  public otpFormat(control: FormControl): IValidation {
    const pattern: RegExp = /^[1-9][0-9]{5}$/;
    return pattern.test(control.value) ? null : {'otpFormat': true};
  }

  public duplicated(control: FormControl) {
    const username = control.value;
    const q = new Promise<IValidation>((resolve, reject) => {
      setTimeout(() => {
        if (!isNullOrUndefined(this.uniqueIdentityObj) && !isNullOrUndefined(this.uniqueIdentityObj.username) && this.uniqueIdentityObj.username === control.value) {
          resolve(null);
        } else {
          const obj = {
            username: username
          }
          // this.httpSubscriptionUnique = this.commonService.checkUniqueIdentity(obj)
          // .subscribe(data => {
          //   if (data.data) {
          //     resolve({'duplicated': true});
          //   } else {
          //     resolve(null);
          //   }
          // }, error => {
          //   console.error(error);
          //   reject(error);
          // });
        }
      });
    });
    return q;
  }

  public checkDuplicateMobile(control: FormControl) {
    const mobile = control.value;
    const promiss = new Promise<IValidation>((resolve, reject) => {
      setTimeout(() => {
        if (!isNullOrUndefined(this.uniqueIdentityObj) && this.uniqueIdentityObj.mobile === control.value) {
          resolve(null);
        } else {
          /*this.httpSubscriptionUnique = this.personService.checkUniqueIdentity(mobile).subscribe(data => {
            if (data) {
              resolve({'duplicated': true});
            } else {
              resolve(null);
            }
          }, error => {
            console.error(error);
            reject(error);
          });*/
        }
      });
    });
    return promiss;
  }

  ngOnDestroy(): void {
    if (this.httpSubscriptionUnique) {
      this.httpSubscriptionUnique.unsubscribe();
    }
  }

}
