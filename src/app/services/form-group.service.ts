import {Injectable} from '@angular/core'
import {FormControl, FormGroup, Validators, FormBuilder} from '@angular/forms';
import * as _ from 'lodash';

export interface Validation {
    type: 'required' | 'minLength' | 'maxLength' | 'pattern' | 'custom';
    value?: any
    message?: string
  }

@Injectable()
export class FormGroupService {

    constructor(private _fb: FormBuilder) {}

    createFormGroup(model: any): any {
        const temp = {},
            toReturn = {};
        let validators = null;

        model.forEach(a => {
            validators = null;
            if (a.validation) {
                if (Array.isArray(a.validation)) {
                    validators = [];
                    _.forEach(a.validation, i => {
                        validators.push(this.setValidator(i));
                    })
                }else {
                    validators = this.setValidator(a.validation)
                }
            }
            temp[a.key] = new FormControl('', validators);
        });

        toReturn['fbGroup'] = this._fb.group(temp);
        return toReturn;
    }

    setValidator(item: Validation) {
        switch (item.type) {
            case 'required':  return Validators.required;
            case 'minLength': return Validators.minLength(item.value);
            case 'maxLength': return Validators.maxLength(item.value);
            case 'pattern': return Validators.pattern(item.value);
            case 'custom': return item.value;
        }
    }

    getError(item: Validation) {
        switch (item.type) {
            case 'required':  return 'Field is required';
            case 'minLength': return 'Please enter minimum of' + item.value + 'charecters';
            case 'maxLength': return 'Please enter maximum of' + item.value + 'charecters';
            case 'pattern': return 'Only letters and spaces are allowed';
        }
    }

}

