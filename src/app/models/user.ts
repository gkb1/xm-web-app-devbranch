import { UserRole } from 'app/models/userRole';
import { AuditField } from 'app/models/auditFields';
import * as _ from 'lodash';
import { Address } from 'app/models/address';

export class User {
    _id: number;
    firstName: string;
    lastName: string;
    email: string;
    mobile: string;
    dob: any;
    username: string;
    role: UserRole;
    address: Address;
    status: string;
    validTill: string;
    auditFields: AuditField;

    constructor(user?: {
        id, firstName, lastName, email, mobile, username, role, address,
        status, validTill, auditFields
    }) {
        if (user) {
            _.assignIn(this, user);
        }

    }
}
