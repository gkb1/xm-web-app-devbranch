import * as _ from 'lodash';

export class Media {
    name: string;
    url: string;
    uploadedAt: Date;
    uploadedBy: string;

    constructor(media?: {
        name, url, uploadedAt, uploadedBy}) {
        if (media) {
            _.assignIn(this, media);
        }
    }

}
