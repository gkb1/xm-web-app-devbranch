import * as _ from 'lodash';

export class AuditField {
     createdAt: string;
     createdBy: string;
     updatedAt: string;
     updatedBy: string;
     createdByUser: string;
     updatedByUser: string;

    constructor(auditField?: {
        createdAt, createdBy, updatedAt, updatedBy}) {
        if (auditField) {
            _.assignIn(this, auditField);
        }
    }
}
