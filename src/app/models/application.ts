import * as _ from 'lodash';
import { Media } from 'app/models/media';
import { AuditField } from 'app/models/auditFields';
import { User } from 'app/models/user';


export class Application {
    _id: number;
    title: String;
    policy_type: String;
    insured: User;
    broker: User;
    media: Media[];
    auditFields: AuditField;
    isActive: boolean;
    isDeleted: boolean;
    applicationFileUrl: String;

    constructor(application?: {
        _id, title, policy_type, insured, broker, media, auditFields, isActive, isDeleted, applicationFileUrl }) {
        if (application) {
            _.assignIn(this, application);
        }
    }
}
