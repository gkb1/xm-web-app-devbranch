import * as _ from 'lodash';

export class Address {
    public street1: string;
    public street2: string;
    public city: string;
    public zipcode: string;
    public state: string;
    public country: string;
    constructor(address?: {
        street1, street2, city, zipcode, state, country
    }) {
        if (address) {
            _.assignIn(this, address);
    }
}
}
