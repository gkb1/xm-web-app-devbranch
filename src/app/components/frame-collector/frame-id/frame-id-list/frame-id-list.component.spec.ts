import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameIdListComponent } from './frame-id-list.component';

describe('FrameIdListComponent', () => {
  let component: FrameIdListComponent;
  let fixture: ComponentFixture<FrameIdListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameIdListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameIdListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
