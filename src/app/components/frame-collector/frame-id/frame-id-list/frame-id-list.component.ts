import { Router } from '@angular/router';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MatDialog, PageEvent, MatSelectChange } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FrameCollectorService } from 'app/components/frame-collector/frame-collector.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { MyErrorStateMatcher } from 'app/constants/errorStateMatcher';
import { Subscription } from 'rxjs/Subscription';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { isNullOrUndefined } from 'util';
import * as Constant from '../../../../constants/app.constants';
import { UserService } from 'app/components/admin/users/users.service';

@Component({
  selector: 'app-frame-id-list',
  templateUrl: './frame-id-list.component.html',
  styleUrls: ['./frame-id-list.component.scss']
})

export class FrameIdListComponent implements OnInit, OnDestroy {
  frameCollectionForm: FormGroup;
  matcher: MyErrorStateMatcher;
  httpPrdtImageUploadSubs: Subscription;
  selectedStoreID: string;
  displayedColumns: string[] = ['storeName', 'image', 'frameID', 'tag'];
  dataSource = [];
  viewImageUrl: any;
  startIndex: number;
  modalRef: BsModalRef;
  isEditMode = false;
  storeExist = false;
  httpStoreSubs: Subscription;
  frameIdList = [];
  storeInfo;
  selectedFile;
  imagePreview;
  payload = { conditions: {}, pagination: {} }
  uploadedFile;

  constructor(
    private dialog: MatDialog,
    private formBuilder: FormBuilder,
    private frameCollectorService: FrameCollectorService,
    private loader: LoadingBarService,
    private notify: NotificationService,
    private modalService: BsModalService,
    private router: Router,
    private userServices: UserService
  ) { }

  frameIdCollectionLists(payload) {
    this.frameCollectorService.getframes(payload).subscribe(res => {
      if (res) {
        this.dataSource = res
      } else {
        this.notify.warn('Warning!', 'No records found')
      }
    })
  }

  ngOnInit() {
    this.getUserList();
    this.initFrameCollectionForm();
    // this.frameCollectorService.instantUpdates$.subscribe(() => {
    //   this.frameIdCollectionLists(this.payload)
    // })
    this.frameIdCollectionLists(this.payload)
  }

  getUserList() {
    const payload = {
      conditions: { role: Constant.ROLE_ECP_CUSTOMER }
    }
    this.httpStoreSubs = this.userServices.getAllUserListByCond(payload).subscribe((res: any) => {
      this.storeInfo = res.data;
    })
  }

  selectStore(value) {
    if (value) {
      const storeIndex = this.storeInfo.map(function (x) { return x.ecpCust.cust_code; }).indexOf(value);
      if (storeIndex > -1) {
        this.frameCollectionForm.get('ecpUserId').setValue(this.storeInfo[storeIndex]._id);
        this.frameCollectionForm.get('store_code').setValue(this.storeInfo[storeIndex].ecpCust.store_code);
        // this.frameCollectionForm.get('cust_code').setValue(this.storeInfo[storeIndex].ecpCust.cust_code);
        this.storeExist = true;
        this.frameCollectionForm.controls.cust_code.disable();
      } else {
        return this.notify.error('Invalid Store Customer Code !!', '');
      }
    }
  }
  initFrameCollectionForm() {
    this.frameCollectionForm = this.formBuilder.group({
      cust_code: ['', { validators: [Validators.required] }],
      ecpUserId: [''],
      frameId: ['', { validators: [Validators.required] }],
      media: ['', { validators: [Validators.required] }],
      store_code: ['']
    });
  }

  addFrameIdModel(template, selFrame) {
    this.frameCollectionForm.reset();
    this.imagePreview = null;
    this.storeExist = false;
    this.isEditMode = false;
    if (!isNullOrUndefined(selFrame)) {
      this.isEditMode = true;
    }
    this.modalRef = this.modalService.show(template);
  }

  setMedia(event) {
    const file = (event.target as HTMLInputElement).files[0];
    if (file.type.indexOf('image/') > -1) {
      this.uploadedFile = file;
      const reader = new FileReader();
      this.frameCollectionForm.get('media').setValue(this.uploadedFile);
      reader.onload = () => {
        this.imagePreview = reader.result;
      };
      reader.readAsDataURL(file);
    } else {
      this.notify.error('Unable to Upload', 'Upload only image file');
    }
  }

  addFrame() {
    const formValue = this.frameCollectionForm.getRawValue();
    const formData = new FormData();
    formData.append('cust_code', formValue.cust_code);
    formData.append('ecpUserId', formValue.ecpUserId);
    formData.append('frameId', formValue.frameId);
    formData.append('media', this.uploadedFile);
    formData.append('store_code', formValue.store_code);
    this.loader.show();
    this.httpStoreSubs = this.frameCollectorService.addFrameId(formData).subscribe(res => {
      if (res) {
        this.modalRef.hide();
        this.notify.success('Success!!', 'Frame Id added successfully!');
        this.loader.hide();
        this.frameIdCollectionLists(this.payload)
        this.storeExist = false;
      }
    }, (error) => {
      this.frameCollectionForm.controls.cust_code.enable();
      this.loader.hide();
      this.storeExist = false;
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 406) {
        this.notify.error('Unable to load!!', body.message);
      } else if (body.status === 409) {
        this.notify.error('Already exist!!', body.message);
      } else {
        this.notify.error('Something went wrong', error.statusText);
      }
    });
    this.imagePreview = null;
    this.frameCollectionForm.reset();
  }

  imagePopUpView(template, element) {
    this.viewImageUrl = element;
    this.modalRef = this.modalService.show(template);
  }

  onPageEvent(pageEvent: PageEvent) {
    this.startIndex = pageEvent.pageIndex;
  }

  ngOnDestroy() {
    if (this.httpPrdtImageUploadSubs) {
      this.httpPrdtImageUploadSubs.unsubscribe();
    }
    if (this.httpStoreSubs) {
      this.httpStoreSubs.unsubscribe();
    }
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
}
