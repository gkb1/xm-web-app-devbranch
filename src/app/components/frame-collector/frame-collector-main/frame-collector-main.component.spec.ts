import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameCollectorMainComponent } from './frame-collector-main.component';

describe('FrameCollectorMainComponent', () => {
  let component: FrameCollectorMainComponent;
  let fixture: ComponentFixture<FrameCollectorMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameCollectorMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameCollectorMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
