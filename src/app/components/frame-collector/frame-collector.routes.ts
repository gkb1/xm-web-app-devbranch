import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { RoleGuardService } from 'app/security/role-guard.service';
import * as Constant from './../../constants/app.constants';
import { FrameCollectorMainComponent } from 'app/components/frame-collector/frame-collector-main/frame-collector-main.component';
import { FrameIdListComponent } from 'app/components/frame-collector/frame-id/frame-id-list/frame-id-list.component';
import { ProductCreateUpdateComponent } from 'app/components/admin/products/product-create-update/product-create-update.component';
import { ProductsListComponent } from 'app/components/admin/products/products-list/products-list.component';

const routes: Routes = [
    {
        path: 'dashboard',  canActivate: [AuthGuard], component: FrameCollectorMainComponent,  children: [
            { path: 'frameId',  canActivate: [RoleGuardService],
                data: {
                    expectedRole: [Constant.ROLE_FRAME_COLLECTOR]
                },
                component: FrameIdListComponent
            },
            { path: 'brand', loadChildren: '../../components/admin/brand/brand.module#BrandModule'},
            { path: 'product', loadChildren: '../../components/admin/products/products.module#AdminProductsModule'},
            { path: 'frame-lookup',  loadChildren: '../../components/admin/frame-lookup/frame-lookup.module#FrameLookupModule'
        },
        ]
    },
   { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

export const FrameCollectorRoutingModule = RouterModule.forChild(routes);
