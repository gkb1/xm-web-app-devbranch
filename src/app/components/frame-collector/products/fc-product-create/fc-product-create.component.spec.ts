import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FcProductCreateComponent } from './fc-product-create.component';

describe('FcProductCreateComponent', () => {
  let component: FcProductCreateComponent;
  let fixture: ComponentFixture<FcProductCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FcProductCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FcProductCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
