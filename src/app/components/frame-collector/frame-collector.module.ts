import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrameCollectorMainComponent } from './frame-collector-main/frame-collector-main.component';
import { MatSidenavModule, MatIconModule, 
  MatToolbarModule, MatButtonModule, MatListModule, MatTableModule, MatTabsModule, MatPaginatorModule, MatSelectModule, MatFormFieldModule, MatInputModule } from '@angular/material';
import { ModalModule, BsDropdownModule } from 'ngx-bootstrap';
import { FrameIdListComponent } from 'app/components/frame-collector/frame-id/frame-id-list/frame-id-list.component';
import { FrameCollectorRoutingModule } from 'app/components/frame-collector/frame-collector.routes';
import { FrameCollectorService } from 'app/components/frame-collector/frame-collector.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialDesignElementsModule } from 'app/components/shared/material-design-element/material-design-element.module';
import { RouterModule } from '@angular/router';
import { SideNavComponent } from 'app/components/shared/side-nav/side-nav.component';
import { AdminHeaderComponent } from 'app/components/shared/header/header.component';
import { NavbarService } from 'app/components/shared/navbar.service';
import { SharedModule } from 'app/components/shared/shared.module';
import {MatTooltipModule} from '@angular/material/tooltip';
import { AdminProductsModule } from 'app/components/admin/products/products.module';
import { AdminProductService } from 'app/components/admin/products/product.service';
import { UserService } from '../admin/users/users.service';

@NgModule({
  declarations: [
    FrameCollectorMainComponent,
    // SideNavComponent,
    // AdminHeaderComponent,
    FrameIdListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FrameCollectorRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialDesignElementsModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    MatButtonModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
    MatFormFieldModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatTooltipModule
  ],
  providers: [
    NavbarService,
    AdminProductService,
    FrameCollectorService,
    UserService
  ],
})
export class FrameCollectorModule { }
