import { Injectable } from '@angular/core';
import * as Constant from '../../constants/app.constants';
import { Http } from '@angular/http';
import * as AppUtils from '../../utils/app.utils';
import { Subject } from 'rxjs';

@Injectable()
export class FrameCollectorService {
  private instantUpdate$ = new Subject<void>();

  constructor(private http: Http) { }

  get instantUpdates$() {
    return this.instantUpdate$;
  }

  imageUpload(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.PRDT_IMAGE_UPLOAD_URL, payload)
    .map(res => res.json());
  }

  getStore(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_USERS_LIST_BY_CONDN, payload)
    .map(res => res.json());
  }

  addFrameId(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.CREATE_FRAME_ID, payload)
    .map(res => res.json());
  }

  getframes(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_FRAME_IDS, payload)
    .map(res => res.json());
  }

  getSelUserByCondn(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_SEL_USER_BY_COND, payload)
    .map(res => res.json());
  }
}
