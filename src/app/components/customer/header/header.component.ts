import { Component, OnInit, Input, Output, EventEmitter, HostListener, ElementRef, ViewChild } from '@angular/core';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { NotificationService } from 'app/components/shared/notification/notification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  affixClass = '';
  isIn: Boolean = false;
  @ViewChild('navbarToggler', { static : false}) navbarToggler: ElementRef;

  constructor(public authService: AuthenticationService,
    private notification: NotificationService) {
  }

  ngOnInit() {
  }

  isCollapsedToggle() {
    this.authService.isNavBarCollapsed.next(this.isIn);
    this.authService.isNavBarCollapsed.subscribe(obj => {
      this.isIn = obj;
    })
    const isCollapsed = this.isIn;
    this.isIn = isCollapsed;
    this.isNotificationPopUpShown();
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    const number = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (number > 50) {
      this.affixClass = 'affix';
    } else {
      this.affixClass = '';
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isIn = false;
    this.authService.isNavBarCollapsed.next(this.isIn);
  }

  toggle() {
    this.isIn = false;
    this.authService.isNavBarCollapsed.next(this.isIn);
  }

  navBarTogglerIsVisible() {
    if (this.navbarToggler) {
      return this.navbarToggler.nativeElement.offsetParent !== null;
    }
  }

  collapseNav() {
    if (this.navBarTogglerIsVisible()) {
      this.navbarToggler.nativeElement.click();
    }
  }

  isNotificationPopUpShown() {
    this.notification.getAlert().subscribe((notification: Notification) => {
      if (notification) {
        this.collapseNav();
      }
    });
  }

  logout() {
    this.authService.logout();
  }
}
