import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { CustomerMainComponent } from 'app/components/customer/customer-main/customer-main.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { HomeComponent } from 'app/components/customer/home/home.component';

const customerRoutes: Routes = [
    // canActivate: [AuthGuard]
    {
        path: '', component: CustomerMainComponent,  children: [
        ]
    },
   { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const CustomerRoutingModule = RouterModule.forChild(customerRoutes);
