import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { CustomerService } from 'app/components/customer/customer-service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
  isIn: Boolean = false;
  userObj;
  navBarMenuList = [];
  constructor(public authService: AuthenticationService,
  private custService: CustomerService) { }

  ngOnInit() {
    this.navBarMenuList = this.custService.customerNavBarMenuList;
    this.userObj = this.authService.getAuthDeatils();
    this.authService.isNavBarCollapsed.subscribe(value => {
      this.isIn = value;
    })
  }

  isCollapsedToggle() {
    this.authService.isNavBarCollapsed.next(false);
  }

  logout() {
    this.authService.logout();
  }

}