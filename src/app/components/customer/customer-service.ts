import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import * as Constant from '../../constants/app.constants';
import * as AppUtils from '../../../app/utils/app.utils';
import { Http } from '@angular/http';

@Injectable()
export class CustomerService {
  customerNavBarMenuList: any = [
    {
      'id': 0,
      'name': 'My Account',
      'routeLink': [''],
      'icon': 'assets/common/menu-01.svg'
    },
    {
      'id': 1,
      'name': 'My Order',
      'routeLink': [''],
      'icon': 'assets/common/menu-02.svg'
    },
    {
      'id': 2,  
      'name': 'My Address',
      'routeLink': [''],
      'icon': 'assets/common/menu-03.svg'
    },
    {
      'id': 3,
      'name': 'My Wishlist',
      'routeLink': [''],
      'icon': 'assets/common/menu-04.svg'
    },
    {
      'id': 4,
      'name': 'Call Us',
      'routeLink': [''],
      'icon': ''
    },
    {
      'id': 5,
      'name': 'Write to Us',
      'routeLink': [''],
      'icon': ''
    },
    {
      'id': 6,
      'name': 'FAQ',
      'routeLink': [''],
      'icon': ''
    },
    {
      'id': 7,
      'name': 'Terms and Conditions',
      'routeLink': [''],
      'icon': ''
    },

    {
      'id': 8,
      'name': 'About Us',
      'routeLink': [''],
      'icon': ''
    }
  ];
  constructor(private http : HttpClient,
    private https: Http) { }

  getGlassType() {
    return this.http.get('assets/json/spectype.json');
  }
  getByCategories(payload) {
    return this.https.post(AppUtils.BACKEND_API_ROOT_URL + Constant.GET_ALL_CATEGORIES, payload)
    .map(res => res.json()) ;
  }

  getByCategoriesWithTrending(payload) {
    return this.https.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_CAT_WITH_TRENDING_URL, payload)
    .map(res => res.json()) ;
  }
}
