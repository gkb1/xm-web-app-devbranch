import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerMainComponent } from './customer-main/customer-main.component';
import { CustomerRoutingModule } from 'app/components/customer/customer-routes';
import { CustomerService } from 'app/components/customer/customer-service';
import { MatTabsModule, MatFormFieldModule, MatSidenavModule, MatIconModule } from '@angular/material';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SideNavComponent } from 'app/components/customer/side-nav/side-nav.component';
import { HomeComponent } from 'app/components/customer/home/home.component';
import { ProductListComponent } from 'app/components/customer/products/product-list/product-list.component';
import { ProductDetailComponent } from 'app/components/customer/products/product-detail/product-detail.component';
import { CarouselModule } from 'ngx-bootstrap';
import { CustomerSharedModule } from 'app/components/customer/customer-shared.module';
import { ClickOutsideModule } from 'ng-click-outside';

@NgModule({
  declarations: [
    CustomerMainComponent,
    // HeaderComponent,
    // SideNavComponent,
    // HomeComponent,
    // ProductListComponent,
    // ProductDetailComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    CustomerSharedModule,
    CustomerRoutingModule,
    MatTabsModule,
    MatFormFieldModule,
    MatSidenavModule,
    MatIconModule,
    CarouselModule.forRoot(),
    ClickOutsideModule
  ],
  providers: [
    CustomerService
  ]
})
export class CustomerModule { }
