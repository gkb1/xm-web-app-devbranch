import { Component, OnInit, OnDestroy } from '@angular/core';
import { ProductService } from '../product.service';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { ActivatedRoute } from '@angular/router';
import { isNullOrUndefined } from 'util';
import * as _ from 'lodash';
import { Subscription } from 'rxjs/Subscription';
import { AuthenticationService } from 'app/components/authentication/authentication.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {
  page = 0;
  limit = 100;
  payload = { conditions: {}, pagination: {}}
  listView = false;
  productList = [];
  productColourList = [];
  httpGetAllPrdtsSubs: Subscription;
  isIn: Boolean = false;

  constructor(public authService: AuthenticationService,
    private productService: ProductService,
    private notification: NotificationService,
    private loader: LoadingBarService,
    private route: ActivatedRoute,
    private notify: NotificationService ) { }

  ngOnInit() {
    this.payload.pagination = { page: this.page, limit: this.limit};
    this.getRouteParams();
  }

  isCollapsedToggle() {
    this.authService.isNavBarCollapsed.next(this.isIn);
    this.authService.isNavBarCollapsed.subscribe(obj => {
      this.isIn = obj;
    })
    const isCollapsed = this.isIn;
    this.isIn = isCollapsed;
  }


  getRouteParams() {
    this.route.params.subscribe(params => {
      if (!isNullOrUndefined(params)) {
          if (!isNullOrUndefined(params['category'])) {
            if (params['category'].indexOf('_') > -1) {
              const splitStr = params['category'].split('_');
              if (splitStr[0]) {
                this.payload.conditions['catName'] = splitStr[0];
              }
              if (splitStr[1]) {
                this.payload.conditions['childCatName'] = splitStr[1];
              }
            } else {
              this.payload.conditions['catName'] = params['category'];
            }
          this.loadProductList();
        }
      }
    });
  }

  loadProductList() {
    this.payload.conditions['isApproved'] = true;
    this.payload.conditions['isActivate'] = true;
    this.httpGetAllPrdtsSubs = this.productService.getAllProducts(this.payload).subscribe(res => {
      if (res && res.data) {
        // this.productList = res.data;
        _.forEach(res.data, product => {
              const selColourDefaultCount = product.prdt_attributes.filter((v) => (v.colour.isDefault === true)).length;
              if (selColourDefaultCount <= 0) { // if no default colour
                this.setProductImageSizeByColour(product, product.prdt_attributes[0]);
              } else {
                  _.forEach(product.prdt_attributes , attribute => {
                    this.productColourList.push(attribute.colour);
                    // setting default product size list
                    if (attribute.colour.isDefault) {
                      this.setProductImageSizeByColour(product, attribute);
                    }
                });
            }
          this.productList.push(product);
        })
      }
    }, (error) => {
      this.loader.hide();
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 406) {
        this.notify.error('Unable to load!!', body.message);
      } else {
        this.notify.error('Something went wrong', error.statusText);
      }
    });
  }

  setProductImageSizeByColour(selProduct, selAttribute) {
    // selected size list only
    selAttribute.colour.size = _.filter(selAttribute.colour.size, function (p) {
      return _.includes([true], p.isSelected);
    });
    if (selAttribute.colour) {
      selProduct.defaultPrdtSizeList = _.filter(selAttribute.colour.size, function (p) {
        return p;
      });
      // setting default product cover img by colour
      if (selAttribute.colour.cover_img) {
        selProduct.defaultCoverImgUrl = selAttribute.colour.cover_img.url;   
      }
    }
  }

  ngOnDestroy() {
    if (this.httpGetAllPrdtsSubs) {
      this.httpGetAllPrdtsSubs.unsubscribe();
    }
  }
}
