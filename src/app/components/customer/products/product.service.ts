import { Injectable } from '@angular/core';
// import { HttpClient } from '@angular/common/http';
import * as AppUtils from '../../../utils/app.utils';
import { Http } from '@angular/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: Http) { }

getAllProducts(payload) {
  return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_PRODUCTS_URL, payload)
    .map(res => res.json());
}
getSelProductDetailsByPrdtId(productId) {
  return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_SEL_PRDT_DETAILS_BY_PRDTID_URL + productId)
    .map(res => res.json());
}
}
