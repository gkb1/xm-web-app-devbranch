import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ProductService } from '../product.service';
import { ActivatedRoute } from '@angular/router';
import { isNullOrUndefined } from 'util';
import { Subscription } from 'rxjs/Subscription';
import * as _ from 'lodash';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { CarouselComponent } from 'ngx-bootstrap';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  // mySlideImages = [1,2].map((i)=> `https://picsum.photos/640/480?image=${i}`);
  bigerImageView: any;
  defaultImage: boolean;
  selectedIndex: number;
  imageIndex;
  selectedColorIndex: number;
  selPrdtDetails;
  httpGetSelPrdtDetailsSubs: Subscription;
  sizeByColor: any = [];
  mediaImage: any = [];
  customProdArtArr: any = [];
  coverImage: any;
  isIn: Boolean = false;
  SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };
  @ViewChild('carousel', { static: false}) carousel: CarouselComponent;

  constructor(private authService: AuthenticationService,
    private productService: ProductService,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.defaultImage = true;
    this.route.params.subscribe((params) => {
      if (!isNullOrUndefined(params) && !isNullOrUndefined(params)) {
        this.loadSelProductDetailsByPrdId(params);
      }
    });
  }

  isCollapsedToggle() {
    this.authService.isNavBarCollapsed.next(this.isIn);
    this.authService.isNavBarCollapsed.subscribe(obj => {
      this.isIn = obj;
    })
    const isCollapsed = this.isIn;
    this.isIn = isCollapsed;
  }

  imageDetails(url, index) {
    this.imageIndex = index;
    this.defaultImage = false;
    this.bigerImageView = url;
  }

  setSize(index: number) {
    this.selectedIndex = index;
  }

  setSizeImageByColourOfSelAttr(prodArr) {
    this.mediaImage = [];
      // set size by colour
      if (prodArr.colour.size && prodArr.colour.size.length > 0) {
        this.sizeByColor = _.filter(prodArr.colour.size, function (p) {
          return _.includes([true], p.isSelected);
        });
      }
      // default cover Image
      if (prodArr.colour.cover_img) {
        this.coverImage = prodArr.colour.cover_img.url;
        this.mediaImage.push({ image: this.coverImage, visible: true});
      }
      this.imageIndex = 0; // default
      // default media Image
      if (prodArr.colour.media.length > 0) {
        prodArr.colour.media.forEach(media => {
          if (media.url && media.url !== '') {
            this.mediaImage.push({image: media.url, visible: false });
          }
        });
      }
  }

  setColor(index: number, proAtrArr) {
    this.selectedColorIndex = index;
    this.defaultImage = true;
    this.mediaImage = [];
    if (proAtrArr[index].colour) {
      this.setSizeImageByColourOfSelAttr(proAtrArr[index]);
    }
  }

  loadSelProductDetailsByPrdId(payload) {
    this.httpGetSelPrdtDetailsSubs = this.productService.getSelProductDetailsByPrdtId(payload.productId).subscribe(res => {
      if (res && res.data) {
        this.selPrdtDetails = res.data;
        // this.selPrdtDetails.forEach(detail => {
        const selColourDefaultCount = this.selPrdtDetails.prdt_attributes.filter((v) => (v.colour.isDefault === true)).length;
        if (selColourDefaultCount <= 0) { // if no default colour
          this.setSizeImageByColourOfSelAttr(this.selPrdtDetails.prdt_attributes[0]);
        } else {
          this.selPrdtDetails.prdt_attributes.forEach(prodArr => {
            if (prodArr.colour.isDefault) {
              this.setSizeImageByColourOfSelAttr(prodArr);
            }
          });
        }
        // });
      }
    });
  }

  swipe(currentIndex: number, action) {
    // tslint:disable-next-line:curly
    if (currentIndex > this.mediaImage.length || currentIndex < 0) return;
    let nextIndex = 0;
    // next
    if (action === this.SWIPE_ACTION.RIGHT) {
        const isLast = currentIndex === this.mediaImage.length - 1;
        nextIndex = isLast ? 0 : currentIndex + 1;
    }
    // previous
    if (action === this.SWIPE_ACTION.LEFT) {
        const isFirst = currentIndex === 0;
        nextIndex = isFirst ? this.mediaImage.length - 1 : currentIndex - 1;
    }
    this.carousel.activeSlide = nextIndex;
    // toggle image visibility
    this.mediaImage.forEach((x, i) => x.visible = (i === nextIndex));
}

  ngOnDestroy() {
    if (this.httpGetSelPrdtDetailsSubs) {
      this.httpGetSelPrdtDetailsSubs.unsubscribe();
    }
  }
}
