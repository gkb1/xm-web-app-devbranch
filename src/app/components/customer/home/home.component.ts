import { Component,OnInit,
  OnDestroy,
  ElementRef,
  Renderer2,
  ViewChild
} from '@angular/core';
import { MatTabChangeEvent, MatSidenav } from '@angular/material';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { CustomerService } from '../customer-service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  @ViewChild('sidenav', { static: true }) sideNav: MatSidenav;
  tab: any;
  totalCount;
  catList = [];
  payload = { conditions: {}, pagination: { page: 0, limit: 100 } };
  httpGetAllCatSubs: Subscription;
  isIn: Boolean = false;
  
  sidenavWidth = 4;
  ngStyle: string;
  overlay = false;
  customerNavBarMenuList = [];
  userRole;

  constructor(
    private elementRef: ElementRef,
    public authService: AuthenticationService,
    public custService: CustomerService,
    private renderer: Renderer2
  ) {}

  ngOnInit() {
    document.querySelector('body').classList.add('colorpurple');
    this.payload.conditions['name'] = 'MEN';
    this.getAllCategories(this.payload);
    this.getNavBarMenu();
    this.ngStyle = 'this.sidenavWidth = 0';
  }

  isCollapsedToggle() {
    this.authService.isNavBarCollapsed.next(this.isIn);
    this.authService.isNavBarCollapsed.subscribe(obj => {
      this.isIn = obj;
    })
    const isCollapsed = this.isIn;
    this.isIn = isCollapsed;
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent): void {
    this.tab = tabChangeEvent.tab.textLabel;
    this.payload.conditions['name'] = this.tab;
    this.getAllCategories(this.payload);
  }

  getAllCategories(payload) {
    this.httpGetAllCatSubs = this.custService
      .getByCategoriesWithTrending(payload)
      .subscribe((res: any) => {
        if (res && res.data) {
          this.catList = res.data;
          this.totalCount = res.count;
        }
      });
  }
  logout() {
    this.authService.logout();
  }
  getNavBarMenu() {
    // const user = this.custService.getAuthDeatils();
    // this.userRole = user.role;
    this.customerNavBarMenuList = this.custService.customerNavBarMenuList;
  }
  increase() {
    this.sidenavWidth = 15;
    this.overlay = true;
  }
  decrease() {
    this.sidenavWidth = 4;
    this.overlay = false;
  }
  sidenavToggle() {
    this.ngStyle = 'this.sidenavWidth = 15';
  }
  // tog() {
  //   console.log('ttt')
  //   this.sideNav.toggle();
  // }
  ngOnDestroy() {
    if (this.httpGetAllCatSubs) {
      this.httpGetAllCatSubs.unsubscribe();
    }
    document.querySelector('body').classList.remove('colorpurple');
  }
}
