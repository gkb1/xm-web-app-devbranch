import { Component, OnInit, Input, HostBinding, ViewChild, ElementRef, HostListener } from '@angular/core';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { NotificationService } from '../notification/notification.service';

@Component({
  selector: 'app-shared-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss',
'../../customer/header/header.component.scss']
})
export class AdminHeaderComponent implements OnInit {
  @HostBinding('class') navClass = 'app-header';
  @Input() sideNav;
  username;
  isIn: Boolean = false;
  @ViewChild('navbarToggler', { static : false}) navbarToggler: ElementRef;

  constructor(public authService: AuthenticationService,
  private notification: NotificationService ) { }

  ngOnInit() {
    this.username = this.authService.getAuthDeatils().name;
  }

  toggleSideNav() {
    this.sideNav.toggle();
  }

  closeSideNav() {
    this.sideNav.close();
  }

  isCollapsedToggle() {
    this.authService.isNavBarCollapsed.next(this.isIn);
    this.authService.isNavBarCollapsed.subscribe(obj => {
      this.isIn = obj;
    })
    const isCollapsed = this.isIn;
    this.isIn = isCollapsed;
    this.isNotificationPopUpShown();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.isIn = false;
    this.authService.isNavBarCollapsed.next(this.isIn);
  }

  toggle() {
    this.isIn = false;
    this.authService.isNavBarCollapsed.next(this.isIn);
  }

  navBarTogglerIsVisible() {
    if (this.navbarToggler) {
      return this.navbarToggler.nativeElement.offsetParent !== null;
    }
  }

  collapseNav() {
    if (this.navBarTogglerIsVisible()) {
      this.navbarToggler.nativeElement.click();
    }
  }

  isNotificationPopUpShown() {
    this.notification.getAlert().subscribe((notification: Notification) => {
      if (notification) {
        this.collapseNav();
      }
    });
  }

  logout() {
    this.authService.logout();
  }
}
