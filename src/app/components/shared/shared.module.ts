import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMaterailModule } from 'app/custom-materail/custom-materail.module';
import { AppComponent } from 'app/app.component';
import { MatSidenavModule, MatListModule } from '@angular/material';
import { SideNavComponent } from 'app/components/shared/side-nav/side-nav.component';
import { AdminHeaderComponent } from 'app/components/shared/header/header.component';
import { NavbarService } from 'app/components/shared/navbar.service';
import { SharedRoutingModule } from 'app/components/shared/shared.routes';
import { BsDropdownModule, ModalModule } from 'ngx-bootstrap';
import { FrameSearchComponent } from 'app/components/shared/frame-search/frame-search.component';
import { ClickOutsideModule } from 'ng-click-outside';


@NgModule({
  declarations: [
    SideNavComponent,
    AdminHeaderComponent,
    FrameSearchComponent
  ],
  imports: [
    CommonModule,
    SharedRoutingModule,
    CustomMaterailModule,
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    ClickOutsideModule
  ],
  exports: [
    SideNavComponent,
    AdminHeaderComponent,
    FrameSearchComponent
  ],
  providers: [
    NavbarService
  ]
})
export class SharedModule { }
