import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { RoleGuardService } from 'app/security/role-guard.service';
import * as Constant from './../../constants/app.constants';
import { SideNavComponent } from 'app/components/shared/side-nav/side-nav.component';

const routes: Routes = [
];

export const SharedRoutingModule = RouterModule.forChild(routes);
