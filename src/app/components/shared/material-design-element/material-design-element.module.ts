import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MdInputDirective} from './md-input.directive';
import {MdSmallProgressCircleComponent} from './md-small-progress-circle.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [MdInputDirective, MdSmallProgressCircleComponent],
  exports: [MdInputDirective, MdSmallProgressCircleComponent]
})
export class MaterialDesignElementsModule {
}
