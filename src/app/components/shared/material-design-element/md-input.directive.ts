import {
    Directive, ElementRef, HostListener, Input, Renderer
  } from '@angular/core';

  @Directive({
    selector: '[appMdInput]'
  })
  export class MdInputDirective {
    constructor(private element: ElementRef,
                private renderer: Renderer) {
    }

    @HostListener('focus')
    mdInputFocus() {
      this.renderer.setElementClass(this.element.nativeElement, 'float', true);
    }

    @HostListener('blur')
    mdInputBlur() {
      (this.element.nativeElement as HTMLInputElement).value.trim();
      if (this.element.nativeElement.value === '') {
        this.renderer.setElementClass(this.element.nativeElement, 'float', false);
      }
    }

    @HostListener('ngModelChange')
    onChange() {
      (this.element.nativeElement as HTMLInputElement).value.trim();
      if (this.element.nativeElement.value === '') {
        this.renderer.setElementClass(this.element.nativeElement, 'float', false);
      }
      if (this.element.nativeElement.value !== '') {
        this.renderer.setElementClass(this.element.nativeElement, 'float', true);
      }
    }
  }
