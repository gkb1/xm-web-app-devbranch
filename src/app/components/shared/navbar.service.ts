import { Injectable } from '@angular/core';
import * as Constant from '../../constants/app.constants';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { Http } from '@angular/http';
import * as AppUtils from '../../utils/app.utils';

@Injectable()
export class NavbarService {
  frameModeratorNavBarList: any = [
    {
      'id': 0,
      'name': 'Home',
      'routeLink': ['/framemoderator/dashboard'],
      'access': [ Constant.ROLE_FRAME_MODERATOR],
      'icon': 'home'
      },
    {
      'id': 1,
      'name': 'Search By Tag No',
      'routeLink': ['/framemoderator/dashboard/searchFrame'],
      'access': [ Constant.ROLE_FRAME_MODERATOR],
      'icon': 'search'
    },
];
  adminNavBarList = [
    {
      'id': 0,
      'name': 'Home',
      'routeLink': ['/admin/dashboard'],
      'access': [ Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN],
      'icon': 'home'
    },
    {
      'id': 1,
      'name': 'Users',
      'routeLink': ['/admin/dashboard/users'],
      'access': [ Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN],
      'icon': 'assignment_ind'
    },
    {
      'id': 2,
      'name': 'Product',
      'routeLink': ['/admin/dashboard/product'],
      'access': [ Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN ],
      'icon': 'collections_bookmark'
    },
    {
      'id': 6,
      'name': 'Brand',
      'routeLink': ['/admin/dashboard/brand'],
      'access': [ Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN ],
      'icon': 'branding_watermark'
    },
    // {
    //   'id': 7,  
    //   'name': 'Category',
    //   'routeLink': ['/admin/dashboard/category'],
    //   'access': [ Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN ],
    //   'icon': 'list'
    // },
    {
      'id': 3,
      'name': 'Frame Look Up',
      'routeLink': ['/admin/dashboard/frame-lookup'],
      'access': [ Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN ],
      'icon': 'list'
    },
    {
      'id': 4,
      'name': 'Frame Collectors',
      'routeLink': ['/admin/dashboard/framecollectors'],
      'access': [ Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN, Constant.ROLE_ECP_CUSTOMER ],
      'icon': 'collections_bookmark'
    },
    {
      'id': 5,
      'name': 'Search By Tag No',
      'routeLink': ['/admin/dashboard/searchFrame'],
      'access': [ Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN],
      'icon': 'search'
    },
    {
      'id': 8,
      'name': 'Verify',
      'routeLink': ['/admin/dashboard/verifyImage'],
      'access': [ Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN ],
      'icon': 'verified_user'
    }
  ];
  ecpNavBarList = [
      {
        'id': 0,
        'name': 'Home',
        'routeLink': ['/ecpcust/dashboard'],
        'access': [ Constant.ROLE_ECP_CUSTOMER],
        'icon': 'home'
      },
      // {
      //   'id': 2,
      //   'name': 'Product',
      //   'routeLink': ['/ecpcust/dashboard/product'],
      //   'access': [ Constant.ROLE_ECP_CUSTOMER ],
      //   'icon': 'assignment'
      // },
      {
        'id': 2,
        'name': 'Add New Product',
        'routeLink': ['/framecollector/dashboard/product/fcCreate'],
        'access': [ Constant.ROLE_FRAME_COLLECTOR ],
        'icon': 'collections_bookmark'
      },
      {
        'id': 3,
        'name': 'Frame Collectors',
        'routeLink': ['/ecpcust/dashboard/framecollectors'],
        'access': [ Constant.ROLE_ECP_CUSTOMER ],
        'icon': 'collections_bookmark'
      },
  ]
  frameNavBarCollectList = [
    {
      'id': 0,
      'name': 'Home',
      'routeLink': ['/framecollector/dashboard'],
      'access': [ Constant.ROLE_FRAME_COLLECTOR],
      'icon': 'home'
    },
    {
      'id': 6,
      'name': 'Brand',
      'routeLink': ['/framecollector/dashboard/brand'],
      'access': [ Constant.ROLE_FRAME_COLLECTOR ],
      'icon': 'branding_watermark'
    },
    {
      'id': 3,
      'name': 'Frame Look Up',
      'routeLink': ['/framecollector/dashboard/frame-lookup'],
      'access': [ Constant.ROLE_FRAME_COLLECTOR ],
      'icon': 'list'
    },
    {
      'id': 2,
      'name': 'Product',
      'routeLink': ['/framecollector/dashboard/product'],
      'access': [ Constant.ROLE_FRAME_COLLECTOR ],
      'icon': 'collections_bookmark'
    },
    // {
    //   'id': 2,
    //   'name': 'Add New Product',
    //   'routeLink': ['/framecollector/dashboard/product/fcCreate'],
    //   'access': [ Constant.ROLE_FRAME_COLLECTOR ],
    //   'icon': 'collections_bookmark'
    // },
    {
      'id': 1,
      'name': 'FrameID',
      'routeLink': ['/framecollector/dashboard/frameId'],
      'access': [ Constant.ROLE_FRAME_COLLECTOR ],
      'icon': 'branding_watermark'
    },
  ]
  constructor(private http: Http, private authService: AuthenticationService) { }

  getAllUserListByCond(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_USERS_LIST_BY_CONDN, payload)
      .map(res => res.json());
  }

  getAllCategoryList(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_CATEGORIES_URL, payload)
      .map(res => res.json());
  }

  getSelCatDetailsByCatId(categoryId) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_CAT_BY_CATID_URL + categoryId)
      .map(res => res.json());
  }

  getBrandsByCat(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_BRANDS_BY_CONDN_URL, payload)
      .map(res => res.json());
  }

  getSelPrdtDetailsByTagNoSearch(tagNo) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_SEL_PRDT_BY_TAGNO_SEARCH_URL + tagNo)
      .map(res => res.json());
  }

  productImageUpload(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.PRDT_IMAGE_UPLOAD_URL, payload)
      .map(res => res.json());
  }

  addPrdtAttributeToStore(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.ADD_SEL_PRDT_ATTR_TO_STORE_URL, payload)
      .map(res => res.json());
  }

  removeSelStoreFromPrdtAttr(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.REMOVE_SEL_STORE_PRDT_ATTR_URL, payload)
      .map(res => res.json());
  }

  updateGKBPrice(prodId, payload) {
    return this.http.put(AppUtils.BACKEND_API_ROOT_URL + AppUtils.UPDATE_GKB_PRICE_PRDTID_URL + prodId, payload)
      .map(res => res.json());
  }
}
