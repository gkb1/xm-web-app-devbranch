import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LoadingBarService {
  private subject = new Subject<boolean>();

  constructor() {
  }

  getLoadingObj(): Observable<any> {
    return this.subject.asObservable();
  }

  show() {
    this.subject.next(true);
  }

  hide() {
    this.subject.next(false);
  }

}
