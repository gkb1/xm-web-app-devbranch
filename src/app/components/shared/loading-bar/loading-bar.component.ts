import {Component, ElementRef, HostListener, OnDestroy, OnInit, Renderer} from '@angular/core';
import {LoadingBarService} from './loading-bar.service';
import {isBoolean} from 'util';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-loading-bar',
  templateUrl: './loading-bar.component.html',
  styleUrls: ['./loading-bar.component.scss']
})
export class LoadingBarComponent implements OnInit, OnDestroy {

  public showLoadingScreen = false;
  public loadingClass = '';
  private httpGetLoadingObj: Subscription;

  constructor(loadingService: LoadingBarService,
              private _elementRef: ElementRef,
              private renderer: Renderer) {
    this.httpGetLoadingObj = loadingService.getLoadingObj().subscribe(obj => {
      if (isBoolean(obj)) {
        if (obj) {
          this.renderer.setElementClass(this._elementRef.nativeElement.parentElement.children[0], 'blur-div', true);
        } else {
          this.renderer.setElementClass(this._elementRef.nativeElement.parentElement.children[0], 'blur-div', false);
        }
        if (this.showLoadingScreen !== obj) {
          this.loadingClass = obj ? 'showLoadingScreen' : 'hideLoadingScreen';
          setTimeout(() => {
            this.showLoadingScreen = obj;
          }, obj ? 0 : 800);
        }

      }
    });
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    if (this.httpGetLoadingObj) {
      this.httpGetLoadingObj.unsubscribe();
    }
  }

  backgroundBlurOn() {
    this.renderer.setElementClass(this._elementRef.nativeElement.parentElement.children[0], 'blur-div', true);
  }

  backgroundBlurOff() {
    this.renderer.setElementClass(this._elementRef.nativeElement.parentElement.children[0], 'blur-div', false);
  }

}
