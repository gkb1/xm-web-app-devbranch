import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { CommonService } from 'app/services/common.service';
import { MyErrorStateMatcher } from 'app/constants/errorStateMatcher';
import * as Constant from '../../../constants/app.constants';
import { Subscription } from 'rxjs';
import { AdminProductService } from 'app/components/admin/products/product.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import * as _ from 'lodash';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { Router } from '@angular/router';
import { ConfirmationService } from 'app/components/shared/confirmation/confirmation.service';
import { NavbarService } from 'app/components/shared/navbar.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { isNullOrUndefined } from 'util';

@Component({
  selector: 'app-frame-search',
  templateUrl: './frame-search.component.html',
  styleUrls: ['./frame-search.component.scss']
})
export class FrameSearchComponent implements OnInit, OnDestroy {
  searchFrameForm: FormGroup;
  matcher: MyErrorStateMatcher;
  productForm: FormGroup;
  pricingForm: FormGroup;
  prdtSizeList = Constant.LOOK_UP_SIZE;
  genderArr = Constant.PRODUCT_GENDER_LOOKUP;
  isLoggedInUserAdmin = false;
  isLoggedInUserECP = false;
  storeUsersList = [];
  httpGetAllUserSubs: Subscription;
  selPrdtDetails;
  tagNoStoreDetails;
  httpGetAllCatSubs: Subscription;
  selCategory;
  httpGetBrandsByCatSubs: Subscription;
  brandsList = [];
  httpPrdtImageUploadSubs: Subscription;
  httpGetSelPrdtDetailsSubs: Subscription;
  httpClonePrdtToStoreSubs: Subscription;
  selImageViewUrl;
  modalRef: BsModalRef;
  isLoggedInUserFrameModerator = false;

  constructor(private router: Router,
  private fb: FormBuilder,
  private commonService: CommonService,
  private navbarService: NavbarService,
  private loader: LoadingBarService,
  private authService: AuthenticationService,
  private notify: NotificationService,
  private confirmation: ConfirmationService,
  private modalService: BsModalService) { }

  ngOnInit() {
    if (this.authService.isLoggedInUserAdmin() || this.authService.isLoggedInUserSuperAdmin()) {
      this.isLoggedInUserAdmin = true;
    }
    this.isLoggedInUserECP = this.authService.isLoggedInUserECPCustomer();
    this.isLoggedInUserFrameModerator = this.authService.isLoggedInUserFrameModerator();
    this.initSearchFrameForm();
    this.initProductForm();
    this.initPricingForm();
  }

  initSearchFrameForm() {
    this.searchFrameForm = this.fb.group({
      tagNo: ['', [Validators.required]],
    });
  }

  initPricingForm() {
    this.matcher = new MyErrorStateMatcher();
    this.pricingForm = this.fb.group({
      pricing: this.fb.group({
        actualPrice: [''],
        gkbPrice: ['', { validators: [Validators.required] }],
        sellingPrice: ['']
      }),
    });
  }

  initProductForm() {
    this.matcher = new MyErrorStateMatcher();
    this.productForm = this.fb.group({
      pricing: this.fb.group({
        actualPrice: [''],
        gkbPrice: ['', { validators: [Validators.required] }],
        sellingPrice: ['']
      }),
      prdt_attributes: this.fb.array([this.createPrdtAttrForm()]),
    });
    this.getAllSellersByCond();
  }

  createPrdtAttrForm() {
    return this.fb.group({
      _id: [''],
      prdAttributeId: [''],
      colour: this.fb.group({
        frameId: ['', [Validators.required]],
        store: this.fb.array([]),
        prdColourId: [''],
        digitCode: [''],
        name: ['', [Validators.required]],
        value : ['', [Validators.required]],
        SKU : [''],
        isDefault: [false],
        cover_img: [''],
        media: this.fb.array(this.createProductMediaForm()),
        size: this.fb.array(this.createPrdtAttrColourForm())
      })
    });
  }

  createPrdtAttrStoreForm() {
    return this.fb.group({
      _id: [''],
      cust_code: [''],
      tagName: [''],
      store_code: ['']
    });
  }

  createProductMediaForm() {
    const formGroup = [];
    for (let i = 0; i <= 3; i++) {
      formGroup.push({});
    }
    return formGroup;
  }

  createPrdtAttrColourForm() {
    const formGroup = [];
    for (let i = 0; i < this.prdtSizeList.length; i++) {
      formGroup.push(this.fb.group({
        _id: [],
        prdSizeId: [],
        value: [this.prdtSizeList[i].value],
        quantity: [0],
        SKU: [''],
        isDefault: [false],
        isSelected: [false],
        digitCode: [this.prdtSizeList[i].code]
      }));
    }
    return formGroup;
  }

  getAllSellersByCond() { // ecp cust list
    const payload = { conditions: {}};
    if (this.isLoggedInUserAdmin) {
      payload['conditions']['role'] = Constant.ROLE_ECP_CUSTOMER;
    }
    if (this.isLoggedInUserECP) {
      payload['conditions']['_id'] = this.authService.getAuthDeatils()._id;
    }
    this.httpGetAllUserSubs = this.navbarService.getAllUserListByCond(payload).subscribe(res => {
      if (res && res.data) {
        this.storeUsersList = res.data;
      }
    })

  }

  searchProductByTagNo() {
    this.selPrdtDetails = null;
    if (!this.searchFrameForm.valid) {
      const target = document.querySelector('.ng-invalid');
      if (target) {
        target.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
      }
      this.commonService.validateAllFormFields(this.searchFrameForm);
      return false;
    }
    this.loadSelProductDetailsByTagNo(this.searchFrameForm.value.tagNo);
  }

  loadSelProductDetailsByTagNo(tagNo) {
    this.selPrdtDetails = null;
    this.httpGetSelPrdtDetailsSubs = this.navbarService.getSelPrdtDetailsByTagNoSearch(tagNo).subscribe(res => {
      if (res && res.data) {
          if (res.data.product) {
          this.selPrdtDetails = res.data.product;
          this.getSelCatDetailsByCatId(res.data.product.category.categoryId);
          if (res.data.store) {
            this.tagNoStoreDetails = res.data.store;
            // const store = {
            //   value: {
            //     _id: res.data.store.ecpUserId._id,
            //     ecpCust: res.data.store.ecpUserId.ecpCust,
            //     tagName: res.data.store.frame[0].tagName
            //   }
            // }
            // this.setStoreToPrdtAttr(0, store);
          }
        } else {
          this.notify.error('No product found with Tag No - ' + tagNo + '!!', '');
          // setTimeout(() => {
          //   if (this.isLoggedInUserAdmin) {
          //     this.router.navigate(['/admin/dashboard/product/create']);
          //   }
          //   if (this.isLoggedInUserECP) {
          //     this.router.navigate(['/ecpcust/dashboard/product/create']);
          //   }
          // }, 1000);
        }
      }
    }, (error) => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 406) {
          this.notify.error('Unable to load details !!', body.message);
        } else {
          this.notify.error('Something went wrong', error.statusText);
        }
      })
  }

  checkIfProductAddedToStore(i) {
    const control = <FormArray>this.productForm.controls['prdt_attributes'];
    if (!isNullOrUndefined(this.selPrdtDetails) && !isNullOrUndefined(this.tagNoStoreDetails)) {
      if (this.selPrdtDetails.prdt_attributes && this.selPrdtDetails.prdt_attributes.length > 0 ) {
        _.forEach(this.selPrdtDetails.prdt_attributes, (prdtAttr, index) => {
          if (prdtAttr.colour && prdtAttr.colour.store.length > 0 ) {
            // tslint:disable-next-line:max-line-length
            const storeIndex = prdtAttr.colour.store.map(function (x) { return x._id; }).indexOf(this.tagNoStoreDetails.ecpUserId._id);
            if (storeIndex > -1) {
              control['controls'][index]['canRemove'] = true;
            }
          }
        });
      }
    }
  }

  showImageViewPopUp(template, element) {
    this.selImageViewUrl = element;
    this.modalRef = this.modalService.show(template);
  }

  getSelCatDetailsByCatId(categoryId) {
    this.httpGetAllCatSubs = this.navbarService.getSelCatDetailsByCatId(categoryId).subscribe(res => {
      if (res && res.name) {
        const rootCatIndex = this.genderArr.map(function (x) { return x.toLowerCase(); }).indexOf(res.name.toLowerCase());
        if (rootCatIndex > -1) {
          this.productForm.patchValue({gender: this.genderArr[rootCatIndex]});
          this.loadAllCategoriesByGender({value: this.genderArr[rootCatIndex] });
        }
      }
    });
  }

  loadAllCategoriesByGender(selItem) {
    const payload = {
      conditions: {
        name: selItem.value
      }
    }
    this.httpGetAllCatSubs = this.navbarService.getAllCategoryList(payload).subscribe(res => {
      if (res && res.data.length > 0) {
        this.selCategory = res.data[0];
        this.setFormValues();
      }
    });
  }

  loadBrandListByChildCat(catName) {
    const payload = {
      conditions: {
        catName: catName
      },
      pagination: {}
    }
    this.httpGetBrandsByCatSubs = this.navbarService.getBrandsByCat(payload).subscribe(res => {
      if (res && res.data) {
        this.brandsList = res.data;
        // if (this.isEditMode) { // set selected brand
          const brandIdIndex = this.brandsList.map(
            function (x) { return x._id ? x._id : null; }).indexOf(this.selPrdtDetails.brand);
            if (brandIdIndex > -1) {
              this.selPrdtDetails['brandName'] =  this.brandsList[brandIdIndex].name;
            }
       // }
      }
    })
  }    

  setFormValues() {
    this.pricingForm.patchValue({
      pricing: {
        gkbPrice: this.selPrdtDetails.pricing.gkbPrice,
        actualPrice: this.selPrdtDetails.pricing.actualPrice,
        sellingPrice: this.selPrdtDetails.pricing.sellingPrice
      }
    });
    if (this.selCategory.childCategories && this.selCategory.childCategories.length > 0) {
      const childCatIdIndex = this.selCategory.childCategories.map(
        function (x) { return x._id ? x._id.categoryId : null; }).indexOf(this.selPrdtDetails.category.childCategories.categoryId);
      if (childCatIdIndex > -1) { // set selected child category
        this.loadBrandListByChildCat(this.selCategory.childCategories[childCatIdIndex]._id.name)
        this.selPrdtDetails['childCatName'] =  this.selCategory.childCategories[childCatIdIndex]._id.name
      }
      // set default product attributes
      const pcontrol = <FormArray>this.productForm.controls['prdt_attributes'];
      if (this.selPrdtDetails.prdt_attributes && this.selPrdtDetails.prdt_attributes.length > 0) {
        if (pcontrol) {
          pcontrol.controls = [];
        }
        _.forEach(this.selPrdtDetails.prdt_attributes, (prdAttr, index) => {
          pcontrol.push(this.createPrdtAttrForm());
          pcontrol['controls'][index]['isEdit'] = false;
          pcontrol['controls'][index]['isFrameMatching'] = false;
          pcontrol['controls'][index]['canRemove'] = false;
          pcontrol['controls'][index]['isChecked'] = false;
          pcontrol['controls'][index]['controls']['_id'].setValue(prdAttr._id);
          pcontrol['controls'][index]['controls']['prdAttributeId'].setValue(prdAttr.prdAttributeId ? prdAttr.prdAttributeId : '' );
          if (prdAttr.colour) {
            // tslint:disable-next-line:max-line-length
            pcontrol['controls'][index]['controls']['colour']['controls']['frameId'].setValue(prdAttr.colour.frameId ? prdAttr.colour.frameId : '');
            // set store
            // if (prdAttr.colour.store && prdAttr.colour.store.length > 0) {
            //   const storeControl = pcontrol['controls'][index]['controls']['colour']['controls']['store'];
            //   _.forEach(prdAttr.colour.store, (store, storeIndex) => {
            //     if (store._id && store._id !== '') {
            //       storeControl.push(this.createPrdtAttrStoreForm());
            //       storeControl['controls'][storeIndex]['controls']['_id'].setValue(store._id);
            //       storeControl['controls'][storeIndex]['controls']['cust_code'].setValue(store.cust_code);
            //     }
            //   })
            // }
            // tslint:disable-next-line:max-line-length
            pcontrol['controls'][index]['controls']['colour']['controls']['prdColourId'].setValue(prdAttr.colour.prdColourId ? prdAttr.colour.prdColourId : '');
            pcontrol['controls'][index]['controls']['colour']['controls']['SKU'].setValue(prdAttr.colour.SKU);
            // tslint:disable-next-line:max-line-length
            pcontrol['controls'][index]['controls']['colour']['controls']['value'].setValue(prdAttr.colour.value ? prdAttr.colour.value : '');
            pcontrol['controls'][index]['controls']['colour']['controls']['name'].setValue(prdAttr.colour.name ? prdAttr.colour.name : '');
            pcontrol['controls'][index]['controls']['colour']['controls']['isDefault'].setValue(prdAttr.colour.isDefault);
            pcontrol['controls'][index]['controls']['colour']['controls']['cover_img'].setValue(prdAttr.colour.cover_img);
            if (prdAttr.colour.media && prdAttr.colour.media.length > 0) {
              _.forEach(prdAttr.colour.media, (media, mediaIndex) => {
                if (media.url) {
                  pcontrol['controls'][index]['controls']['colour']['controls']['media']['controls'][mediaIndex].setValue(media);
                }
              });
            }
            if (prdAttr.colour.size) {
              _.forEach(prdAttr.colour.size, (size, j) => {
                pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']['_id'].setValue(size._id);
                pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']
                ['prdSizeId'].setValue(size.prdSizeId);
                pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']
                ['quantity'].setValue(size.quantity);
                pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']['SKU'].setValue(size.SKU);
                pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']['isDefault']
                .setValue(size.isDefault);
                pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']['isSelected']
                .setValue(size.isSelected);
              });
            }
          }
        });
      }
    } 
  }

  setProductColourMedia(key, fileInput: any, index, mediaIndex) {
    if (fileInput && fileInput.target.files[0]) {
      const control = <FormArray>this.productForm.controls['prdt_attributes'];
      if (control && control.controls.length > 0) {
        this.loader.show();
        const formData = new FormData();
        formData.append('content', fileInput.target.files[0]);
        this.httpPrdtImageUploadSubs = this.navbarService.productImageUpload(formData).subscribe(res => {
          if (res) {
          // const res = {
          //   'name' : "l-m242bk2t-fastrack-original-imafkz5mvfqu3bvv.jpeg",
          //   'type' : "Image",
          //   'url' : "https://xm-customer.s3.ap-south-1.amazonaws.com/images/1575892721287G8jsBnHD.jpeg"
          // }
            if (key === 'cover_img') {
              control['controls'][index]['controls']['colour']['controls'][key].setValue(res);
            }
            if (key === 'media') {
              control['controls'][index]['controls']['colour']['controls'][key]['controls'][mediaIndex].setValue(res);
            }
              this.notify.success('Image Uploaded Successfully !!', '');
              this.loader.hide();
          } else {
            this.loader.hide();
          }
        }, (error) => {
          this.loader.hide();
          let body;
          if (error._body) {
            body = JSON.parse(error._body);
          }
          if (body.status === 406) {
            this.notify.error('Unable to Upload Image!!', body.message);
          } else {
            this.notify.error('Something went wrong', error.statusText);
          }
        });
      }
    }
  }

  acceptNumOnly(evt) {
    return this.commonService.acceptNumOnly(evt);
  }

  addDynamicForm(value) {
    const control = <FormArray>this.productForm.controls[value];
    switch (value) {
      case 'prdt_attributes':
        if (control.controls.length <= 2) {
          control.push(this.createPrdtAttrForm());
        }
    }
    control['controls'][control.controls.length - 1]['isEdit'] = true;  // default edit
    control['controls'][control.controls.length - 1]['isChecked'] = false;  // default checked
    control['controls'][control.controls.length - 1]['isFrameMatching'] = false;
    control['controls'][control.controls.length - 1]['canRemove'] = false;
  }

  removeDynamicForm(value, index) {
    const control = <FormArray>this.productForm.controls[value];
    return control.removeAt(index);
  }

  validateSpecificFormArray(i) {
    const control = <FormArray>this.productForm.controls['prdt_attributes'];
    if (control && control.controls.length > 0) {
        if (control.at(i)['controls']['colour'] && control.at(i)['controls']['colour']['controls']['value'] &&
        !control.at(i)['controls']['colour']['controls']['value'].valid) {
          control.at(i)['controls']['colour']['controls']['value'].markAsTouched();
        }
        // pending size validation
        for (let j = 0 ; j < control.at(i)['controls']['colour']['controls']['size'].controls.length; j++) {
          const colourControl = control.at(i)['controls']['colour']['controls']['size'].controls;
          if (colourControl[j]['controls']['value'] &&
          !colourControl[j]['controls']['value'].valid) {
            colourControl[j]['controls']['value'].markAsTouched();
          }
          if (colourControl[j]['controls']['quantity'] &&
          !colourControl[j]['controls']['quantity'].valid) {
            colourControl[j]['controls']['quantity'].markAsTouched();
          }
    }
  }
}

setProductSize(event, sizeVal, i, j) {
  const control = <FormArray>this.productForm.controls['prdt_attributes'];
  if (event.target.checked) {
    control['controls'][i]['controls']['colour']['controls']['size']['controls'][j]['controls']['isSelected'].setValue(true);
    control['controls'][i]['controls']['colour']['controls']['size']['controls'][j]['controls']['quantity'].markAsTouched();
  } else {
    control['controls'][i]['controls']['colour']['controls']['size']['controls'][j]['controls']['isSelected'].setValue(false);
  }
}

toggleFrameMatching(i, event) {
  const control = <FormArray>this.productForm.controls['prdt_attributes'];
    if (event.target.checked) {
      control['controls'][i]['isFrameMatching'] = true;
    } else {
      control['controls'][i]['isFrameMatching'] = false;
    }
  this.checkIfProductAddedToStore(i);
}

setStoreToPrdtAttr(i, selItem) {
  const control = <FormArray>this.productForm.controls['prdt_attributes'];
  if (selItem.value) {
      const storeControl = control['controls'][i]['controls']['colour']['controls']['store'];
      if (storeControl) {
        storeControl.controls = [];
      }
      // _.forEach(selItem.value, (store, storeIndex) => {
      //   const storeIdIndex = storeControl.controls.map(function (x) { return x.controls._id.value; }).indexOf(selItem.value._id);
      //   if (selItem.value._id && selItem.value._id !== '') {
      //     if (storeIdIndex <= -1) { // if store id doesnt exist, then add
      //       storeControl.push(this.createPrdtAttrStoreForm());
      //       storeControl['controls'][storeIndex]['controls']['_id'].setValue(store._id);
      //       storeControl['controls'][storeIndex]['controls']['cust_code'].setValue(store.ecpCust.cust_code);
      //     } else { // update
      //       storeControl['controls'][storeIndex]['controls']['_id'].setValue(store._id);
      //       storeControl['controls'][storeIndex]['controls']['cust_code'].setValue(store.ecpCust.cust_code);
      //     }
      //   }
      // });
      storeControl.push(this.createPrdtAttrStoreForm());
      storeControl['controls'][i]['controls']['_id'].setValue(selItem.value._id);
      storeControl['controls'][i]['controls']['cust_code'].setValue(selItem.value.ecpCust.cust_code);
      storeControl['controls'][i]['controls']['store_code'].setValue(selItem.value.ecpUserId.store_code);
      storeControl['controls'][i]['controls']['tagName'].setValue(selItem.value.tagName);
  }
  control['controls'][i]['isChecked'] = false;
}


validateAndSetStore(index) {
  if (!this.tagNoStoreDetails) {
    return this.notify.error('Store details isnt proper !!', '');
  }
  const control = <FormArray>this.productForm.controls['prdt_attributes'];
    const storeControl = control['controls'][index]['controls']['colour']['controls']['store'];
    if (storeControl) {
      storeControl.controls = [];
    }
    storeControl.push(this.createPrdtAttrStoreForm());
    storeControl['controls'][index]['controls']['_id'].setValue(this.tagNoStoreDetails.ecpUserId._id);
    storeControl['controls'][index]['controls']['cust_code'].setValue(this.tagNoStoreDetails.ecpUserId.ecpCust.cust_code);
    storeControl['controls'][index]['controls']['store_code'].setValue(this.tagNoStoreDetails.ecpUserId.store_code);
    storeControl['controls'][index]['controls']['tagName'].setValue(this.tagNoStoreDetails.frame[0].tagName);
}

addToSelStore(event, index) {
  this.validateAndSetStore(index);
  const control = <FormArray>this.productForm.controls['prdt_attributes'];
  // if (event.target.checked) { // insert
      if (!control['controls'][index].valid) {
        const target = document.querySelector('.ng-invalid');
        if (target) {
          target.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
        }
        control['controls'][index]['isChecked'] = false;
        // event.target.checked = false;
        this.validateSpecificFormArray(index);
        return false;
    }
    if (control['controls'][index].value.length <= 0) {
      // event.target.checked = false;
      return this.notify.warn('Please select atleast one product colour with size details !!', '');
    }
    // prdt_attributes form validation for store
    if (control['controls'][index].value['colour']['store'].length <= 0) {
      // event.target.checked = false;
      return this.notify.error('Please select store for',
      control['controls'][index].value.colour.value);
    }
    // size validation
    if (control['controls'][index]['controls']['colour']['controls']['size'].controls.length > 0) {
      for (let j = 0 ; j < control['controls'][index]['controls']['colour']['controls']['size'].controls.length; j++) {
        const sizeControl = control['controls'][index]['controls']['colour']['controls']['size'].controls;
        const selSizeCount = sizeControl.filter((v) => (v.controls.isSelected.value === true)).length;
        if (selSizeCount <= 0) {
          // event.target.checked = false;
          return this.notify.error('Please select atleast one size !!',
          control['controls'][index].value.colour.value);
        }
      }
    }
    this.confirmation.showDialog('',
    status ? 'Are you sure ?' : 'Are you sure add product to store ' + this.tagNoStoreDetails.ecpUserId.ecpCust.cust_code + ' ?')
    .subscribe(obj => {
      if (obj) {
        // control['controls'][index]['isChecked'] = true;
        this.addPrdtAttributeToMyStore(control['controls'][index]['controls']['prdAttributeId'].value, control['controls'][index].value);
      }
      // else {
      //   event.target.checked = false;
      // }
    });
  // } else { // delete
  // this.removeFromStore(index);
  // }
}

removeFromStore(event, index) {
  this.validateAndSetStore(index);
  const control = <FormArray>this.productForm.controls['prdt_attributes'];
  // control['controls'][index]['isChecked'] = false;
  const prdtAttrId = control['controls'][index]['controls']['prdAttributeId'].value;
  if (prdtAttrId && prdtAttrId !== '') {
    this.confirmation.showDialog('',
    status ? 'Are you sure ?' : 'Are you sure remove product from store ' + this.tagNoStoreDetails.ecpUserId.ecpCust.cust_code + ' ?')
    .subscribe(obj => {
      if (obj) {
       // control['controls'][index]['isChecked'] = true;
        this.removeSelStoreFromPrdtAttr(control['controls'][index]['controls']['prdAttributeId'].value, control['controls'][index].value);
      }
      //  else {
      //   event.target.checked = false;
      // }
    });
  }
}

 updateGkbprice() {
  if (!this.pricingForm.valid) {
    const target = document.querySelector('.ng-invalid');
    if (target) {
      target.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
    }
    this.commonService.validateAllFormFields(this.pricingForm);
    return false;
  }
  this.loader.show();
  this.httpClonePrdtToStoreSubs = this.navbarService.updateGKBPrice(this.selPrdtDetails.productId,
    this.pricingForm.value).subscribe(res => {
      this.loader.hide();
      if (res) {
        this.pricingForm.reset();
        this.productForm.reset();
        this.notify.success('Product GKB price updated successfully !!', '');
        this.selPrdtDetails = null;
        this.searchFrameForm.reset();
      }
    }, (error) => {
      this.loader.hide();
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 406) {
        this.notify.error('Unable to Update price!!', body.message);
      } else {
        this.notify.error('Something went wrong', error.statusText);
      }
    })
 }

  addPrdtAttributeToMyStore(prdAttributeId, selAttr) {
    this.loader.show();
    const payload = {
      conditions: {
        productId: this.selPrdtDetails.productId,
        prdAttributeId: prdAttributeId,
      },
      updateFields: selAttr
    }
    this.httpClonePrdtToStoreSubs = this.navbarService.addPrdtAttributeToStore(payload).subscribe(res => {
      this.loader.hide();
      if (res) {
        this.productForm.reset();
        this.notify.success('Product Added to Store SuccessFully !!', '');
        this.selPrdtDetails = null;
        this.searchFrameForm.reset();
      }
    }, (error) => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 406) {
          this.notify.error('Unable to Add Product to ur store!!', body.message);
        } else {
          this.notify.error('Something went wrong', body.message);
        }
    })
  }

  removeSelStoreFromPrdtAttr(prdAttributeId, selAttr) {
    this.loader.show();
    const payload = {
        productId: this.selPrdtDetails.productId,
        prdAttributeId: prdAttributeId,
        cust_code: selAttr.colour.store[0].cust_code
    }
    this.httpClonePrdtToStoreSubs = this.navbarService.removeSelStoreFromPrdtAttr(payload).subscribe(res => {
      this.loader.hide();
      if (res) {
        this.productForm.reset();
        this.notify.success('Removed Store from product SuccessFully !!', '');
        this.selPrdtDetails = null;
        this.searchFrameForm.reset();
      }
    }, (error) => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 406) {
          this.notify.error('Unable to Remove Product to ur store!!', body.message);
        } else {
          this.notify.error('Something went wrong', body.message);
        }
    })
  }

  ngOnDestroy() {
    if (this.httpGetAllCatSubs) {
      this.httpGetAllCatSubs.unsubscribe();
    }
    if (this.httpGetBrandsByCatSubs) {
      this.httpGetBrandsByCatSubs.unsubscribe();
    }
    if (this.httpPrdtImageUploadSubs) {
      this.httpPrdtImageUploadSubs.unsubscribe();
    }
    if (this.httpGetAllUserSubs) {
      this.httpGetAllUserSubs.unsubscribe();
    }
    if (this.httpGetSelPrdtDetailsSubs) {
      this.httpGetSelPrdtDetailsSubs.unsubscribe();
    }
    if (this.httpClonePrdtToStoreSubs) {
      this.httpClonePrdtToStoreSubs.unsubscribe();
    }
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
}
