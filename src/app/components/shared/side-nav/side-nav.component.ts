import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { NavbarService } from 'app/components/shared/navbar.service';
import * as Constant from '../../../constants/app.constants';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss',
  '../../customer/side-nav/side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  @ViewChild('sidenav', { static: true }) sideNav: MatSidenav;
  sidenavWidth = 4;
  ngStyle: string;
  overlay = false
  navBarMenuList = [];
  userRole;
  userObj;

  constructor(public authService: AuthenticationService,
  private navbarService: NavbarService) { }

  ngOnInit() {
    this.userObj = this.authService.getAuthDeatils();
    this.getNavBarMenu();
    this.ngStyle = 'this.sidenavWidth = 0';
  }
  getNavBarMenu() {
    const user = this.authService.getAuthDeatils();
    this.userRole = user.role;    
    this.setNavBarList();
  }

  setNavBarList() {
    switch (this.userRole) {
      case Constant.ROLE_SUPER_ADMIN: this.navBarMenuList = this.navbarService.adminNavBarList;
        break;
      case Constant.ROLE_ADMIN: this.navBarMenuList = this.navbarService.adminNavBarList;
        break;
      case Constant.ROLE_ECP_CUSTOMER: this.navBarMenuList = this.navbarService.ecpNavBarList;
         break;
      case Constant.ROLE_FRAME_COLLECTOR: this.navBarMenuList = this.navbarService.frameNavBarCollectList;
      break;
      case Constant.ROLE_FRAME_MODERATOR: this.navBarMenuList = this.navbarService.frameModeratorNavBarList;
      break;
    }
  }
  increase() {
    this.sidenavWidth = 15;
    this.overlay = true;
  }
  decrease() {
    this.sidenavWidth = 4;
    this.overlay = false;
  }
  sidenavToggle() {
    this.ngStyle = 'this.sidenavWidth = 15';
  }
  logout() {
    this.authService.logout();
  }
  closeOnClick() {
    this.sidenavWidth = 4;
    this.overlay = false;
  }
}
