/**
 * Authentication module
 */

import {NgModule} from '@angular/core';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from 'app/components/authentication/authentication.routes';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
  providers: [AuthenticationService],
  imports : [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AuthRoutingModule,
  ],
  declarations : []

})
export class AuthenticationModule {}
