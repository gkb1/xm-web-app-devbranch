import { MaterialDesignElementsModule } from './../../shared/material-design-element/material-design-element.module';
import { ReactiveFormsModule, FormsModule, NgForm, NgModel } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { userRoutingModule } from 'app/components/authentication/user/user.routes';
import { CustomValidatorService } from 'app/services/customValidator.service';
import { InputTrimModule } from 'ng2-trim-directive';
import { MatFormFieldModule } from '@angular/material';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  imports: [
    CommonModule,
    userRoutingModule,
    ReactiveFormsModule,
    MaterialDesignElementsModule,
    FormsModule,
    InputTrimModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [
    LoginComponent,
    SignupComponent
  ],
  exports: [
    MatInputModule
  ],
  providers: [
    NgForm,
    NgModel,
    CustomValidatorService
  ]
})
export class UserModule { }
