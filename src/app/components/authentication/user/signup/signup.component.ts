import { CustomValidatorService } from './../../../../services/customValidator.service';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import {Component, OnDestroy, OnInit} from '@angular/core';
import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {Title} from '@angular/platform-browser';
import {NotificationService} from '../../../shared/notification/notification.service';
import {LoadingBarService} from '../../../shared/loading-bar/loading-bar.service';
import {ConfirmationService} from '../../../shared/confirmation/confirmation.service';
import {Subscription} from 'rxjs/Subscription';
import { CommonService } from 'app/services/common.service';
import * as Constant from '../../../../constants/app.constants';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, OnDestroy {
  duplicateEmail: boolean;
  password: any;
  confirmPass: any;
  httpRegisterSubscription: Subscription;
  selectedRole: any;
  selectedGender: any;
  mobileNumber: string;
  userRolesList = [];
  addressForm: FormGroup;
  signupForm: FormGroup;
  payload = { conditions: {}};
  genderList = Constant.GENDER_LOOKUP;

  constructor(
    private titleService: Title,
    private _fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private confirm: ConfirmationService,
    private loader: LoadingBarService,
    private commonService: CommonService,
    private notify: NotificationService,
    private customValidator: CustomValidatorService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.userRolesList = Constant.LOOKUP_USER_ROLE;
    this.titleService.setTitle('Signup');
    this.userRolesList = Constant.LOOKUP_USER_ROLE;
    this.initFormViewControls();
  }

  initFormViewControls() {
    this.signupForm = this._fb.group({
      name: ['', [Validators.required, Validators.pattern(/^[a-zA-Z\s.]+$/)]],
      email: ['', [Validators.required, Validators.compose([<any>Validators.required, this.customValidator.emailFormat])]],
      gender: ['',Validators.required],
      mobile: ['', [Validators.required, Validators.pattern(/^[0-9-() +]*$/)]],
      role: [''],
    });
  }
  createAccount() {
    switch (this.route.snapshot.url.join('')) {
      case 'fsignup': 
        this.signupForm.controls.role.setValue(Constant.ROLE_FRAME_COLLECTOR);
        break;
      case 'fmsignup': 
        this.signupForm.controls.role.setValue(Constant.ROLE_FRAME_MODERATOR);
        break;
      case 'signup': 
        this.signupForm.controls.role.setValue(Constant.ROLE_CUSTOMER);
        break
    }
    if (!this.signupForm.valid) {
      this.commonService.validateAllFormFields(this.signupForm);
      return false;
    }
    this.loader.show();
    this.httpRegisterSubscription = this.authService.registerUser(this.signupForm.value)
    .subscribe(res => {
      this.loader.hide();
      if (res) {
        this.notify.success('Your account is created', 'Please login to continue.', true);
        this.router.navigate(['/auth']);
      } else if (res.isExist) {
        this.notify.error(res.msg, 'Please try again');
        this.signupForm.controls['email'].setErrors({setErrors: true});
      } else {
         this.notify.error('Cannot Create account', 'Please contact administrator');
      }
    }, (error) => {
      this.loader.hide();
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 406 || body.status === 409) {
        this.notify.error('Unable to Register!!', body.message);
      } else {
        this.notify.error('Something went wrong', error.statusText);
      }
    });
  }

  ngOnDestroy() {
    if (this.httpRegisterSubscription) {
      this.httpRegisterSubscription.unsubscribe();
    }
  }

}
