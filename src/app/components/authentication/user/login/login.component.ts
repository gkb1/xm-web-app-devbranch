import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { Component, OnDestroy, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { NotificationService } from '../../../shared/notification/notification.service';
import { LoadingBarService } from '../../../shared/loading-bar/loading-bar.service';
import { ConfirmationService } from '../../../shared/confirmation/confirmation.service';
import { CommonService } from 'app/services/common.service';
import { CustomValidatorService } from 'app/services/customValidator.service';
import { isNullOrUndefined } from 'util';
import * as Constant from '../../../../constants/app.constants';
import { FormGroupService } from 'app/services/form-group.service';
import { Subscription } from 'rxjs/Subscription';
import { MyErrorStateMatcher } from 'app/constants/errorStateMatcher';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy{
  loginForm: FormGroup;
  showOtpView = false;
  httpAuthenticateSubscription: Subscription;
  otpText = 'Generate OTP';

  matcher: MyErrorStateMatcher;
  phoneNumberRegexPattern = new RegExp(/^[0][1-9]\d{9}$|^[1-9]\d{9}$/);
  constructor(
    private titleService: Title,
    private _fb: FormBuilder,
    private authService: AuthenticationService,
    private router: Router,
    private loader: LoadingBarService,
    private customValidator: CustomValidatorService,
    private notify: NotificationService,
    private commonService: CommonService,
  ) { }

  ngOnInit() {
    this.matcher = new MyErrorStateMatcher();
    this.titleService.setTitle('Login');
    this.initLoginForm();
  }

  initLoginForm() {
    this.loginForm = this._fb.group({
      // mobile: ['', Validators.compose([<any>Validators.required, this.customValidator.mobileFormat])],
      mobile: [
        '',
        {
          validators: [
        Validators.required,
        Validators.pattern(this.phoneNumberRegexPattern)
      ]
    }
  ],
      otp: ['', ]
    });
  }

  generateOtp() {
    // if (!this.loginForm.valid) {
    //   this.commonService.validateAllFormFields(this.loginForm);
    //   return false;
    // }
    this.loader.show();
    this.httpAuthenticateSubscription = this.authService.generateOTP({mobile: this.loginForm.value.mobile})
      .subscribe(data => {
        // console.log(data);
        this.loader.hide();
      if (data) {
        this.notify.success(data.message, '');
        this.otpText = 'Resend OTP';
        this.showOtpView = true;
        this.loginForm.controls.otp.setValidators(
          [<any>Validators.required, Validators.compose([<any>Validators.required,
            this.customValidator.otpFormat])]);
        this.loginForm.controls.otp.updateValueAndValidity();
      }
    }, (error) => {
        // console.log('error', error);
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 406) {
          this.notify.error('Unable to Send OTP !!', body.message);
        } else {
          this.notify.error('Something went wrong', error.statusText);
        }
      });
  }

  verifyOtpAndLogin() {
    if (!(this.loginForm.value.otp) || /^\s+$/.test(this.loginForm.value.otp)) {
      return this.notify.error('Please Enter Otp', '');
    }
    if (!this.loginForm.valid) {
      this.commonService.validateAllFormFields(this.loginForm);
      return false;
    }
    this.loader.show();
    this.httpAuthenticateSubscription = this.authService.authenticateUser(this.loginForm.value)
      .subscribe(data => {
        this.loader.hide();
        if (data) {
          // PENDING handle role
          this.authService.storeUserData(data.token, data.user);
          const role = data.user.role;
          switch (role) {
            case Constant.ROLE_SUPER_ADMIN:
              this.router.navigate(['/admin/dashboard']);
              break;
            case Constant.ROLE_ADMIN:
              this.router.navigate(['/admin/dashboard']);
              break;
            case Constant.ROLE_FRAME_COLLECTOR:
              this.router.navigate(['/framecollector/dashboard']);
              break;
            case Constant.ROLE_FRAME_MODERATOR:
              this.router.navigate(['/framemoderator/dashboard']);
              break;
            case Constant.ROLE_ECP_CUSTOMER:
              this.router.navigate(['/ecpcust/dashboard']);
              break;
            case Constant.ROLE_CUSTOMER:
            this.router.navigate(['/']);
            break;
          }
          // if (role === Constant.ROLE_SUPER_ADMIN || role === Constant.ROLE_ADMIN) {
          //   this.router.navigate(['/admin/dashboard']);
          // } else if (this.authService.isLoggedInUserECPCustomer()) {
          //   this.router.navigate(['/c']);
          // } else if (this.authService.isLoggedInUserFrameCollector()) {
          //   this.router.navigate(['/framecollector']);
          // } else if (this.authService.isLoggedInUserFrameModerator()) {
          //   this.router.navigate(['/framemoderator']);
          // } else {
          //   this.router.navigate(['/']);
          // }
        }
      }, (error) => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 406) {
          this.notify.error('Unable to Login!!', body.message);
        } else {
          this.notify.error('Something went wrong', error.statusText);
        }
      });
  }
//   keytab(event){
//     let element = event.srcElement.nextElementSibling; // get the sibling element

//     if(element == null)  // check if its null
//         return;
//     else
//         element.focus();   // focus if not null


// }
numberOnly(event): boolean {
  const charCode = (event.which) ? event.which : event.keyCode;
  if (charCode > 31 && (charCode < 48 || charCode > 57)) {
    return false;
  }
  return true;
}
  ngOnDestroy() {
    if (this.httpAuthenticateSubscription) {
      this.httpAuthenticateSubscription.unsubscribe();
    }
  }

}


