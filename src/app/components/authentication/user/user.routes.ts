import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { LoginComponent } from 'app/components/authentication/user/login/login.component';
import { SignupComponent } from 'app/components/authentication/user/signup/signup.component';

const userRoutes: Routes = [
    { path: '', component: LoginComponent},
    { path: 'fsignup', component: SignupComponent},   // frame collector role
    { path: 'fmsignup', component: SignupComponent}, // frame moderator role
    { path: 'signup', component: SignupComponent},  // cust role
  ];

  export const userRoutingModule: ModuleWithProviders = RouterModule.forChild(userRoutes);
