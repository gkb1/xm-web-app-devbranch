// import { HmacHttpClient } from './../../utils/app-token-http-client';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import * as AppUtils from '../../../app/utils/app.utils';
import { UserRole } from 'app/models/userRole';
import { Auth } from 'app/models/auth';
import { AbstractControl } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { tokenNotExpired, JwtHelper } from 'angular2-jwt';
import { Router } from '@angular/router';
import * as Constant from '../../constants/app.constants';
import { isNullOrUndefined } from 'util';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class AuthenticationService extends Subject<any> {
  authUser: Auth;
  public userRoles: Subject<any> = new Subject<any>();
  isNavBarCollapsed = new Subject<Boolean>();

  constructor(private http: Http,
    private router: Router) {
    super();
  }

  authenticateUser(payload) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + Constant.VERIFY_OTP_LOGIN_URL + payload.mobile + '/' + payload.otp)
      .map(res => res.json());
  }

  storeUserData(token, user) {
    this.authUser = new Auth();
    this.authUser = user;
    if (!isNullOrUndefined(this.authUser.role)) {
      this.userRoles.next(this.authUser.role);
    }
    localStorage.setItem(AppUtils.STORAGE_ACCOUNT_TOKEN, token);
    localStorage.setItem(AppUtils.STORAGE_USER_DETAILS, JSON.stringify(this.authUser));
    this.loginSuccess(this.authUser);
  }

  loggedIn(): boolean {
    const token = localStorage.getItem(AppUtils.STORAGE_ACCOUNT_TOKEN);
    return tokenNotExpired(AppUtils.STORAGE_ACCOUNT_TOKEN, token);
  }

  loginSuccess(auth: Auth) {
    this.authUser = auth;
    if (auth) {
      auth.authenticated = true;
      super.next(auth);
    }
  }

  isLoggedInUserSuperAdmin() {
    const user = this.getAuthDeatils();
    if (user.role === Constant.ROLE_SUPER_ADMIN) { // super admin login
      return true;
    } else {
      return false;
    }
  }

  isLoggedInUserAdmin() {
    const user = this.getAuthDeatils();
    if (user.role === Constant.ROLE_SUPER_ADMIN || user.role === Constant.ROLE_ADMIN) { // admin login
      return true;
    } else {
      return false;
    }
  }

  isLoggedInUserCustomer() {
      const user = this.getAuthDeatils();
      if (user.role === Constant.ROLE_CUSTOMER) { // customer login
        return true;
      } else {
        return false;
      }
  }

  isLoggedInUserECPCustomer() {
    const user = this.getAuthDeatils();
    if (user.role === Constant.ROLE_ECP_CUSTOMER) { // ecp customer login (optician/ store)
      return true;
    } else {
      return false;
    }
  }

  isLoggedInUserFrameCollector() {
    const user = this.getAuthDeatils();
    if (user.role === Constant.ROLE_FRAME_COLLECTOR) { // frame collector login 
      return true;
    } else {
      return false;
    }
  }

  isLoggedInUserPhotographer() {
    const user = this.getAuthDeatils();
    if (user.role === Constant.ROLE_PHOTOGRAPHER) { // photographer login 
      return true;
    } else {
      return false;
    }
  }

  isLoggedInUserFrameModerator() {
    const user = this.getAuthDeatils();
    if (user.role === Constant.ROLE_FRAME_MODERATOR) { // frame moderator login 
      return true;
    } else {
      return false;
    }
  }

  getAuthDeatils(): Auth {
    // this.authUser = (this.authUser) ? this.authUser : new Auth(JSON.parse(localStorage.getItem(AppUtils.STORAGE_USER_DETAILS)));
    this.authUser = JSON.parse(localStorage.getItem(AppUtils.STORAGE_USER_DETAILS));
    return this.authUser;
  }

  set authdetails(value: Auth) {
    this.authUser = value;
  }

  generateOTP(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + Constant.GENERATE_OTP_URL, payload)
      .map(res => res.json());
  }

  forgotPassword(payload) {
    // return this.http.post(AppUtils.BACKEND_API_ROOT_URL + Constant.FORGOT_PWD_URL, payload)
    //   .map(res => res.json());
  }

  resetPassword(resetPassword, resetToken) {
    // console.log(resetPassword, resetToken);
    // return this.http.post(AppUtils.BACKEND_API_ROOT_URL + Constant.RESET_PWD_URL,
    //   { 'password': resetPassword, 'otp': resetToken })
    //   .map(res => res.json())
  }

  getUserDetailsByUserId(userId) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + Constant.GET_USER_DETAILS_BY_ID_URL + userId)
    .map(res => res.json());
  }


  logout() {
    this.authUser = null;
    if (this.authUser) {
      this.authUser.authenticated = false;
      this.userRoles.complete();
      super.next(this.authUser);
    }
    this.clearStorage();
    this.router.navigate(['/']);
  }

  registerUser(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + Constant.GET_USER_REGISTERED, payload)
    .map(res => res.json());
  }

  clearStorage() {
    localStorage.removeItem(AppUtils.STORAGE_ACCOUNT_TOKEN);
    localStorage.removeItem(AppUtils.STORAGE_USER_DETAILS);
  }
}
