import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameModeratorMainComponent } from './frame-moderator-main.component';

describe('FrameModeratorMainComponent', () => {
  let component: FrameModeratorMainComponent;
  let fixture: ComponentFixture<FrameModeratorMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameModeratorMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameModeratorMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
