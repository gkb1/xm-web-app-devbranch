import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { RoleGuardService } from 'app/security/role-guard.service';
import * as Constant from './../../constants/app.constants';
import { FrameCollectorMainComponent } from 'app/components/frame-collector/frame-collector-main/frame-collector-main.component';
import { FrameModeratorMainComponent } from 'app/components/frame-moderator/frame-moderator-main/frame-moderator-main.component';
import { FrameSearchComponent } from 'app/components/shared/frame-search/frame-search.component';

const routes: Routes = [
    {
        path: 'dashboard',  canActivate: [AuthGuard], component: FrameModeratorMainComponent,  children: [
            { path: 'searchFrame',  canActivate: [RoleGuardService],
                data: {
                    expectedRole: [Constant.ROLE_FRAME_MODERATOR]
                },
                component: FrameSearchComponent
        },
        ]
    },
   { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

export const FrameModeratorRoutingModule = RouterModule.forChild(routes);
