import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrameModeratorMainComponent } from './frame-moderator-main/frame-moderator-main.component';
import { SharedModule } from 'app/components/shared/shared.module';
import { NavbarService } from 'app/components/shared/navbar.service';
import { FrameModeratorRoutingModule } from 'app/components/frame-moderator/frame-moderator.routes';
import { FrameModeratorService } from 'app/components/frame-moderator/frame-moderator.service';



@NgModule({
  declarations: [
    FrameModeratorMainComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FrameModeratorRoutingModule
  ],
  providers: [
    NavbarService,
    FrameModeratorService
  ],
})
export class FrameModeratorModule { }
