import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrandMainComponent } from './brand-main/brand-main.component';
import { BrandsListComponent } from './brands-list/brands-list.component';
import { BrandCreateUpdateComponent } from './brand-create-update/brand-create-update.component';
import { BrandRoutingModule } from 'app/components/admin/brand/brand.route';
import { MatCardModule } from '@angular/material';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';
import { BrandService } from 'app/components/admin/brand/brand.service';
@NgModule({
  declarations: [
    BrandMainComponent, 
    BrandsListComponent, 
    BrandCreateUpdateComponent
  ],
  imports: [
    CommonModule,
    BrandRoutingModule,
    FormsModule,
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatIconModule
  ],
  exports: [
    MatCardModule,
    MatButtonModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule
  ],
  providers: [
    BrandService
  ]
})
export class BrandModule { }
