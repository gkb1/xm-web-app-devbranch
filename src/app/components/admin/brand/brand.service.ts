import { Injectable } from '@angular/core';
import * as AppUtils from '../../../utils/app.utils';
import { Http } from '@angular/http';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class BrandService {
  private instantUpdate$ = new Subject<void>();
  constructor(private http: Http) { }

  get instantUpdates$() {
    return this.instantUpdate$;
  }

  createBrand(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.CREATE_BRAND, payload)
      .map(res => res.json());
  }

  getAllBrandList(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_BRANDS_BY_CONDN_URL, payload)
      .map(res => res.json());
  }

  getBrandByNameAndId(payload) {
    const query = `?brandId=${payload['brandId']}&name=${payload['brandName']}`
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_BRAND_BY_NAME_AND_ID + query)
      .map(res => res.json());
  }

  updateBrandByNameAndId(payload, data) {
    const query = `?brandId=${payload['brandId']}&name=${payload['brandName']}`
    return this.http.put(AppUtils.BACKEND_API_ROOT_URL + AppUtils.UPDATE_BRAND_BY_NAME_AND_ID + query, data)
      .map(res => res.json());
  }

  deleteBrandByNameAndId(payload) {
    const query = `?brandId=${payload['brandId']}&name=${payload['name']}`
    return this.http.delete(AppUtils.BACKEND_API_ROOT_URL + AppUtils.DELETE_BRAND_BY_NAME_AND_ID + query)
      .pipe(tap(() => {
        this.instantUpdate$.next();
      })
      );
  }
}
