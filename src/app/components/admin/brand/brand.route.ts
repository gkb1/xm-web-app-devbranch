import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { BrandMainComponent } from 'app/components/admin/brand/brand-main/brand-main.component';
import { BrandCreateUpdateComponent } from 'app/components/admin/brand/brand-create-update/brand-create-update.component';
import { BrandsListComponent } from 'app/components/admin/brand/brands-list/brands-list.component';

const brandRoutes: Routes = [
    {
        path: '', canActivate: [AuthGuard], component: BrandMainComponent,  children: [
            { path : '', component: BrandsListComponent },
            { path: 'create', component: BrandCreateUpdateComponent},
            { path: 'item/:brandId/:brandName', component: BrandCreateUpdateComponent},
        ]
    },
   { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const BrandRoutingModule = RouterModule.forChild(brandRoutes);
