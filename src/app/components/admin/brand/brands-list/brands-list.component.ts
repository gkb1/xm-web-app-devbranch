import { Component, OnInit, OnDestroy } from '@angular/core';
import { BrandService } from '../brand.service';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { Subscription } from 'rxjs';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ConfirmationService } from 'app/components/shared/confirmation/confirmation.service';
import { AuthenticationService } from 'app/components/authentication/authentication.service';

@Component({
  selector: 'app-brands-list',
  templateUrl: './brands-list.component.html',
  styleUrls: ['./brands-list.component.scss']
})
export class BrandsListComponent implements OnInit, OnDestroy {
  page = 0;
  limit = 100;
  payload = { conditions: {}, pagination: {} }
  brandList: any = [];
  httpGetAllBrandsSubs: Subscription;
  httpDeleteSub: Subscription;
  isAdmin = this.authServices.isLoggedInUserAdmin();
  isFrameCollector = this.authServices.isLoggedInUserFrameCollector();

  constructor(private brandService: BrandService,
    private notify: NotificationService,
    private loader: LoadingBarService,
    private route: ActivatedRoute,
    private confirmation: ConfirmationService,
    private authServices: AuthenticationService,
    private router: Router) { }

  ngOnInit() {
    this.loadBrandList();
  }

  loadBrandList() {
    this.httpGetAllBrandsSubs = this.brandService.getAllBrandList(this.payload).subscribe(res => {
      if (res && res.data) {
        this.brandList = res.data;
      }
    }, (error) => {
      this.loader.hide();
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 406) {
        this.notify.error('Unable to load!!', body.message);
      } else {
        this.notify.error('Something went wrong', error.statusText);
      }
    });
  }

  deleteBrand(brand) {
    this.confirmation.showDialog('',
      status ? 'Are you sure ?' : 'Are you sure you want to delete ' + brand.name + '?')
      .subscribe(obj => {
        if (obj) {
          const payload = { brandId: brand.brandId, name: brand.name }
          this.httpDeleteSub = this.brandService.deleteBrandByNameAndId(payload).subscribe((res: any) => {
            if (res) {
              this.brandList = [];
              this.loadBrandList();
              res = res.json();
              this.notify.success('Success!!', res.message);
            } else {
              this.notify.error('Unable to Update Examiner Status, Please try again later!!', '');
            }
          }, (error) => {
            this.loader.hide();
            let body;
            if (error._body) {
              body = JSON.parse(error._body);
            }
            if (body.status === 406 || body.status === 409) {
              this.notify.error('Unable to Update product status, Please try again later!!', body.message);
            } else {
              this.notify.error('Sorry, Something went wrong', error.statusText);
            }
          });
        }
      });
  }
  navigateToCreate() {
    if(this.isAdmin) {
      this.router.navigate(['/admin/dashboard/brand/create']);
    }
    if(this.isFrameCollector) {
      this.router.navigate(['/framecollector/dashboard/brand/create']);
    }
  }
  ngOnDestroy() {
    if (this.httpGetAllBrandsSubs) {
      this.httpGetAllBrandsSubs.unsubscribe();
    }
    if (this.httpDeleteSub) {
      this.httpDeleteSub.unsubscribe();
    }
  }

}
