import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandCreateUpdateComponent } from './brand-create-update.component';

describe('BrandCreateUpdateComponent', () => {
  let component: BrandCreateUpdateComponent;
  let fixture: ComponentFixture<BrandCreateUpdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrandCreateUpdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandCreateUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
