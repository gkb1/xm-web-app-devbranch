import { FormGroup, Validators, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { AdminProductService } from '../../products/product.service';
import { BrandService } from '../brand.service';
import { ErrorStateMatcher } from '@angular/material';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import * as _ from 'lodash';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { CommonService } from 'app/services/common.service';
import { AuthenticationService } from 'app/components/authentication/authentication.service';

@Component({
  selector: 'app-brand-create-update',
  templateUrl: './brand-create-update.component.html',
  styleUrls: ['./brand-create-update.component.scss']
})
export class BrandCreateUpdateComponent implements OnInit, OnDestroy {
  brandForm: FormGroup;
  selCategory: any;
  rootCategoryList = [];
  matcher: ErrorStateMatcher;
  httpGetAllCatSubs: Subscription;
  httpCreateBrandSubs: Subscription;
  isEditMode = false;
  brandId: string;
  name: string;
  existCategory = [];
  childCatList = [];
  categoryControl = new FormControl();
  isAdmin = this.authservices.isLoggedInUserAdmin();
  isFrameCollector = this.authservices.isLoggedInUserFrameCollector();
  backdata:any;

  constructor(private formBuilder: FormBuilder,
    private productService: AdminProductService,
    private brandService: BrandService,
    private notify: NotificationService,
    private loader: LoadingBarService,
    private route: ActivatedRoute,
    private router: Router,
    private commonService: CommonService,
    private authservices: AuthenticationService) { }

  ngOnInit() {
    this.backdata=history.state;
    this.initBrandForm();
    this.loadRootCatList();
    let genderlist = {};
    genderlist['value'] = ["WOMEN", "MEN", "KIDS"];
    this.loadAllCategories(genderlist);
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      this.brandId = paramMap.get('brandId');
      this.name = paramMap.get('brandName');
      if (paramMap.has('brandId')) {
        this.isEditMode = true;
        this.getSelBrandDetails();
      }
    })
  }

  getSelBrandDetails() {
    const payload = {
      brandId: this.brandId,
      brandName: this.name
    }
    this.brandService.getBrandByNameAndId(payload)
      .subscribe((res: any) => {
        this.brandForm.patchValue({
          name: res.name,
          description: res.description,
          media: res.media,
          gender: res.gender,
          code: res.code,
          // category: res.category
        });
        this.loadAllCategories({ gender: res.gender, category: res.category });
      });
  }

  setExistingCategory(category): FormArray {
    const formArray = new FormArray([]);
    category.forEach((cat) => {
      formArray.push(this.formBuilder.group({
        _id: cat._id
      }));
    });
    return formArray;
  }

  initBrandForm() {
    this.brandForm = this.formBuilder.group({
      name: ['', { validators: [Validators.required] }],
      description: [''],
      media: [''],
      gender: ['', { validators: [Validators.required] }],
      // code: ['', { validators: [Validators.required, Validators.maxLength(2)] }],
      category: this.formBuilder.array([])
    });
  }

  loadRootCatList() {
    const payload = {
      conditions: { isRoot: true },
      pagination: {}
    }
    this.httpGetAllCatSubs = this.productService.getAllCategoryList(payload).subscribe(res => {
      if (res && res.data.length > 0) {
        this.rootCategoryList = res.data;
      }
    });
  }

  loadAllCategories(selItem): any {
    this.childCatList = [];
    this.existCategory = [];
    const selectedCat = [];
    const payload = {
      conditions: selItem.value ? { catNameList: { $in: selItem.value } } : { catNameList: { $in: selItem.gender } },
      pagination: {}
    }
    this.httpGetAllCatSubs = this.productService.getAllCategoryList(payload).subscribe(res => {
      if (res && res.data.length > 0) {
        const control = <FormArray>this.brandForm.controls['category'];
        this.selCategory = res.data;
        _.forEach(res.data, data => {
          if (data.childCategories && data.childCategories.length > 0) {
            _.forEach(data.childCategories, childCat => {
              this.childCatList.push(childCat);
            })
          }
        })
        if (this.isEditMode) {
          for (const data of this.childCatList) {
            for (const exist of selItem.category) {
              if (exist._id === data._id._id) {
                this.existCategory.push(data._id);
                selectedCat.push(data._id._id);
                // data['isSelected'] = true;                
              }
            }
          }
          // this.childCatList = this.existCategory;
          this.brandForm.setControl('category', this.setExistingCategory(this.existCategory));          
        }
        // setting default selected
        this.categoryControl.setValue(selectedCat);
      }
    });
  }

  createCatForm(selCatId) {
    return this.formBuilder.group({
      _id: [selCatId]
    })
  }

  setChildCat(selItem) {
    if (selItem && (selItem.value ? (selItem.value.length > 0) : (selItem.length > 0))) {
      const control = <FormArray>this.brandForm.controls['category'];
      if (control) { // resetting controls
        control.controls = [];
      }
      _.forEach(selItem.value, selValue => {
        control.push(this.createCatForm(selValue));
      });
    }
  }

  setMedia(fileInput: any) {
    if (fileInput && fileInput.target.files[0]) {
      const control = <FormArray>this.brandForm.controls['media'];
      if (control) {
        this.loader.show();
        const formData = new FormData();
        formData.append('content', fileInput.target.files[0]);
        this.httpCreateBrandSubs = this.productService.productImageUpload(formData).subscribe(res => {
          if (res) {
            control.setValue(res);
            this.notify.success('Success', 'Image uploaded successfully !!');
            this.loader.hide();
          } else {
            this.loader.hide();
          }
        }, (error) => {
          this.loader.hide();
          let body;
          if (error._body) {
            body = JSON.parse(error._body);
          }
          if (body.status === 406) {
            this.notify.error('Unable to Upload Image!!', body.message);
          } else {
            this.notify.error('Something went wrong', error.statusText);
          }
        })
      }
    }
  }

  createBrand() {
    if (!this.brandForm.valid) {
      const target = document.querySelector('.ng-invalid');
      if (target) {
        target.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
      }
      this.commonService.validateAllFormFields(this.brandForm);
      return false;
    }
    if (!this.isEditMode) {
      this.loader.show();
      this.httpCreateBrandSubs = this.brandService.createBrand(this.brandForm.value).subscribe(res => {
        this.loader.hide();
        if (res) {
          this.brandForm.reset();
          console.log(this.backdata)
          if(this.backdata.productId){
            this.router.navigate(['/framecollector/dashboard/product/'+this.backdata.productId+'/'+this.backdata.productName]);
          }else
          if (this.isFrameCollector) {
            this.router.navigate(['/framecollector/dashboard/brand']);
          }
          if (this.isAdmin) {
            this.router.navigate(['/admin/dashboard/brand']);
          }
          this.notify.success('Success', 'Brand created successfully!!');
        }
      }, (error) => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 406 || body.status === 409) {
          this.notify.error('Unable to create product!!', body.message);
        } else {
          this.notify.error('Something went wrong', body.message);
        }
      })
    }

  }
  updateBrand() {
    if (!this.brandForm.valid) {
      const target = document.querySelector('.ng-invalid');
      if (target) {
        target.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
      }
      this.commonService.validateAllFormFields(this.brandForm);
      return false;
    }
    if (this.isEditMode) {
      const payload = {
        brandId: this.brandId,
        brandName: this.name
      }
      this.loader.show();
      this.httpCreateBrandSubs = this.brandService.updateBrandByNameAndId(payload, this.brandForm.value).subscribe(res => {
        this.loader.hide();
        if (res) {
          this.brandForm.reset();
          this.notify.success('Success', 'Brand updated successfully!!');
          if (this.isFrameCollector) {
            this.router.navigate(['/framecollector/dashboard/brand']);
          }
          if (this.isAdmin) {
            this.router.navigate(['/admin/dashboard/brand']);
          }
        }
      }, (error) => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 406) {
          this.notify.error('Unable to update Product!!', body.message);
        } else {
          this.notify.error('Something went wrong', error.statusText);
        }
      })
    }
  }

  ngOnDestroy() {
    if (this.httpGetAllCatSubs) {
      this.httpGetAllCatSubs.unsubscribe()
    }
    if (this.httpCreateBrandSubs) {
      this.httpCreateBrandSubs.unsubscribe()
    }
  }

}
