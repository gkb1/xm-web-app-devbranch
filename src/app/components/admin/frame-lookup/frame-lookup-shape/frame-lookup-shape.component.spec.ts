import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameLookupShapeComponent } from './frame-lookup-shape.component';

describe('FrameLookupShapeComponent', () => {
  let component: FrameLookupShapeComponent;
  let fixture: ComponentFixture<FrameLookupShapeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameLookupShapeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameLookupShapeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
