import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameLookupMaterialComponent } from './frame-lookup-material.component';

describe('FrameLookupMaterialComponent', () => {
  let component: FrameLookupMaterialComponent;
  let fixture: ComponentFixture<FrameLookupMaterialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameLookupMaterialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameLookupMaterialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
