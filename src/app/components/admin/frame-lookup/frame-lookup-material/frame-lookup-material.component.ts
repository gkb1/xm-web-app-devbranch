import { Component, OnInit, OnDestroy } from '@angular/core';
import { FrameLookupService } from '../frame-lookup.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MyErrorStateMatcher } from 'app/constants/errorStateMatcher';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { Subscription } from 'rxjs/Subscription';
import { from } from 'rxjs';

@Component({
  selector: 'app-frame-lookup-material',
  templateUrl: './frame-lookup-material.component.html',
  styleUrls: ['./frame-lookup-material.component.scss']
})
export class FrameLookupMaterialComponent implements OnInit, OnDestroy {
  frameMaterialList = [];
  payload = { conditions: {}, pagination: {} };
  modalRef: BsModalRef;
  frameMaterialForm: FormGroup;
  matcher: MyErrorStateMatcher;
  uploadedFile;
  imagePreview;
  codeExist = false;
  frameServiceSubs: Subscription;
  frameCreateSubs: Subscription;
  textOnlyRegexPattern = new RegExp(/^[a-zA-Z ]*$/);

  constructor(private frameLookUpServices: FrameLookupService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private loader: LoadingBarService,
    private notify: NotificationService) { }

  ngOnInit() {
    this.getMaterailList(this.payload);
    this.initFrameMaterialForm();
  }
  initFrameMaterialForm() {
    this.frameMaterialForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern(this.textOnlyRegexPattern)]],
      media: ['', { Validators: [Validators.required] }]
    });
  }

  addframeMaterialModal(template) {
    this.modalRef = this.modalService.show(template);
  }

  setMedia(event) {
    const file = (event.target as HTMLInputElement).files[0];
    if (file.type.indexOf('image/') > -1) {
      this.uploadedFile = file;
      const reader = new FileReader();
      this.frameMaterialForm.get('media').setValue(this.uploadedFile);
      reader.onload = () => {
        this.imagePreview = reader.result;
      };
      reader.readAsDataURL(file);
    } else {
      this.notify.error('Unable to Upload', 'Upload only image file');
    }
  }

  createMaterial() {
    const formValue = this.frameMaterialForm.getRawValue();
    const formData = new FormData();
    formData.append('name', this.frameMaterialForm.value.name);
    //formData.append('media', this.frameMaterialForm.get('media').value);
    // formData.append('media', this.uploadedFile);
      console.log(this.frameMaterialForm.value)
    console.log(formData)
    this.loader.show();
    this.frameCreateSubs = this.frameLookUpServices.createFrameMaterial(formData).subscribe((res: any) => {
      if (res) {
        this.loader.hide();
        this.modalRef.hide();
        this.frameMaterialForm.reset();
        this.imagePreview = '';
        this.getMaterailList(this.payload);
        this.notify.success('Success', 'Created Successfully !!');
      }
    },
      error => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 409) {
          this.notify.error('Unable to Create!!', body.message);
        } else {
          this.notify.error('Something went wrong', body.message);
        }
      });
  }

  getMaterailList(payload) {
    this.frameServiceSubs = this.frameLookUpServices.getFrameMaterialList(payload).subscribe((res: any) => {
      this.frameMaterialList = res;
    })
  }

  ngOnDestroy() {
    if (this.frameServiceSubs) {
      this.frameServiceSubs.unsubscribe();
    }
    if (this.frameCreateSubs) {
      this.frameCreateSubs.unsubscribe();
    }
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
}
