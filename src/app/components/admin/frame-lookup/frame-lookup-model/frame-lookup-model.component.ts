import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from 'app/constants/errorStateMatcher';
import { FrameLookupService } from '../frame-lookup.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';

@Component({
  selector: 'app-frame-lookup-model',
  templateUrl: './frame-lookup-model.component.html',
  styleUrls: ['./frame-lookup-model.component.scss']
})
export class FrameLookupModelComponent implements OnInit, OnDestroy {

  frameModelList = [];
  payload = { conditions: {}, pagination: {} };
  modalRef: BsModalRef;
  frameModelForm : FormGroup;
  matcher: MyErrorStateMatcher;
  uploadedFile;
  imagePreview;
  codeExist = false;
  frameServiceSubs: Subscription;
  frameCreateSubs: Subscription;
  textOnlyRegexPattern = new RegExp(/^[a-zA-Z ]*$/);
  backdata:any;
  constructor(private frameLookUpServices : FrameLookupService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private loader: LoadingBarService,
    private notify: NotificationService,
    private router:Router) { }

  ngOnInit() {
    this.backdata=history.state;
    this.getModelList(this.payload);
    this.initFrameModelForm();
  }

  initFrameModelForm() {
    this.frameModelForm = this.formBuilder.group({
      name: ['', {Validators: [Validators.required]}],
      modelCode: ['', {Validators: [Validators.required]}]
      // code: ['',{Validators:[Validators.required]}],
    });
  }

  addframeMaterialModal(template) {
    this.modalRef = this.modalService.show(template);
  }
  // checkCodeAvailability(event){
  //   this.codeExist = false;
  //   this.frameModelList.forEach(element => {
  //    if(element.code == event.key && !this.frameModelForm.controls['code'].hasError('required')) {
  //      this.codeExist = true;
  //    } 
  //   });
  // }
  createMaterial() {
    this.loader.show();
    this.frameCreateSubs = this.frameLookUpServices.createFrameModel(this.frameModelForm.value).subscribe((res:any)=>{
      if (res) {
        this.loader.hide();
        this.frameModelForm.reset();
        this.modalRef.hide();
        this.getModelList(this.payload);
        if(this.backdata.productId){
          this.router.navigate(['/framecollector/dashboard/product/'+this.backdata.productId+'/'+this.backdata.productName]);
        };
        this.notify.success('Success', 'Created Successfully !!');
      }
    },
    error => {
      this.loader.hide();
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 409) {
        this.notify.error('Unable to Create!!', body.message);
      } else {
        this.notify.error('Something went wrong', body.message);
      }
    });
  }

  getModelList(payload) {
    this.frameServiceSubs = this.frameLookUpServices.getFrameModelList(payload).subscribe((res:any)=>{
      this.frameModelList = res;
    })
  }

  ngOnDestroy() {
    if (this.frameServiceSubs) {
      this.frameServiceSubs.unsubscribe();
    }
    if (this.frameCreateSubs) {
      this.frameCreateSubs.unsubscribe();
    }
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
}
