import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameLookupModelComponent } from './frame-lookup-model.component';

describe('FrameLookupModelComponent', () => {
  let component: FrameLookupModelComponent;
  let fixture: ComponentFixture<FrameLookupModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameLookupModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameLookupModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
