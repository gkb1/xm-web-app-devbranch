import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { RoleGuardService } from 'app/security/role-guard.service';
import * as Constant from '../../../constants/app.constants';
import { FrameLookupMainComponent } from 'app/components/admin/frame-lookup/frame-lookup-main/frame-lookup-main.component';
import { FrameLookupHomeComponent } from 'app/components/admin/frame-lookup/frame-lookup-home/frame-lookup-home.component';

const frameRoutes: Routes = [
    {
        path: '',  canActivate: [AuthGuard], component: FrameLookupMainComponent,  children: [
            { path : '', canActivate: [RoleGuardService],
                data: {
                    expectedRole: [Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN, Constant.ROLE_FRAME_COLLECTOR]
                },
                component: FrameLookupHomeComponent 
            }
        ]
    },
   { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const FrameLookupRoutingModule = RouterModule.forChild(frameRoutes);
