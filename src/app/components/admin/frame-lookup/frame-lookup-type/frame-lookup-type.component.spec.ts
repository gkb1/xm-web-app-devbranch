import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameLookupTypeComponent } from './frame-lookup-type.component';

describe('FrameLookupTypeComponent', () => {
  let component: FrameLookupTypeComponent;
  let fixture: ComponentFixture<FrameLookupTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameLookupTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameLookupTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
