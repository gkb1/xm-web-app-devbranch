import { Component, OnInit, OnDestroy } from '@angular/core';
import { FrameLookupService } from '../frame-lookup.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from 'app/constants/errorStateMatcher';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-frame-lookup-type',
  templateUrl: './frame-lookup-type.component.html',
  styleUrls: ['./frame-lookup-type.component.scss']
})
export class FrameLookupTypeComponent implements OnInit, OnDestroy {

  
  frameTypeList = [];
  payload = { conditions: {}, pagination: {} };
  modalRef: BsModalRef;
  frameTypeForm : FormGroup;
  matcher: MyErrorStateMatcher;
  uploadedFile;
  imagePreview;
  codeExist = false;
  frameServiceSubs: Subscription;
  frameCreateSubs: Subscription;
  textOnlyRegexPattern = new RegExp(/^[a-zA-Z ]*$/);

  constructor(private frameLookUpServices : FrameLookupService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private loader: LoadingBarService,
    private notify: NotificationService) { }

  ngOnInit() {
    this.getTypeList(this.payload);
    this.initFrameTypeForm();
  }

  initFrameTypeForm() {
    this.frameTypeForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern(this.textOnlyRegexPattern)]],
      // code: ['',{Validators:[Validators.required]}],
      media: ['',{Validators:[Validators.required]}]
    });
  }

  addframeTypeModal(template) {
    this.modalRef = this.modalService.show(template);
  }

  setMedia(event) {
    const file = (event.target as HTMLInputElement).files[0];
    if(file.type.indexOf('image/')>-1) {
      this.uploadedFile = file;
      const reader = new FileReader();
      this.frameTypeForm.get('media').setValue(this.uploadedFile);
      reader.onload = () => {
        this.imagePreview = reader.result;
      };
      reader.readAsDataURL(file);
    } else {
      this.notify.error('Unable to Upload', 'Upload only image file');
    }
    
  }
  // checkCodeAvailability(event){
  //   this.codeExist = false;
  //   this.frameTypeList.forEach(element => {
  //    if(element.code == event.key && !this.frameTypeForm.controls['code'].hasError('required')) {
  //      this.codeExist = true;
  //    } 
  //   });
  // }
  createType() {
    const formData = new FormData();
    formData.append('name', this.frameTypeForm.value.name);
    // formData.append('code', this.frameTypeForm.value.code);
    //formData.append('media', this.uploadedFile);
    this.loader.show();
    this.frameCreateSubs = this.frameLookUpServices.createFrameType(formData).subscribe((res:any)=>{
      if(res) {
        this.loader.hide();
        this.modalRef.hide();
        this.frameTypeForm.reset();
        this.imagePreview = '';
        this.getTypeList(this.payload);
        this.notify.success('Success','Created Successfully !!');
      }
    },
    error => {
      this.loader.hide();
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 409) {
        this.notify.error('Unable to Create!!', body.message);
      } else {
        this.notify.error('Something went wrong', body.message);
      }
    });
  }

  getTypeList(payload) {
    this.frameServiceSubs = this.frameLookUpServices.getFrameTypeList(payload).subscribe((res:any)=>{
      this.frameTypeList = res;
    })
  }
  
  ngOnDestroy() {
    if (this.frameServiceSubs) {
      this.frameServiceSubs.unsubscribe();
    }
    if (this.frameCreateSubs) {
      this.frameCreateSubs.unsubscribe();
    }
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
}
