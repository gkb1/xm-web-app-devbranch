import { Component, OnInit, OnDestroy } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MyErrorStateMatcher } from 'app/constants/errorStateMatcher';
import { FrameLookupService } from '../frame-lookup.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { Subscription } from 'rxjs/Subscription';
import { Router } from '@angular/router';

@Component({
  selector: 'app-frame-lookup-colour',
  templateUrl: './frame-lookup-colour.component.html',
  styleUrls: ['./frame-lookup-colour.component.scss']
})
export class FrameLookupColourComponent implements OnInit, OnDestroy {

  frameColorList = [];
  payload = { conditions: {}, pagination: {} };
  modalRef: BsModalRef;
  frameColorForm: FormGroup;
  matcher: MyErrorStateMatcher;
  uploadedFile;
  imagePreview;
  frameServiceSubs: Subscription;
  frameCreateSubs: Subscription;
  parentFrameColourLookupList = [];
  httpParentFrameColourSubs: Subscription;
  textOnlyRegexPattern = new RegExp(/^[a-zA-Z ]*$/);
  backdata:any;
  constructor(private frameLookUpServices: FrameLookupService,
    private modalService: BsModalService,
    private formBuilder: FormBuilder,
    private loader: LoadingBarService,
    private notify: NotificationService,
    private router:Router) { }

  ngOnInit() {
    this.backdata=history.state;
    this.getColorList(this.payload);
    this.loadAllParentColourLookup();
  }

  initFrameColorForm() {
    this.frameColorForm = this.formBuilder.group({
      actualColour: ['', [Validators.required, Validators.pattern(this.textOnlyRegexPattern)]],
      parentColour: ['', {Validators: [Validators.required]}],
      hexCode: ['', {Validators: [Validators.required]}],
      // XMCode: ['', {Validators: [Validators.required]}]
    });
  }

  addframeMaterialModal(template) {
    this.modalRef = this.modalService.show(template);
  }

  loadAllParentColourLookup() {
    this.httpParentFrameColourSubs = this.frameLookUpServices.getAllFrameParentColorList().subscribe(res => {
      if (res) {
        this.parentFrameColourLookupList = res;
      }
    });
    this.initFrameColorForm();
  }

  createMaterial() {
    this.loader.show();
    // let str = this.frameColorForm.value.parentColour;
    // str = str ? str.charAt(0).toUpperCase() + str.substr(1).toLowerCase() : '';
    // this.frameColorForm.controls.parentColour.setValue(str);
    this.frameCreateSubs = this.frameLookUpServices.createFrameColor(this.frameColorForm.value).subscribe((res:any)=>{
      if (res) {
        this.loader.hide();
        this.modalRef.hide();
        this.frameColorForm.reset();
        this.getColorList(this.payload);
        if(this.backdata.productId){
          this.router.navigate(['/framecollector/dashboard/product/'+this.backdata.productId+'/'+this.backdata.productName]);
        };
        this.notify.success('Success', 'Created Successfully !!');
      }
    },
    error => {
      this.loader.hide();
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 409) {
        this.notify.error('Unable to Create!!', body.message);
      } else {
        this.notify.error('Something went wrong !!', body.message);
      }
    });
  }

  getColorList(payload) {
    this.frameServiceSubs = this.frameLookUpServices.getFrameColorList(payload).subscribe((res: any) => {
      this.frameColorList = res;
    })
  }

  ngOnDestroy() {
    if (this.frameServiceSubs) {
      this.frameServiceSubs.unsubscribe();
    }
    if (this.frameCreateSubs) {
      this.frameCreateSubs.unsubscribe();
    }
    if (this.httpParentFrameColourSubs) {
      this.httpParentFrameColourSubs.unsubscribe();
    }
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
}
