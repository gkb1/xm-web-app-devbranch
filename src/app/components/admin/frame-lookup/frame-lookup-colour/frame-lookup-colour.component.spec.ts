import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameLookupColourComponent } from './frame-lookup-colour.component';

describe('FrameLookupColourComponent', () => {
  let component: FrameLookupColourComponent;
  let fixture: ComponentFixture<FrameLookupColourComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameLookupColourComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameLookupColourComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
