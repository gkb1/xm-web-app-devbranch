import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameLookupHomeComponent } from './frame-lookup-home.component';

describe('FrameLookupHomeComponent', () => {
  let component: FrameLookupHomeComponent;
  let fixture: ComponentFixture<FrameLookupHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameLookupHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameLookupHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
