import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameLookupMainComponent } from './frame-lookup-main.component';

describe('FrameLookupMainComponent', () => {
  let component: FrameLookupMainComponent;
  let fixture: ComponentFixture<FrameLookupMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameLookupMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameLookupMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
