import { Injectable } from '@angular/core';
import * as AppUtils from '../../../utils/app.utils';
import { Http } from '@angular/http';

@Injectable()
export class FrameLookupService {
  constructor(private http: Http) { }
  getFrameMaterialList(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_FRAME_MATERIAL_LIST, payload)
      .map(res => res.json());
  }

  getFrameShapeList(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_FRAME_SHAPE_LIST, payload)
      .map(res => res.json());
  }

  getFrameTypeList(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_FRAME_TYPE_LIST, payload)
      .map(res => res.json());
  }

  getFrameColorList(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_FRAME_COLOR_LIST, payload)
      .map(res => res.json());
  }

  getFrameModelList(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_FRAME_MODEL_LIST, payload)
      .map(res => res.json());
  }

  createFrameMaterial(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.CREATE_FRAME_MATERIAL, payload)
      .map(res => res.json());
  }

  createFrameShape(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.CREATE_FRAME_SHAPE, payload)
      .map(res => res.json());
  }

  createFrameType(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.CREATE_FRAME_TYPE, payload)
      .map(res => res.json());
  }

  createFrameColor(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.CREATE_FRAME_COLOR, payload)
      .map(res => res.json());
  }

  createFrameModel(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.CREATE_FRAME_MODEL, payload)
      .map(res => res.json());
  }

  getAllFrameParentColorList() {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_FRAME_PARENT_COLOUR_LIST)
      .map(res => res.json());
  }
}
