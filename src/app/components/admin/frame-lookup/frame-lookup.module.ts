import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrameLookupMainComponent } from './frame-lookup-main/frame-lookup-main.component';
import { FrameLookupTypeComponent } from './frame-lookup-type/frame-lookup-type.component';
import { FrameLookupMaterialComponent } from './frame-lookup-material/frame-lookup-material.component';
import { FrameLookupShapeComponent } from './frame-lookup-shape/frame-lookup-shape.component';
import { FrameLookupHomeComponent } from './frame-lookup-home/frame-lookup-home.component';
import { FrameLookupRoutingModule } from 'app/components/admin/frame-lookup/frame-lookup-routes';
import { MatTabsModule } from '@angular/material/tabs';
import { FrameLookupService } from 'app/components/admin/frame-lookup/frame-lookup.service';
import { CustomMaterailModule } from 'app/custom-materail/custom-materail.module';
import { FrameLookupColourComponent } from './frame-lookup-colour/frame-lookup-colour.component';
import { FrameLookupModelComponent } from './frame-lookup-model/frame-lookup-model.component';



@NgModule({
  declarations: [
    FrameLookupMainComponent,
    FrameLookupHomeComponent,
    FrameLookupTypeComponent,
    FrameLookupMaterialComponent,
    FrameLookupShapeComponent,
    FrameLookupColourComponent,
    FrameLookupModelComponent,
    ],
  imports: [
    CommonModule,
    FrameLookupRoutingModule,
    MatTabsModule,
    CustomMaterailModule
  ],
  providers: [
    FrameLookupService
  ]
})
export class FrameLookupModule { }
