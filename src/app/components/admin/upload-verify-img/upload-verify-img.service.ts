import { Injectable } from '@angular/core';
import * as Constant from '../../../constants/app.constants';
import { Http } from '@angular/http';
import * as AppUtils from '../../../utils/app.utils';
import { Subject } from 'rxjs';

@Injectable()
export class VerifyImgService {
  
  private instantUpdate = new Subject<void>();
  constructor(private http: Http) { }

  get instantUpdates$() {
    return this.instantUpdate;
  }

  getSelPrdtDetailsByTagNoSearch(tagNo) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_SEL_PRDT_BY_TAGNO_SEARCH_URL + tagNo)
      .map(res => res.json());
  }

  triggerS3Upload() {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.TRIGGER_S3_FOLDER_UPLOAD_URL, {})
    .map(res => res.json());
  }

  verifyAndGenerateSKU(productId, prdtAttrId, storeCode) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.VERIFY_GENERATE_SKU_URL + productId + '/' + prdtAttrId + '/' + storeCode)
    .map(res => res.json());
  }
}
