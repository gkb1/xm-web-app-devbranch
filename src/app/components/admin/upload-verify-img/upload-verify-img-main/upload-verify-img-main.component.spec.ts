import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadVerifyImgMainComponent } from './upload-verify-img-main.component';

describe('UploadVerifyImgMainComponent', () => {
  let component: UploadVerifyImgMainComponent;
  let fixture: ComponentFixture<UploadVerifyImgMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadVerifyImgMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadVerifyImgMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
