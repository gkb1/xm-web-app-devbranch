import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploadVerifyImgMainComponent } from './upload-verify-img-main/upload-verify-img-main.component';
import { UploadVerifyImgListComponent } from './upload-verify-img-list/upload-verify-img-list.component';
import { VerifyImgService } from 'app/components/admin/upload-verify-img/upload-verify-img.service';
import { VerifyImgRoutingModule } from 'app/components/admin/upload-verify-img/upload-verify-img.routes';
import { CustomMaterailModule } from 'app/custom-materail/custom-materail.module';
import { ModalModule } from 'ngx-bootstrap';



@NgModule({
  declarations: [
    UploadVerifyImgMainComponent, 
    UploadVerifyImgListComponent],
  imports: [
    CommonModule,
    VerifyImgRoutingModule,
    CustomMaterailModule,
    ModalModule.forRoot()
  ],
  providers: [
    VerifyImgService
  ]
})
export class UploadVerifyImgModule { }
