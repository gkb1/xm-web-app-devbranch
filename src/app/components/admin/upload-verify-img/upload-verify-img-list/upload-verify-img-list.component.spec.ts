import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadVerifyImgListComponent } from './upload-verify-img-list.component';

describe('UploadVerifyImgListComponent', () => {
  let component: UploadVerifyImgListComponent;
  let fixture: ComponentFixture<UploadVerifyImgListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadVerifyImgListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadVerifyImgListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
