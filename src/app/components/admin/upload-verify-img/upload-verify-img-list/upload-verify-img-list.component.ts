import { Component, OnInit, OnDestroy } from '@angular/core';
import { MyErrorStateMatcher } from 'app/constants/errorStateMatcher';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { CommonService } from 'app/services/common.service';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { VerifyImgService } from 'app/components/admin/upload-verify-img/upload-verify-img.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { Subscription } from 'rxjs';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-upload-verify-img-list',
  templateUrl: './upload-verify-img-list.component.html',
  styleUrls: ['./upload-verify-img-list.component.scss']
})
export class UploadVerifyImgListComponent implements OnInit, OnDestroy {
  verifyForm: FormGroup;
  matcher: MyErrorStateMatcher;
  selPrdtDetails;
  httpGetSelPrdtDetailsSubs: Subscription;
  httpTriggerS3FolderUploadSubs: Subscription;
  productsList = [];
  selImageViewUrl;
  modalRef: BsModalRef;
  httpVerifyGenSKUSubs: Subscription;

  constructor(private fb: FormBuilder,
  private commonService: CommonService,
  private notify: NotificationService,
  private loader: LoadingBarService,
  private verifyImgService: VerifyImgService,
  private modalService: BsModalService) { }

  ngOnInit() {
    this.initVerifyForm();
    this.verifyImgService.instantUpdates$.subscribe(() => {
      this.loadSelProductDetailsByTagNo(this.verifyForm.value.tagNo);
    })
  }

  initVerifyForm() {
    this.verifyForm = this.fb.group({
      tagNo: ['', [Validators.required]],
    });
  }

  triggerS3Upload() {
    this.loader.show();
    this.httpTriggerS3FolderUploadSubs = this.verifyImgService.triggerS3Upload().subscribe(res => {
      this.loader.hide();
      if (res) {
        this.notify.success('Uploaded !!', res.message);
      }
    }, (error) => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 406) {
          this.notify.error('Unable to Upload !!', body.message);
        } else {
          this.notify.error('Something went wrong', error.statusText);
        }
    });
  }

  searchProductByTagNo() {
    this.selPrdtDetails = null;
    if (!this.verifyForm.valid) {
      const target = document.querySelector('.ng-invalid');
      if (target) {
        target.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
      }
      this.commonService.validateAllFormFields(this.verifyForm);
      return false;
    }
    this.loadSelProductDetailsByTagNo(this.verifyForm.value.tagNo);
  }

  loadSelProductDetailsByTagNo(tagNo) {
    this.productsList = [];
    this.selPrdtDetails = null;
    this.httpGetSelPrdtDetailsSubs = this.verifyImgService.getSelPrdtDetailsByTagNoSearch(tagNo).subscribe(res => {
      if (res && res.data) {
          if (res.data.product) {
          this.selPrdtDetails = res.data.product;
          this.productsList.push(res.data.product);
        } else {
          this.notify.error('No product found with Tag No - ' + tagNo + '!!', '');
          // setTimeout(() => {
          //   if (this.isLoggedInUserAdmin) {
          //     this.router.navigate(['/admin/dashboard/product/create']);
          //   }
          //   if (this.isLoggedInUserECP) {
          //     this.router.navigate(['/ecpcust/dashboard/product/create']);
          //   }
          // }, 1000);
        }
      }
    }, (error) => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 406) {
          this.notify.error('Unable to load details !!', body.message);
        } else {
          this.notify.error('Something went wrong', error.statusText);
        }
      })
  }

  showImageViewPopUp(template, element) {
    this.selImageViewUrl = element;
    this.modalRef = this.modalService.show(template);
  }

  verifyAndGenerateSKU(product) {
  // tslint:disable-next-line:max-line-length
  if (!product || product.prdt_attributes.length <= 0 || !product.prdt_attributes[0].colour || 
    !product.prdt_attributes[0].colour.store.length) {
      return this.notify.error('Tag ' + this.verifyForm.value.tagNo + ' is not yet cloned to store !!', '');
  }
    this.httpVerifyGenSKUSubs =
    this.verifyImgService.verifyAndGenerateSKU(product.productId, 
      product.prdt_attributes[0].prdAttributeId, product.prdt_attributes[0].colour.store[0].store_code).subscribe(res => {
      this.loader.hide();
        if (res) {
          // this.verifyForm.reset();
          this.notify.success('Product Verfied & SKU generated For ' + this.verifyForm.value.tagNo +' SuccessFully !!', '');
          this.loadSelProductDetailsByTagNo(this.verifyForm.value.tagNo);
          // this.selPrdtDetails = null;
        }
      }, (error) => {
          this.loader.hide();
          let body;
          if (error._body) {
            body = JSON.parse(error._body);
          }
          if (body.status === 406) {
            this.notify.error('Unable to Verify!!', body.message);
          } else {
            this.notify.error('Something went wrong', body.message);
          }
      });
}

  ngOnDestroy() {
    if (this.httpGetSelPrdtDetailsSubs) {
      this.httpGetSelPrdtDetailsSubs.unsubscribe();
    }
    if (this.httpTriggerS3FolderUploadSubs) {
      this.httpTriggerS3FolderUploadSubs.unsubscribe();
    }
    if (this.httpVerifyGenSKUSubs) {
      this.httpVerifyGenSKUSubs.unsubscribe();
    }
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
}
