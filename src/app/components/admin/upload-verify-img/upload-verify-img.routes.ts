import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { UploadVerifyImgMainComponent } from 'app/components/admin/upload-verify-img/upload-verify-img-main/upload-verify-img-main.component';
import { UploadVerifyImgListComponent } from 'app/components/admin/upload-verify-img/upload-verify-img-list/upload-verify-img-list.component';

const verifyRoutes: Routes = [
    // canActivate: [AuthGuard]
    {
        path: '', component: UploadVerifyImgMainComponent,  children: [
            { path : '', component: UploadVerifyImgListComponent },
        ]
    },
   { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const VerifyImgRoutingModule = RouterModule.forChild(verifyRoutes);
