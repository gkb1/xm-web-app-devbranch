import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import * as AppUtils from '../../../utils/app.utils';

@Injectable()
export class UserService {
  
  constructor(private http: Http) { }
  getAllUserListByCond(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_USERS_LIST_BY_CONDN, payload)
      .map(res => res.json());
  }
  updateUserStatus(id,payload) {
    return this.http.put(AppUtils.BACKEND_API_ROOT_URL + AppUtils.UPDATE_USER_STATUS + id, payload)
    .map(res => res.json());
  }
}
