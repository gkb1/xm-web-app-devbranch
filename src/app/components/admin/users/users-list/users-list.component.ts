import { Component, OnInit, OnDestroy } from '@angular/core';
import * as Constant from '../../../../constants/app.constants';
import { UserService } from '../users.service';
import { Subscription } from 'rxjs/Subscription';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { ConfirmationService } from 'app/components/shared/confirmation/confirmation.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { NotificationService } from 'app/components/shared/notification/notification.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit, OnDestroy {
  userDetailList = [];
  payload = { conditions: {}, pagination: {} };
  httpUserListSubs: Subscription;
  httpUserStatus: Subscription;
  selected = 'Ecp';
  user = ['Ecp','Moderators','collectors'];
  showStoreCode = true;
  collapsibleView;
  constructor(private userServices : UserService,private confirmation: ConfirmationService,
    private loader: LoadingBarService,
    private notify: NotificationService) { }

  ngOnInit() {
    this.showStoreCode = true;
    this.payload.conditions['role'] = Constant.ROLE_ECP_CUSTOMER;
    this.getUserList(this.payload);
  }

  userSelected(event) {
    if (event === 'Moderators') {
      this.userDetailList = [];
      this.payload.conditions['role'] = Constant.ROLE_FRAME_MODERATOR;
      this.getUserList(this.payload);
    }
    if (event === 'collectors') {
      this.userDetailList = [];
      this.showStoreCode = false;
      this.payload.conditions['role'] = Constant.ROLE_FRAME_COLLECTOR;
      this.getUserList(this.payload);
    }
    if (event === 'Ecp') {
      this.userDetailList = [];
      this.payload.conditions['role'] = Constant.ROLE_ECP_CUSTOMER;
      this.getUserList(this.payload);
    }
  }

  updateAdminVerifiedStatus(user) {
    this.confirmation.showDialog('',
    status ? 'Are you sure ?' : 'Are you sure you want to ' + (user.auditFields. isActive ? 'DEACTIVATE' : 'ACTIVATE') + ' ' + user.name + '?')
    .subscribe(obj => {
      if (obj) {
        const payload = {
          auditFields: {
              isActive: user.auditFields. isActive ? false : true
          }
      }
        this.httpUserStatus = this.userServices.updateUserStatus(user.userId, payload).subscribe((res: any) => {
          if (res) {
            this.getUserList(this.payload);
            this.notify.success('Success!!', res.message);
          } else {
            this.notify.error('Unable to Update Product status, Please try again later!!', '');
          }
        }, (error) => {
          this.loader.hide();
          let body;
          if (error._body) {
            body = JSON.parse(error._body);
          }
          if (body.status === 406 || body.status === 409) {
            this.notify.error('Unable to Update product status, Please try again later!!', body.message);
          } else {
            this.notify.error('Sorry, Something went wrong', error.statusText);
          }
        });
      }
    });
  }

  getUserList(payload) {
    this.httpUserListSubs = this.userServices.getAllUserListByCond(payload).subscribe((res:any)=>{
      this.userDetailList = res.data;
    })
  }

  ngOnDestroy() {
    if (this.httpUserStatus) {
      this.httpUserStatus.unsubscribe();
    }
    if (this.httpUserListSubs) {
      this.httpUserListSubs.unsubscribe();
    }
  }
}
