import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { RoleGuardService } from 'app/security/role-guard.service';
import { UsersMainComponent } from 'app/components/admin/users/users-main/users-main.component';
import { UsersListComponent } from 'app/components/admin/users/users-list/users-list.component';

const userRoutes: Routes = [
    {
        path: '',  canActivate: [AuthGuard], component: UsersMainComponent,  children: [
            { path : '', component: UsersListComponent }
        ]
    },
   { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const UserRoutingModule = RouterModule.forChild(userRoutes);
