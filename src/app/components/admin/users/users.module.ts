import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersMainComponent } from './users-main/users-main.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UserRoutingModule } from 'app/components/admin/users/users.routes';
import { UserService } from 'app/components/admin/users/users.service';
import { CustomMaterailModule } from 'app/custom-materail/custom-materail.module';



@NgModule({
  declarations: [
    UsersMainComponent, 
    UsersListComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    CustomMaterailModule
  ],
  providers: [
    UserService
  ]
})
export class UsersModule { }
