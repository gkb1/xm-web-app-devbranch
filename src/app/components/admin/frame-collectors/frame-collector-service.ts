import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http } from '@angular/http';
import * as AppUtils from '../../../utils/app.utils';

@Injectable()
export class FrameCollectorService {

  constructor( private http: Http ) { }

  getAllUserListByCond(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_USERS_LIST_BY_CONDN, payload)
      .map(res => res.json());
  }

  addStoreToCollector(payload) {
    return this.http.put(AppUtils.BACKEND_API_ROOT_URL + AppUtils.ADD_STORE_TO_FRAME_COLLECTOR, payload)
    .map(res => res.json());
  }

  deleteStoreToCollector(cust_code, userID) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.DELETE_STORE_FROM_FRAME_COLLECTOR + cust_code +'/'+ userID)
      .map(res => res.json());
  }
  
  getFrameCollectorByStore(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_FRAME_COLLECTORS_BY_STORE, payload)
    .map(res => res.json());
  }
  updateFrameCollectorStatus(payload) {
    return this.http.put(AppUtils.BACKEND_API_ROOT_URL + AppUtils.UPDATE_FRAME_COLLECTOR_STATUS + payload.ecpUserId + '/' + payload.frameId, 
    payload.status)
    .map(res => res.json());
  }

  updateAllFrameStatusOfSelECP(payload) {
    return this.http.put(AppUtils.BACKEND_API_ROOT_URL + AppUtils.UPDATE_SEL_ECP_ALLFRAME_STATUS + payload.ecpUserId ,  payload.status)
    .map(res => res.json());
  }
}
