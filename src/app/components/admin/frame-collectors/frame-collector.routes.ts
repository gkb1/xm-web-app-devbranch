import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { FrameCollectorMainComponent } from 'app/components/admin/frame-collectors/frame-collector-main/frame-collector-main.component';
import { FrameCollectorListComponent } from 'app/components/admin/frame-collectors/frame-collector-list/frame-collector-list.component';
import { AddStoreComponent } from './add-store/add-store.component';

const frameCollectorRoutes: Routes = [
    {
        path: '', canActivate: [AuthGuard], component: FrameCollectorMainComponent,  children: [
            { path : '', component: FrameCollectorListComponent },
            { path : ':collectorId/:collectorName/:status', component: AddStoreComponent }
        ]
    },
   { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const FrameCollectorRoutingModule = RouterModule.forChild(frameCollectorRoutes);
