import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrameCollectorMainComponent } from './frame-collector-main/frame-collector-main.component';
import { FrameCollectorListComponent } from './frame-collector-list/frame-collector-list.component';
import { FrameCollectorRoutingModule } from 'app/components/admin/frame-collectors/frame-collector.routes';
import { FrameCollectorService } from 'app/components/admin/frame-collectors/frame-collector-service';
import { AddStoreComponent } from './add-store/add-store.component';
import { CustomMaterailModule } from 'app/custom-materail/custom-materail.module';
import { MatFormFieldModule } from '@angular/material';



@NgModule({
  declarations: [
    FrameCollectorMainComponent, 
    FrameCollectorListComponent, AddStoreComponent],
  imports: [
    CommonModule,
    FrameCollectorRoutingModule,
    CustomMaterailModule,
  ],
  providers: [
    FrameCollectorService
  ]
})
export class FrameCollectorModule { }
