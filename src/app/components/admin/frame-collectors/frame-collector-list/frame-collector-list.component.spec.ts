import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FrameCollectorListComponent } from './frame-collector-list.component';

describe('FrameCollectorListComponent', () => {
  let component: FrameCollectorListComponent;
  let fixture: ComponentFixture<FrameCollectorListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FrameCollectorListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FrameCollectorListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
