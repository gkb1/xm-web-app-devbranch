import { Component, OnInit, OnDestroy } from "@angular/core";
import { FrameCollectorService } from "../frame-collector-service";
import * as Constant from "../../../../constants/app.constants";
import { Subscription } from "rxjs/Subscription";
import { MatSelectChange } from "@angular/material";
import { AuthenticationService } from "app/components/authentication/authentication.service";
import { LoadingBarService } from "app/components/shared/loading-bar/loading-bar.service";
import { NotificationService } from "app/components/shared/notification/notification.service";
import * as _ from 'lodash';
import { ConfirmationService } from "app/components/shared/confirmation/confirmation.service";
import { BsModalRef, BsModalService } from "ngx-bootstrap";

@Component({
  selector: "app-frame-collector-list",
  templateUrl: "./frame-collector-list.component.html",
  styleUrls: ["./frame-collector-list.component.scss"]
})
export class FrameCollectorListComponent implements OnInit, OnDestroy {
  userDataList: any;
  httpFrameCollSubs: Subscription;
  payload = { conditions: {}, pagination: {} };
  localUserDetail: any;
  showTable: boolean;
  tableDataList: any;
  frameCollectorsDetails: any;
  filters = ['All', 'Approved', 'Rejected'];
  selected = 'All';
  toggleBind: any;
  selectedStoreID: any;
  filterFlag = false;
  httpUpdateFrameStatusSubs: Subscription;
  isSelectedAll = false;
  viewImageUrl: any;
  modalRef: BsModalRef;
  isLoggedInUserAdmin = false;
  isLoggedInUserECP = false;

  constructor(
    private frameCollectorService: FrameCollectorService,
    private authServices: AuthenticationService,
    private loader: LoadingBarService,
    private notify: NotificationService,
    private confirmation: ConfirmationService,
    private modalService: BsModalService
  ) {}

  ngOnInit() {
    this.isLoggedInUserAdmin = this.authServices.isLoggedInUserAdmin();
    this.isLoggedInUserECP = this.authServices.isLoggedInUserECPCustomer();
    this.localUserDetail = this.authServices.getAuthDeatils();
    if (this.isLoggedInUserAdmin) {
      this.showTable = false;
      this.payload['conditions']['role'] = Constant.ROLE_ECP_CUSTOMER;
      this.getUserList(this.payload);
    }
    if (this.isLoggedInUserECP) {
      this.showTable = true;
      const payload = { conditions: {} };
      payload['conditions']['store.ecpUserId'] = this.localUserDetail._id;
      this.selectedStoreID = this.localUserDetail._id;
      this.getFrameCollectorList(payload);
    }
  }

  storeSelection(event) {
    this.frameCollectorsDetails = [];
    this.selectedStoreID = event.value;
    const payload = { conditions: {} };
    payload['conditions']['store.ecpUserId'] = event.value;
    this.getFrameCollectorList(payload);
    this.showTable = true;
  }

  checkValue(event: any) {
    console.log(event);
  }

  filterBy(filter) {
    let load = { conditions: {} };
    if (filter === 'Approved') {
      load = { conditions: { filter: {} } };
      this.filterFlag = true;
      load.conditions['filter']['frameFilter'] = true;
    } else if (filter === 'Rejected') {
      load = { conditions: { filter: {} } };
      this.filterFlag = true;
      load.conditions['filter']['frameFilter'] = false;
    } else {
      this.filterFlag = false;
      load = { conditions: {} };
      load.conditions['store.ecpUserId'] = this.selectedStoreID;
    }
    this.getFrameCollectorList(load);
  }

  toggle(id, frameId, isApproved) {
    this.confirmation.showDialog('',
    status ? 'Are you sure ?' : 'Are you sure you want to ' + (isApproved ? 'REJECT' : 'APROVE') + '?')
    .subscribe(obj => {
      if (obj) {
        const payload = {
          ecpUserId: id,
          frameId: frameId,
          status: {}
        };
        if (isApproved) {
          payload.status['isApproved'] = false;
        } else {
          payload.status['isApproved'] = true;
        }
        this.updateFrameCollectorStatus(payload);
      }
    });
    
  }

  getUserList(payload) {
    this.httpFrameCollSubs = this.frameCollectorService
      .getAllUserListByCond(payload)
      .subscribe((res: any) => {
        if (res && res.data) {
          this.userDataList = res.data;
        }
      });
  }

  getFrameCollectorList(payload) {
    this.frameCollectorService
      .getFrameCollectorByStore(payload)
      .subscribe((res: any) => {
        this.tableDataList = res;
        // if (this.filterFlag) {
        //   this.tableDataList = res;
        //   _.forEach(this.tableDataList, (value, index) => {
        //     this.tableDataList["ecpUser"] = value.ecpUser[index];
        //     this.tableDataList["store"] = value.store;
        //     this.tableDataList["frameCollector"] = value.frameCollector[index];
        //   })
        //   console.log(this.tableDataList);
        // }
        res.forEach(storeList => {
          storeList.isSelected = false;
          this.frameCollectorsDetails = storeList.store.frame;
        });
      });
  }

  toggleSelectedAll() {
    this.frameCollectorsDetails.forEach(frame => {
      frame.isSelected = this.isSelectedAll;
    });
  }

  imagePopUpView(template, element) {
    this.viewImageUrl = element;
    this.modalRef = this.modalService.show(template);
  }

  reload() {
    this.tableDataList = [];
    this.frameCollectorsDetails = [];
    const listPayload = { conditions: {} };
    listPayload['conditions']['store.ecpUserId'] =  this.selectedStoreID;
    this.getFrameCollectorList(listPayload);
  }

  updateFrameCollectorStatus(payload) {
    this.loader.show();
    this.httpUpdateFrameStatusSubs = this.frameCollectorService.updateFrameCollectorStatus(payload).subscribe(
      (res: any) => {
        this.loader.hide();
        if (res) {
          this.notify.success('Updated Successfully !!', '');
          this.reload();
        }
      },
      error => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 409) {
          this.notify.error('Unable to update', body.message);
        } else {
          this.notify.error('Something went wrong', error.statusText);
        }
      }
    );
  }

  updateAllFrameStatusOfSelECP(status, message) {
    const payload = {
      ecpUserId: this.selectedStoreID,
      status: { isApproved : status }
    };
    this.confirmation.showDialog('Are you sure? ', 'Are you sure want to ' + message + ' Frames ?')
    .subscribe(obj => {
      if (obj) {
        this.loader.show();
        this.httpUpdateFrameStatusSubs = this.frameCollectorService.updateAllFrameStatusOfSelECP(payload).subscribe(
          (res: any) => {
            this.loader.hide();
            if (res) {
              this.notify.success('Updated Successfully !!', '');
              this.isSelectedAll = false;
              this.reload();
            }
          },
          error => {
            this.loader.hide();
            let body;
            if (error._body) {
              body = JSON.parse(error._body);
            }
            if (body.status === 409) {
              this.notify.error('Unable to update', body.message);
            } else {
              this.notify.error('Something went wrong', error.statusText);
            }
          }
        );
      } else {
        this.isSelectedAll = false;
      }
    });
  }

  ngOnDestroy() {
    if (this.httpFrameCollSubs) {
      this.httpFrameCollSubs.unsubscribe();
    }
    if (this.httpUpdateFrameStatusSubs) {
      this.httpUpdateFrameStatusSubs.unsubscribe();
    }
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }
}
