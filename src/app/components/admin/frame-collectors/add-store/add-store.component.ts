import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { MyErrorStateMatcher } from 'app/constants/errorStateMatcher';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { FrameCollectorService } from '../frame-collector-service';
import * as Constant from '../../../../constants/app.constants';
import { Subscription } from 'rxjs/Subscription';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';

@Component({
  selector: 'app-add-store',
  templateUrl: './add-store.component.html',
  styleUrls: ['./add-store.component.scss']
})
export class AddStoreComponent implements OnInit {
  addStoreForm : FormGroup;
  matcher: MyErrorStateMatcher;
  frameCollectorUserId: string;
  status: string;
  myStoreList: any;
  storeList: any = [];
  storeAlreadyExist: boolean;
  httpFrameCollSubs: Subscription;
  payload = { conditions: {}};
  frameCollectorName: any;
  ecpUserList: any;
  storeToBeDeleted: any;
  constructor(private formBuilder: FormBuilder,
    private notify: NotificationService,
    private route: ActivatedRoute,
    private router: Router,
    private frameCollectorService : FrameCollectorService,
    private loader: LoadingBarService) { }

  ngOnInit() {
    this. storeAlreadyExist =false;
    this.initAddStoreForm();
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      this.frameCollectorUserId = paramMap.get('collectorId');
      this.status = paramMap.get('status');
      this.addStoreForm.patchValue({
        name: paramMap.get('collectorName'),
      });
      this.addStoreForm.controls.name.disable();
        if(this.status == 'false') {
          this.payload['conditions']['role'] = Constant.ROLE_FRAME_COLLECTOR;
          this.httpFrameCollSubs =this.frameCollectorService.getAllUserListByCond(this.payload)
          .subscribe((res:any) => {
            if(res && res.data){
              res.data.forEach(list => {
                this.myStoreList =list.mystores
              });
            }
          });
        } else {
          this.payload['conditions']['role'] = Constant.ROLE_ECP_CUSTOMER;
          this.getList(this.payload);
        }
    })
  }
  getList(payload) {
    this.httpFrameCollSubs =this.frameCollectorService.getAllUserListByCond(payload)
    .subscribe((res:any) => {
      if(res && res.data){
          this.ecpUserList =res.data
      }
    });
  }
  initAddStoreForm() {
    this.matcher = new MyErrorStateMatcher();
    this.addStoreForm = this.formBuilder.group({
      name: ["", { validators: [Validators.required] }],
      store: ["", { validators: [Validators.required] }]
    });
  }
  storeSelection(event) {
    this.storeList.push({_id: event.ecpCust._id, cust_code: event.ecpCust.cust_code});
    // console.log('event.value;', event.value);
    // event.value.forEach(storeToBEAdded => {
      this.myStoreList.forEach(existingStore => {
        if(event.ecpCust.cust_code == existingStore.cust_code) {
          this.storeAlreadyExist = true;
        }
      });
    // });
  }
  addStore() {
    this.ecpUserList.forEach(element => {
      element
    });
    let payload = {
      frame_collector_userId: this.frameCollectorUserId,
      mystores: this.storeList
    }
    this.loader.show();
    this.frameCollectorService.addStoreToCollector(payload).subscribe((res:any)=>{
      console.log(res);
      if (res && res.data) {
        this.loader.hide();
        this.notify.success('Store Uploaded Successfully !!', '');
      }
    }, error => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 409) {
          this.notify.error("Unable to Add store", body.message);
        } else {
          this.notify.error("Something went wrong", error.statusText);
        }
    })
  }
  storeDelete(event){
    this.storeToBeDeleted = event.cust_code;
  }
  deleteStore() {
    console.log('form',this.addStoreForm.value);
    this.loader.show();
    this.frameCollectorService.deleteStoreToCollector(this.storeToBeDeleted,this.frameCollectorUserId).subscribe((res:any)=>{
      console.log("delete store",res);
      if (res && res.data) {
        this.loader.hide();
        this.notify.success('Store Deleted Successfully !!', '');
      }
    }, error => {
      this.loader.hide();
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 409) {
        this.notify.error("Unable to Delete store", body.message);
      } else {
        this.notify.error("Something went wrong", error.statusText);
      }
  })
  }
  ngOnDestroy() {
    if (this.httpFrameCollSubs) {
      this.httpFrameCollSubs.unsubscribe();
    }
  }
}