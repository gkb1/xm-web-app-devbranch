import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {

  rootCategoryList = [];
  httpGetAllCatSubs: Subscription;
  
  constructor( private categoryServices: CategoryService,) { }

  ngOnInit() {
    this.loadRootCatList();
  }
  loadRootCatList() {
    const payload = {
      conditions: { isRoot: true },
      pagination: {}
    }
    this.httpGetAllCatSubs = this.categoryServices.getAllCategoryList(payload).subscribe(res => {
      if (res && res.data.length > 0) {
        this.rootCategoryList = res.data;
      }
    });
  }
  deleteBrand(brandId) {
    console.log(brandId);
  }
  ngOnDestroy() {
    if (this.httpGetAllCatSubs) {
      this.httpGetAllCatSubs.unsubscribe();
    }
  }
}
