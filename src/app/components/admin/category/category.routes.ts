import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { CategoryMainComponent } from 'app/components/admin/category/category-main/category-main.component';
import { CategoryListComponent } from 'app/components/admin/category/category-list/category-list.component';
import { CategoryCreateUpdateComponent } from 'app/components/admin/category/category-create-update/category-create-update.component';

const categoryRoutes: Routes = [
    {
        path: '', canActivate: [AuthGuard], component: CategoryMainComponent,  children: [
            { path : '', component: CategoryListComponent },
            { path: 'create', component: CategoryCreateUpdateComponent},
            { path: ':categoryId', component: CategoryCreateUpdateComponent},
        ]
    },
   { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const CategoryRoutingModule = RouterModule.forChild(categoryRoutes);
