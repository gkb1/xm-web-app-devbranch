import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, FormArray } from "@angular/forms";
import { MyErrorStateMatcher } from "app/constants/errorStateMatcher";
import { CommonService } from "app/services/common.service";
import { LoadingBarService } from "app/components/shared/loading-bar/loading-bar.service";
import { CategoryService } from "../category.service";
import { NotificationService } from "app/components/shared/notification/notification.service";
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import * as _ from 'lodash';
import * as Constant from "../../../../constants/app.constants";

@Component({
  selector: "app-category-create-update",
  templateUrl: "./category-create-update.component.html",
  styleUrls: ["./category-create-update.component.scss"]
})
export class CategoryCreateUpdateComponent implements OnInit {
  categoryForm: FormGroup;
  matcher: MyErrorStateMatcher;
  isEditMode = false;
  categoryId: string;
  categoryName: string;
  childCatList = [];
  selCategory: any;
  categoryArr = [];
  constructor(
    private formBuilder: FormBuilder,
    private commonService: CommonService,
    private loader: LoadingBarService,
    private categoryServices: CategoryService,
    private notify: NotificationService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.categoryArr = Constant.PRODUCT_GENDER_LOOKUP;
    this.initCategoryForm();
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      this.categoryId = paramMap.get('categoryId');
      if (paramMap.has('categoryId')) {
        this.isEditMode = true;
        this.categoryServices.getCategoryByNameAndId(this.categoryId)
          .subscribe((res:any) => {
            this.categoryForm.patchValue({
              name: res.name,
            });
            // this.categoryForm.controls.name.disable();
            this.categoryForm.setControl('childCategories', this.setExistingCategory(res.childCategories));
          })
      } else {
        this.isEditMode = false;
        this.categoryId = null;
        this.categoryName = null;
      }
    })
  }
  setExistingCategory(category): FormArray {
    const formArray = new FormArray([]);
    category.forEach(cat => {
      formArray.push(this.formBuilder.group({
        name: cat._id.name
      }));
    });
    return formArray;
  }
  initCategoryForm() {
    this.matcher = new MyErrorStateMatcher();
    this.categoryForm = this.formBuilder.group({
      name: ["", { validators: [Validators.required] }],
      childCategories: this.formBuilder.array([this.createCategoryInfoForm()])
    });
  }
  createCategoryInfoForm() {
    return this.formBuilder.group({
      name: ["", [Validators.required]]
    });
  }
  addDynamicForm(value) {
    const control = <FormArray>this.categoryForm.controls[value];
    switch (value) {
      case "childCategories":
        return control.push(this.createCategoryInfoForm());
    }
  }
  removeDynamicForm(value, index) {
    const control = <FormArray>this.categoryForm.controls[value];
    return control.removeAt(index);
  }
  validateFormArray() {
    // childCategories form validation
    const control = <FormArray>this.categoryForm.controls["childCategories"];
    if (control && control.controls.length > 0) {
      for (let i = 0; i < control.controls.length; i++) {
        if (
          control.at(i)["controls"]["name"] &&
          !control.at(i)["controls"]["name"].valid
        ) {
          control.at(i)["controls"]["name"].markAsTouched();
        }
      }
    }
  }
  createCategory() {
    if (!this.isEditMode) {
      let payload = {
        name: this.categoryForm.value.name,
        childCategories: this.categoryForm.value.childCategories
      };
      if (!this.categoryForm.valid) {
        this.validateFormArray();
        this.commonService.validateAllFormFields(this.categoryForm);
        return false;
      }
      this.loader.show();
      this.categoryServices.getByCategory(payload).subscribe(
        (res: any) => {
          this.loader.hide();
          this.notify.error("Sucessfull", 'category cerated sucessfully');
        },
        error => {
          this.loader.hide();
          let body;
          if (error._body) {
            body = JSON.parse(error._body);
          }
          if (body.status === 409) {
            this.notify.error("Unable to submit category", body.message);
          } else {
            this.notify.error("Something went wrong", error.statusText);
          }
        }
      );
    }
  }
  SelectedCategory(category) {
    this.categoryForm.controls.name.setValue(category);
  }
  updateCategory() {
    console.log(this.categoryForm); 
  }
}
