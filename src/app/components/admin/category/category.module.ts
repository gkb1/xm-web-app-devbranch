import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryMainComponent } from './category-main/category-main.component';
import { CategoryListComponent } from './category-list/category-list.component';
import { CategoryCreateUpdateComponent } from './category-create-update/category-create-update.component';
import { CategoryService } from 'app/components/admin/category/category.service';
import { CategoryRoutingModule } from 'app/components/admin/category/category.routes';
import { CustomMaterailModule } from 'app/custom-materail/custom-materail.module';



@NgModule({
  declarations: [
    CategoryMainComponent, 
    CategoryListComponent, 
    CategoryCreateUpdateComponent],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    CustomMaterailModule
  ],
  providers: [
    CategoryService
  ]
})
export class CategoryModule { }
