import { Injectable } from '@angular/core';
import * as AppUtils from '../../../utils/app.utils';
import { Http } from '@angular/http';

@Injectable()
export class CategoryService {

  constructor(private http: Http) { }

  getByCategory(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.CREATE_CATEGORY, payload)
      .map(res => res.json());
  }
  getAllCategoryList(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_CATEGORIES_URL, payload)
    .map(res => res.json());
  }
  getCategoryByNameAndId (payload) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_CAT_BY_CATID_URL + `${payload}`)
      .map(res => res.json());
  }
}
