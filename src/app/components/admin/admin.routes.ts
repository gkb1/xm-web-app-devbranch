import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { AdminMainComponent } from 'app/components/admin/admin-main/admin-main.component';
import { HomeComponent } from 'app/components/admin/home/home.component';
import { RoleGuardService } from 'app/security/role-guard.service';
import * as Constant from './../../constants/app.constants';
import { FrameSearchComponent } from 'app/components/shared/frame-search/frame-search.component';

const adminRoutes: Routes = [
    {
        path: 'dashboard',  canActivate: [AuthGuard], component: AdminMainComponent,  children: [
            { path : '', component: HomeComponent },
            { path: 'users',  canActivate: [RoleGuardService],
                data: {
                    expectedRole: [Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN]
                },
                loadChildren: '../../components/admin/users/users.module#UsersModule'
            },
            { path: 'product', loadChildren: '../../components/admin/products/products.module#AdminProductsModule'},
            { path: 'brand',  canActivate: [RoleGuardService],
                data: {
                    expectedRole: [Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN, Constant.ROLE_FRAME_COLLECTOR ]
                },
                loadChildren: '../../components/admin/brand/brand.module#BrandModule'
            },
            { path: 'category',  canActivate: [RoleGuardService],
                data: {
                    expectedRole: [Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN]
                },
                loadChildren: '../../components/admin/category/category.module#CategoryModule'
            },
            { path: 'framecollectors',  canActivate: [RoleGuardService],
                data: {
                    expectedRole: [Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN, Constant.ROLE_ECP_CUSTOMER]
                },
                loadChildren: '../../components/admin/frame-collectors/frame-collector.module#FrameCollectorModule'
            },
            { path: 'frame-lookup',  canActivate: [RoleGuardService],
                data: {
                    expectedRole: [Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN, Constant.ROLE_FRAME_COLLECTOR]
                },
                loadChildren: '../../components/admin/frame-lookup/frame-lookup.module#FrameLookupModule'
            },
            { path: 'searchFrame', 
                data: {
                    expectedRole: [Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN]
                },
                component: FrameSearchComponent
            },
            { path: 'verifyImage', 
                data: {
                    expectedRole: [Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN]
                },
                loadChildren: '../../components/admin/upload-verify-img/upload-verify-img.module#UploadVerifyImgModule'
            },
        ]
    },
   { path: '**', redirectTo: 'dashboard', pathMatch: 'full' }
];

export const AdminRoutingModule = RouterModule.forChild(adminRoutes);
