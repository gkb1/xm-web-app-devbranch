import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminMainComponent } from './admin-main/admin-main.component';
import { HomeComponent } from './home/home.component';
import { AdminRoutingModule } from 'app/components/admin/admin.routes';
import { AdminService } from 'app/components/admin/admin.service';
import { MatSidenavModule, MatIconModule, MatListModule, MatToolbarModule, MatPaginatorModule, MatTableModule, MatSelectModule } from '@angular/material'
import {MatButtonModule} from '@angular/material/button';;
import { ModalModule, BsDropdownModule } from 'ngx-bootstrap';
import {MatTabsModule} from '@angular/material/tabs';
import { NavbarService } from 'app/components/shared/navbar.service';
import { SharedModule } from 'app/components/shared/shared.module';

@NgModule({
  declarations: [
    AdminMainComponent,
    HomeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    MatButtonModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    MatTabsModule,
    MatTableModule,
    MatPaginatorModule,
    MatSelectModule,
  ],
  exports: [
    MatTabsModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    MatButtonModule
  ],
  providers: [
    NavbarService,
    AdminService
  ]
})
export class AdminModule { }
