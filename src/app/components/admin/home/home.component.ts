import { Component, OnInit, HostBinding } from '@angular/core';
import { AuthenticationService } from 'app/components/authentication/authentication.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
}
