import { Injectable } from '@angular/core';
import * as AppUtils from '../../../utils/app.utils';
import { Http } from '@angular/http';
import { Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdminProductService {
  private instantUpdate$ = new Subject<void>();
  constructor(private http: Http) { }

  get instantUpdates$() {
    return this.instantUpdate$;
  }

  getAllProducts(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_PRODUCTS_URL, payload)
      .map(res => res.json());
  }

  approvereject(prodId, payload) {
    return this.http.put(AppUtils.BACKEND_API_ROOT_URL + AppUtils.APPROVAL_REJECT + prodId, payload)
      .pipe(tap((res) => { this.instantUpdate$.next() })
      );
  }

  getAllCategoryList(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_CATEGORIES_URL, payload)
      .map(res => res.json());
  }

  getBrandsByCat(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_BRANDS_BY_CONDN_URL, payload)
      .map(res => res.json());
  }

  productImageUpload(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.PRDT_IMAGE_UPLOAD_URL, payload)
      .map(res => res.json());
  }

  createProduct(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.CREATE_PRDT_URL, payload)
      .map(res => res.json());
  }

  getAllUserListByCond(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_ALL_USERS_LIST_BY_CONDN, payload)
      .map(res => res.json());
  }

  getSelProductDetailsByPrdtId(productId) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_SEL_PRDT_DETAILS_BY_PRDTID_URL + productId)
      .map(res => res.json());
  }

  getSelCatDetailsByCatId(categoryId) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_CAT_BY_CATID_URL + categoryId)
      .map(res => res.json());
  }

  updateSelProduct(prodId, payload) {
    return this.http.put(AppUtils.BACKEND_API_ROOT_URL + AppUtils.UPDATE_SEL_PRDTID_URL + prodId, payload)
      .map(res => res.json());
  }

  getSelPrdtDetailsByFrameIdSearch(frameId) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_SEL_PRDT_BY_FRAMEID_SEARCH_URL + frameId)
      .map(res => res.json());
  }

  addPrdtAttributeToStore(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.ADD_SEL_PRDT_ATTR_TO_STORE_URL, payload)
      .map(res => res.json());
  }

  removeSelStoreFromPrdtAttr(payload) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.REMOVE_SEL_STORE_PRDT_ATTR_URL, payload)
      .map(res => res.json());
  }

  getFrameMaterial(frameData) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_FRAME_INFO_URL + 'frameMaterial', frameData)
      .map(res => res.json());
  }
  getFrameType(frameData) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_FRAME_INFO_URL + 'frameType', frameData)
      .map(res => res.json());
  }
  getFrameShape(frameData) {
    return this.http.post(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_FRAME_INFO_URL + 'frameShape', frameData)
      .map(res => res.json());
  }
  getFrameSizes() {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_FRAME_INFO_URL + 'frameSizes')
      .map(res => res.json());
  }

  getSelPrdtDetailsByTagNoSearch(tagNo) {
    return this.http.get(AppUtils.BACKEND_API_ROOT_URL + AppUtils.GET_SEL_PRDT_BY_TAGNO_SEARCH_URL + tagNo)
      .map(res => res.json());
  }

}