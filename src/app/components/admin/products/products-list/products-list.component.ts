import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import { AdminProductService } from 'app/components/admin/products/product.service';
import { LoadingBarService } from 'app/components/shared/loading-bar/loading-bar.service';
import { ConfirmationService } from 'app/components/shared/confirmation/confirmation.service';
import { NotificationService } from 'app/components/shared/notification/notification.service';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import * as _ from 'lodash';
import { Router } from '@angular/router';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit, OnDestroy {
  page = 0;
  limit = 100;
  payload = { conditions: {}, pagination: {} }
  listView = false;
  productList = [];
  status: boolean;
  isEcpCustomer = false;
  isAdmin = false;
  isFrameCollector = false;
  filters = ['All', 'Approved', 'Rejected'];
  filtersForEcp = ['All', 'Active', 'De-active'];
  httpConfirmBeforeApproveReject: Subscription;
  httpGetAllPrdtsSubs: Subscription;
  productColourList = [];

  constructor(private router: Router,
    private productService: AdminProductService,
    private notify: NotificationService,
    private authService: AuthenticationService,
    private loader: LoadingBarService,
    private confirmation: ConfirmationService) { }

  ngOnInit() {
    this.isEcpCustomer = this.authService.isLoggedInUserECPCustomer();
    this.isAdmin = this.authService.isLoggedInUserAdmin();
    this.isFrameCollector = this.authService.isLoggedInUserFrameCollector();
    this.payload.pagination = { page: this.page, limit: this.limit };
    this.productService.instantUpdates$.subscribe(() => {
      this.loadProductList();
    })
    this.loadProductList();
  }

  loadProductList() {
    this.productList = [];
    this.productColourList = [];
    if (this.isEcpCustomer) {
      this.payload.conditions['createdEcpCustId'] = this.authService.getAuthDeatils()._id;
    }
    if (this.isFrameCollector) {
      this.payload.conditions['createdFrameCollectorId'] = this.authService.getAuthDeatils()._id;
    }
    this.httpGetAllPrdtsSubs = this.productService.getAllProducts(this.payload).subscribe(res => {
      if (res && res.data) {
        // this.productList = res.data;
        _.forEach(res.data, product => {
          const selColourDefaultCount = product.prdt_attributes.filter((v) => (v.colour.isDefault === true)).length;
          if (selColourDefaultCount <= 0) { // if no default colour
            this.setProductImageSizeByColour(product, product.prdt_attributes[0]);
          } else {
                _.forEach(product.prdt_attributes , attribute => {
                  this.productColourList.push(attribute.colour);
                  // setting default product size list
                  if (attribute.colour.isDefault) {
                    this.setProductImageSizeByColour(product, attribute);   
                  }
              });
          }
          this.productList.push(product);          
        });
      }
    }, (error) => {
      this.loader.hide();
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 406) {
        this.notify.error('Unable to load!!', body.message);
      } else {
        this.notify.error('Something went wrong', error.statusText);
      }
    });
  }

  approvePrdtByAdmin(data) {
    this.confirmation.showDialog('',
      status ? 'Are you sure ?' : 'Are you sure you want to ' + (data.isApproved ? 'Reject' : 'Approve') + ' ' + data.name + '?')
      .subscribe(obj => {
        if (obj) {
          const payload = { isApproved: !data.isApproved }
          this.httpConfirmBeforeApproveReject = this.productService.approvereject(data.productId, payload).subscribe((res: any) => {
            if (res) {
              res = res.json();
              this.notify.success('Success!!', res.message);
            } else {
              this.notify.error('Unable to Update Product status, Please try again later!!', '');
            }
          }, (error) => {
            this.loader.hide();
            let body;
            if (error._body) {
              body = JSON.parse(error._body);
            }
            if (body.status === 406 || body.status === 409) {
              this.notify.error('Unable to Update product status, Please try again later!!', body.message);
            } else {
              this.notify.error('Sorry, Something went wrong', error.statusText);
            }
          });
        }
      });
  }

  activateDeactivatePrdt(data) {
    this.confirmation.showDialog('',
      status ? 'Are you sure ?' : 'Are you sure you want to ' + (data.isActivate ? 'De-Activate' : 'Activate') + ' ' + data.name + '?')
      .subscribe(obj => {
        if (obj) {
          const payload = { isActivate: !data.isActivate }
          this.httpConfirmBeforeApproveReject = this.productService.approvereject(data.productId, payload).subscribe((res: any) => {
            if (res) {
              res = res.json();
              this.notify.success('Success!!', res.message);
            } else {
              this.notify.error('Unable to update product status, Please try again later!!', '');
            }
          }, (error) => {
            this.loader.hide();
            let body;
            if (error._body) {
              body = JSON.parse(error._body);
            }
            if (body.status === 406 || body.status === 409) {
              this.notify.error('Unable to Update product status, Please try again later!!', body.message);
            } else {
              this.notify.error('Sorry, Something went wrong', error.statusText);
            }
          });
        }
      });
  }

  filterBy(filter) {
    if (this.isAdmin) {
      if (filter === 'Approved') {
        this.payload.conditions['isApproved'] = true;
      } else if (filter === 'Rejected') {
        this.payload.conditions['isApproved'] = false;
      } else {
        this.payload.conditions = {};
      }
    } else if (this.isEcpCustomer) {
      if (filter === 'Active') {
        this.payload.conditions['isActivate'] = true;
      } else if (filter === 'De-active') {
        this.payload.conditions['isActivate'] = false;
      } else {
        this.payload.conditions = {};
      }
    }
    this.loadProductList();
  }

  togglegridListView() {
    this.listView = !this.listView;
  }

  setProductImageSizeByColour(selProduct, selAttribute) {
    // selected size list only
    selAttribute.colour.size = _.filter(selAttribute.colour.size, function (p) {
      return _.includes([true], p.isSelected);
    });
    if (selAttribute.colour) {
      selProduct.defaultPrdtSizeList = _.filter(selAttribute.colour.size, function (p) {
          return p;
      });
      // setting default product cover img by colour
      if (selAttribute.colour.cover_img) {
        selProduct.defaultCoverImgUrl = selAttribute.colour.cover_img.url;
      }
    }
  }

  navigateToPrdtCreatePage() {
    if (this.isAdmin) {
      this.router.navigate(['/admin/dashboard/product/create']);
    } else if (this.isEcpCustomer) {
      this.router.navigate(['/ecpcust/dashboard/product/create']);
    } else if (this.isFrameCollector) {
      this.router.navigate(['/framecollector/dashboard/product/fcCreate']);
    }
  }

  navigateToPrdtUpdatePage(selPrdt) {
    if (this.isAdmin) {
      this.router.navigate(['/admin/dashboard/product', selPrdt.productId, selPrdt.name]);
    } else if (this.isEcpCustomer) {
      this.router.navigate(['/ecpcust/dashboard/product', selPrdt.productId, selPrdt.name]);
    } else if (this.isFrameCollector) {
      this.router.navigate(['/framecollector/dashboard/product', selPrdt.productId, selPrdt.name]);
    }
  }

  ngOnDestroy() {
    if (this.httpConfirmBeforeApproveReject) {
      this.httpConfirmBeforeApproveReject.unsubscribe();
    }
    if (this.httpGetAllPrdtsSubs) {
      this.httpGetAllPrdtsSubs.unsubscribe();
    }
  }
}