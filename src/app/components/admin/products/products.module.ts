import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsMainComponent } from './products-main/products-main.component';
import { ProductCreateUpdateComponent } from './product-create-update/product-create-update.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { ProductRoutingModule } from 'app/components/admin/products/product.route';
import { AdminProductService } from 'app/components/admin/products/product.service';
import { FormsModule, ReactiveFormsModule, NgForm, NgModel } from '@angular/forms';
import { MaterialDesignElementsModule } from 'app/components/shared/material-design-element/material-design-element.module';
import { InputTrimModule } from 'ng2-trim-directive';
import { MatFormFieldModule, MatInputModule, MatTabsModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import {MatSelectModule} from '@angular/material/select';
// import { MaterialFileInputModule } from 'ngx-material-file-input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import { FrameLookupService } from '../frame-lookup/frame-lookup.service';
import { FcProductCreateComponent } from 'app/components/frame-collector/products/fc-product-create/fc-product-create.component';

@NgModule({
  declarations: [
    ProductsMainComponent,
    ProductCreateUpdateComponent,
    ProductsListComponent,
    FcProductCreateComponent
  ],
  imports: [
    CommonModule,
    ProductRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialDesignElementsModule,
    FormsModule,
    InputTrimModule,
    MatFormFieldModule,
    MatInputModule,
    HttpClientModule,
    MatSelectModule,
    // MaterialFileInputModule,
    MatIconModule,
    MatTabsModule,
    MatButtonModule
  ],
  exports: [
    MatInputModule, 
    MatSelectModule,
    // MaterialFileInputModule,
    MatTabsModule,
    MatIconModule,
    MatButtonModule,
    ProductsListComponent,
    ProductCreateUpdateComponent,
    FcProductCreateComponent
  ],
  providers: [
    AdminProductService,
    NgForm,
    NgModel,
    FrameLookupService
  ]
})
export class AdminProductsModule { }
