import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormBuilder, Validators, FormGroup, FormArray } from "@angular/forms";
import { MyErrorStateMatcher } from "app/constants/errorStateMatcher";
import * as Constant from '../../../../constants/app.constants';
import { CommonService } from "app/services/common.service";
import { Subscription } from "rxjs/Subscription";
import { AdminProductService } from "app/components/admin/products/product.service";
import * as _ from 'lodash';
import { NotificationService } from "app/components/shared/notification/notification.service";
import { LoadingBarService } from "app/components/shared/loading-bar/loading-bar.service";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationService } from "app/components/authentication/authentication.service";
import { MatTabChangeEvent } from '@angular/material';
import { isNullOrUndefined } from "util";
import { FrameLookupService } from '../../frame-lookup/frame-lookup.service';

@Component({
  selector: "app-product-create-update",
  templateUrl: "./product-create-update.component.html",
  styleUrls: ["./product-create-update.component.scss"]
})
export class ProductCreateUpdateComponent implements OnInit, OnDestroy {
  productForm: FormGroup;
  fileData: File = null;
  previewUrl: any = null;
  genderArr = Constant.PRODUCT_GENDER_LOOKUP;
  frameDetailArr;
  matcher: MyErrorStateMatcher;
  urls = [];
  // frameMaterialList = Constant.LOOK_UP_FRAME_MATERIAL;
  // frameShapeList = Constant.LOOK_UP_FRAME_SHAPE;
  // frameTypeList = Constant.LOOK_UP_FRAME_TYPE;
  prdtSizeList = Constant.LOOK_UP_SIZE;
  frameMaterialList = [];
  frameTypeList = [];
  frameShapeList = [];
  httpGetAllCatSubs: Subscription;
  selCategory;
  httpGetBrandsByCatSubs: Subscription;
  brandsList = [];
  httpCreatePrdtSubs: Subscription;
  httpPrdtImageUploadSubs: Subscription;
  isLoggedInUserAdmin = false;
  isLoggedInUserECP = false;
  storeUsersList = [];
  httpGetAllUserSubs: Subscription;
  isEditMode = false;
  selPrdtDetails;
  frameModelList = [];
  frameColourList = [];
  payload = { conditions: {}, pagination: {} };
  httpGetSelPrdtDetailsSubs: Subscription;
  httpUpdateSelPrdtSubs: Subscription;
  frameInfoSubscription: Subscription;
  httpGetAllFrameModel: Subscription;
  httpGetAllFrameColour: Subscription;
  numberOnlyRegexPattern = new RegExp(/^[0-9]*$/);
  paramData:any;
  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private productService: AdminProductService,
    private commonService: CommonService,
    private loader: LoadingBarService,
    private authService: AuthenticationService,
    private notify: NotificationService,
    private route: ActivatedRoute,
    private frameService: FrameLookupService) { }

  ngOnInit() {
    this.loadLookupFrameAPI();
    if (this.authService.isLoggedInUserAdmin() || this.authService.isLoggedInUserSuperAdmin()) {
      this.isLoggedInUserAdmin = true;
    }
    this.isLoggedInUserECP = this.authService.isLoggedInUserECPCustomer();
    this.route.params.subscribe((params) => {
      if (!isNullOrUndefined(params) && !isNullOrUndefined(params.productId)) {
        this.isEditMode = true;
        this.paramData=params;
        this.loadSelProductDetailsByPrdId(params.productId);
      }
    });
  }

  loadLookupFrameAPI() {
    // this.getFrameSizeinfo();
    this.getFrameMaterialinfo();
    this.getFrameShapeinfo();
    this.getFrameTypeinfo();
    this.initProductForm();
    this.getAllFrameModel();
    this.getAllFrameColours();
  }

  initProductForm() {
    this.matcher = new MyErrorStateMatcher();
    this.productForm = this.formBuilder.group({
      SKU: [''],
      name: ['', { validators: [Validators.required] }],
      gender: ['', { validators: [Validators.required] }],
      model: this.formBuilder.group({
        _id: ['', { validators: [Validators.required] }],
        name: [''],
        code: ['']
      }),
      brand: this.formBuilder.group({
        _id: ['', { validators: [Validators.required] }],
        name: [''],
        code: ['']
      }),
      description: ['', { validators: [Validators.required] }],
      createdEcpCustId: [''],
      createdFrameCollectorId: [''],
      category: this.formBuilder.group({
        _id: [''],
        categoryId: [''],
        childCategories: this.formBuilder.group({
          _id: ['', { validators: [Validators.required] }],
          categoryId: ['', { validators: [Validators.required] }],
        }),
      }),
      pricing: this.formBuilder.group({
        actualPrice: [''],
        sellingPrice: ['', [Validators.required, Validators.pattern(this.numberOnlyRegexPattern)]],
        gkbPrice: []
      }),
      product_info: this.formBuilder.array([this.createPrdtInfoForm()]),
      prdt_attributes: this.formBuilder.array([this.createPrdtAttrForm()]),
      frame_attributes: this.formBuilder.array([]),
    });
    this.setDefaultFrameFormValue();
    this.getAllSellersByCond();
  }

  loadSelProductDetailsByPrdId(productId) {
    this.selPrdtDetails = {};
    this.httpGetSelPrdtDetailsSubs = this.productService.getSelProductDetailsByPrdtId(productId).subscribe(res => {
      if (res && res.data) {
        this.selPrdtDetails = res.data;
        this.getSelCatDetailsByCatId(res.data.category.categoryId);
      }
    });
  }

  getAllFrameModel() {
    this.httpGetAllFrameModel = this.frameService.getFrameModelList(this.payload).subscribe(res => {
      if (res) {
        this.frameModelList = res;
      }
    });
  }

  getAllFrameColours() {
    this.httpGetAllFrameColour = this.frameService.getFrameColorList(this.payload).subscribe(res => {
      if (res) {
        this.frameColourList = res;
      }
    });
  }
  getSelCatDetailsByCatId(categoryId) {
    this.httpGetAllCatSubs = this.productService.getSelCatDetailsByCatId(categoryId).subscribe(res => {
      if (res && res.name) {
        const rootCatIndex = this.genderArr.map(function (x) { return x.toLowerCase(); }).indexOf(res.name.toLowerCase());
        if (rootCatIndex > -1) {
          this.productForm.patchValue({ gender: this.genderArr[rootCatIndex] });
          this.loadAllCategoriesByGender({ value: this.genderArr[rootCatIndex] });
        }
      }
    });
  }

  createPrdtAttrStoreForm() {
    return this.formBuilder.group({
      _id: [''],
      cust_code: [''],
      tagName: [''],
      store_code: [''],
      SKU: ['']
    });
  }

  setFormValues() {
    this.productForm.patchValue({
      name: this.selPrdtDetails.name,
      description: this.selPrdtDetails.description,
      createdEcpCustId: this.selPrdtDetails.createdEcpCustId,
      createdFrameCollectorId: this.selPrdtDetails.createdFrameCollectorId,
      SKU: this.selPrdtDetails.SKU,
      pricing: {
        actualPrice: this.selPrdtDetails.pricing.actualPrice,
        sellingPrice: this.selPrdtDetails.pricing.sellingPrice,
        gkbPrice: this.selPrdtDetails.pricing.gkbPrice
      }
    });
    const childCatIdIndex = this.selCategory.childCategories.map(
      function (x) { return x._id ? x._id.categoryId : null; }).indexOf(this.selPrdtDetails.category.childCategories.categoryId);
    if (childCatIdIndex > -1) { // set selected child category
      this.productForm.controls.category['controls']['childCategories']['controls']['_id']
        .setValue(this.selCategory.childCategories[childCatIdIndex]._id._id);
      this.productForm.controls.category['controls']['childCategories']['controls']['categoryId']
        .setValue(this.selPrdtDetails.category.childCategories.categoryId);
      this.loadBrandListByChildCat(this.selCategory.childCategories[childCatIdIndex]._id.name)
    }
    this.setModel({ value: this.selPrdtDetails.model._id });
    // set Product Info
    const control = <FormArray>this.productForm.controls['product_info'];
    if (this.selPrdtDetails.product_info && this.selPrdtDetails.product_info.length > 0) {
      if (control) {
        control.controls = [];
      }
      _.forEach(this.selPrdtDetails.product_info, (prdtInfo, index) => {
        control.push(this.createPrdtInfoForm());
        control['controls'][index]['controls']['_id'].setValue(prdtInfo._id);
        control['controls'][index]['controls']['heading'].setValue(prdtInfo.heading);
        control['controls'][index]['controls']['content']['controls']['key_name']
          .setValue(prdtInfo.content ? prdtInfo.content.key_name : '');
        control['controls'][index]['controls']['content']['controls']['key_value'].setValue(prdtInfo.content ? prdtInfo.content.key_value : '');
      });
    }
    // set default frame attributes
    const fcontrol = <FormArray>this.productForm.controls['frame_attributes'];
    if (this.selPrdtDetails.frame_attributes && this.selPrdtDetails.frame_attributes.length > 0) {
      if (fcontrol) {
        fcontrol.controls = [];
      }
      _.forEach(this.selPrdtDetails.frame_attributes, (frameAttr, index) => {
        fcontrol.push(this.createFrameForm('', index <= 2 ? true : false));
        fcontrol['controls'][index]['controls']['_id'].setValue(frameAttr._id);
        fcontrol['controls'][index]['controls']['key_name'].setValue(frameAttr.key_name);
        fcontrol['controls'][index]['controls']['key_value'].setValue(frameAttr.key_value);
        fcontrol['controls'][index]['controls']['digitCode'].setValue(frameAttr.digitCode);
      });
    }
    // set default product attributes
    const pcontrol = <FormArray>this.productForm.controls['prdt_attributes'];
    if (this.selPrdtDetails.prdt_attributes && this.selPrdtDetails.prdt_attributes.length > 0) {
      if (pcontrol) {
        pcontrol.controls = [];
      }
      _.forEach(this.selPrdtDetails.prdt_attributes, (prdAttr, index) => {
        pcontrol.push(this.createPrdtAttrForm());
        pcontrol['controls'][index]['controls']['_id'].setValue(prdAttr._id);
        pcontrol['controls'][index]['controls']['prdAttributeId'].setValue(prdAttr.prdAttributeId ? prdAttr.prdAttributeId : '');
        if (prdAttr.colour) {
          // tslint:disable-next-line:max-line-length
          pcontrol['controls'][index]['controls']['colour']['controls']['frameId'].setValue(prdAttr.colour.frameId ? prdAttr.colour.frameId : '');
          // set store
          if (prdAttr.colour.store && prdAttr.colour.store.length > 0) {
              const storeControl = pcontrol['controls'][index]['controls']['colour']['controls']['store'];
              _.forEach(prdAttr.colour.store, (store, storeIndex) => {
                if (store._id && store._id !== '') {
                  storeControl.push(this.createPrdtAttrStoreForm());
                  storeControl['controls'][storeIndex]['controls']['_id'].setValue(store._id);
                  storeControl['controls'][storeIndex]['controls']['cust_code'].setValue(store.cust_code);
                  storeControl['controls'][storeIndex]['controls']['tagName'].setValue(store.tagName);
                  storeControl['controls'][storeIndex]['controls']['store_code'].setValue(store.store_code);
                  storeControl['controls'][storeIndex]['controls']['SKU'].setValue(store.SKU);
                }
              })
          }
          pcontrol['controls'][index]['controls']['colour']['controls']['_id'].setValue(prdAttr.colour._id);
          pcontrol['controls'][index]['controls']['colour']['controls']['digitCode'].setValue(prdAttr.colour.digitCode);
          pcontrol['controls'][index]['controls']['colour']['controls']['prdColourId'].setValue(prdAttr.colour.prdColourId ? prdAttr.colour.prdColourId : '');
          pcontrol['controls'][index]['controls']['colour']['controls']['SKU'].setValue(prdAttr.colour.SKU);
          pcontrol['controls'][index]['controls']['colour']['controls']['value'].setValue(prdAttr.colour.value ? prdAttr.colour.value : '');
          pcontrol['controls'][index]['controls']['colour']['controls']['name'].setValue(prdAttr.colour.name ? prdAttr.colour.name : '');
          pcontrol['controls'][index]['controls']['colour']['controls']['isDefault'].setValue(prdAttr.colour.isDefault);
          pcontrol['controls'][index]['controls']['colour']['controls']['cover_img'].setValue(prdAttr.colour.cover_img);
          if (prdAttr.colour.media && prdAttr.colour.media.length > 0) {
            _.forEach(prdAttr.colour.media, (media, mediaIndex) => {
              if (media.url) {
                pcontrol['controls'][index]['controls']['colour']['controls']['media']['controls'][mediaIndex].setValue(media);
              }
            });
          }
          if (prdAttr.colour.size) {
            _.forEach(prdAttr.colour.size, (size, j) => {
              pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']['_id'].setValue(size._id);
              pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']
              ['prdSizeId'].setValue(size.prdSizeId);
              pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']
              ['quantity'].setValue(size.quantity);
              pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']
              ['sizeValue'].setValue(size.sizeValue);
              pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']['SKU'].setValue(size.SKU);
              pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']['isDefault']
                .setValue(size.isDefault);
              pcontrol['controls'][index]['controls']['colour']['controls']['size']['controls'][j]['controls']['isSelected']
                .setValue(size.isSelected);
            });
          }
        }
      });
    }
  }

  loadAllCategoriesByGender(selItem) {
    const payload = {
      conditions: {
        name: selItem.value
      }
    }
    this.httpGetAllCatSubs = this.productService.getAllCategoryList(payload).subscribe(res => {
      if (res && res.data.length > 0) {
        this.selCategory = res.data[0];
        // set root category
        this.productForm.controls.category['controls']['_id'].setValue(this.selCategory._id);
        this.productForm.controls.category['controls']['categoryId'].setValue(this.selCategory.categoryId);
        if (this.isEditMode) { // set form values
          this.setFormValues();
        }
      }
    })
  }

  getAllSellersByCond() { // ecp cust list
    const payload = { conditions: {} };
    payload['conditions']['role'] = Constant.ROLE_ECP_CUSTOMER;
    if (this.isLoggedInUserECP) {
      payload['conditions']['_id'] = this.authService.getAuthDeatils()._id;
    }
    this.httpGetAllUserSubs = this.productService.getAllUserListByCond(payload).subscribe(res => {
      if (res && res.data) {
        this.storeUsersList = res.data;
      }
    })
  }

  setChildCategory(selItem) {
    if (selItem && selItem.value) {
      const childCatIdIndex = this.selCategory.childCategories.map(
        function (x) { return x._id ? x._id.categoryId : null; }).indexOf(selItem.value);
      if (childCatIdIndex > -1) {  // set selected child category
        this.productForm.controls.category['controls']['childCategories']['controls']['_id']
          .setValue(this.selCategory.childCategories[childCatIdIndex]._id._id);
        this.loadBrandListByChildCat(this.selCategory.childCategories[childCatIdIndex]._id.name)
      }
    }
  }


  loadBrandListByChildCat(catName) {
    const payload = {
      conditions: {
        catName: catName
      },
      pagination: {}
    }
    this.httpGetBrandsByCatSubs = this.productService.getBrandsByCat(payload).subscribe(res => {
      if (res && res.data) {
        this.brandsList = res.data;
        if (this.isEditMode) { // set selected brand
          this.setBrand({ value: this.selPrdtDetails.brand._id });
        }
      }
    })
  }

  setBrand(selItem) {
    if (selItem && selItem.value) {
      const brandIdIndex = this.brandsList.map(
        function (x) { return x._id }).indexOf(selItem.value);
      if (brandIdIndex > -1) {  // set selected child category
        this.productForm.controls.brand['controls']['_id'].setValue(this.brandsList[brandIdIndex]._id);
        this.productForm.controls.brand['controls']['name'].setValue(this.brandsList[brandIdIndex].name);
        this.productForm.controls.brand['controls']['code'].setValue(this.brandsList[brandIdIndex].code);
      }
    }
  }

  setModel(selItem) {
    if (selItem && selItem.value) {
      const frameModelIndex = this.frameModelList.map(
        function (x) { return x._id }).indexOf(selItem.value);
      if (frameModelIndex > -1) {
        this.productForm.controls.model['controls']['_id'].setValue(this.frameModelList[frameModelIndex]._id);
        this.productForm.controls.model['controls']['name'].setValue(this.frameModelList[frameModelIndex].name);
        this.productForm.controls.model['controls']['code'].setValue(this.frameModelList[frameModelIndex].code);
      }
    }
  }

  createPrdtInfoForm() {
    return this.formBuilder.group({
      _id: [''],
      heading: [''],
      content: this.formBuilder.group({
        key_name: [''],
        key_value: ['']
      })
    });
  }

  createFrameForm(value, isDisabled) {
    return this.formBuilder.group({
      _id: [''],
      key_name: [{ value: value, disabled: isDisabled }, [Validators.required]],
      key_value: ['', [Validators.required]],
      digitCode: [''],
    });
  }

  setDefaultFrameFormValue() {
    const control = <FormArray>this.productForm.controls['frame_attributes'];
    if (control.controls.length <= 0) {
      control.push(this.createFrameForm('Frame Material', true));
      control.push(this.createFrameForm('Frame Type', true));
      control.push(this.createFrameForm('Frame Shape', true));
    }
  }

  addDynamicForm(value) {
    const control = <FormArray>this.productForm.controls[value];
    switch (value) {
      case 'frame_attributes':
        return control.push(this.createFrameForm('', false));
      case 'product_info':
        return control.push(this.createPrdtInfoForm());
      case 'prdt_attributes':
        if (control.controls.length <= 2) {
          return control.push(this.createPrdtAttrForm());
        }
    }
  }

  removeDynamicForm(value, index) {
    const control = <FormArray>this.productForm.controls[value];
    return control.removeAt(index);
  }

  // createPrdtAttrForm() {
  //   return this.formBuilder.group({
  //     size: this.formBuilder.group({
  //       value : ['', [Validators.required]],
  //       SKU : [''],
  //       colour: this.formBuilder.array([this.createPrdtAttrColourForm()])
  //     })
  //   });
  // }

  createStoreForm(selEcpID, selEcpCustCode) {
    return this.formBuilder.group({
      _id: [selEcpID, [Validators.required]],
      cust_code: [selEcpCustCode, [Validators.required]]
    });
  }

  createPrdtAttrForm() {
    return this.formBuilder.group({
      _id: [''],
      prdAttributeId: [''],
      colour: this.formBuilder.group({
        _id: [''],
        frameId: ['', [Validators.required]],
        store: this.formBuilder.array([]),
        prdColourId: [''],
        name: ['', [Validators.required]],
        value: ['', [Validators.required]],
        digitCode: [''],
        SKU: [''],
        isDefault: [false],
        cover_img: [''],
        media: this.formBuilder.array(this.createProductMediaForm()),
        size: this.formBuilder.array(this.createPrdtAttrColourForm())
      })
    });
  }

  createProductMediaForm() {
    const formGroup = [];
    for (let i = 0; i <= 3; i++) {
      formGroup.push({});
    }
    return formGroup;
  }

  // createPrdtAttrColourForm() {
  //   return this.formBuilder.group({
  //       value : ['', [Validators.required]],
  //       quantity: ['', [Validators.required]],
  //       SKU: [''] ,
  //       cover_img: ['', [Validators.required]],
  //       media: this.formBuilder.array([])
  //   });
  // }

  createPrdtAttrColourForm() {
    const formGroup = [];
    for (let i = 0; i < this.prdtSizeList.length; i++) {
      formGroup.push(this.formBuilder.group({
        _id: [],
        prdSizeId: [],
        value: [this.prdtSizeList[i].value],
        quantity: [0, [Validators.required, Validators.pattern(this.numberOnlyRegexPattern)]],
        sizeValue:[0, [Validators.required, Validators.pattern(this.numberOnlyRegexPattern)]],
        SKU: [''],
        isDefault: [false],
        isSelected: [false],
        digitCode: [this.prdtSizeList[i].code]
      }));
    }
    return formGroup;
  }

  setSelFrameDigitCode(keyName, index, keyValue) {
    const fcontrol = <FormArray>this.productForm.controls['frame_attributes'];
    let selFrameIndex;
    switch (keyName) {
      case 'Material':
        selFrameIndex = this.frameMaterialList.map(function (x) { return x.name; }).indexOf(keyValue);
        if (selFrameIndex > -1) {
          fcontrol.at(index)['controls']['digitCode'].setValue(this.frameMaterialList[selFrameIndex].code);
        }
        return;
      case 'Type':
        selFrameIndex = this.frameTypeList.map(function (x) { return x.name; }).indexOf(keyValue);
        if (selFrameIndex > -1) {
          fcontrol.at(index)['controls']['digitCode'].setValue(this.frameTypeList[selFrameIndex].code);
        }
        return;
      case 'Shape':
        selFrameIndex = this.frameShapeList.map(function (x) { return x.name; }).indexOf(keyValue);
        if (selFrameIndex > -1) {
          fcontrol.at(index)['controls']['digitCode'].setValue(this.frameShapeList[selFrameIndex].code);
        }
    }
  }

  setStoreToPrdtAttr(index, selItem) {
    if (selItem && (selItem.value ? (selItem.value.length > 0) : (selItem.length > 0))) {
      const control = <FormArray>this.productForm.controls['prdt_attributes'];
      const storeControl = control['controls'][index]['controls']['colour']['controls']['store'];
      if (storeControl) { // resetting controls
        storeControl.controls = [];
      }
      _.forEach(selItem.value, selValue => {
        storeControl.push(this.createStoreForm(selValue._id, selValue.ecpCust.cust_code));
      });
    }
  }

  setProductColourMedia(key, fileInput: any, index, mediaIndex) {
    if (fileInput && fileInput.target.files[0] && fileInput.target.files[0].type.indexOf('image/')>-1) {
      const control = <FormArray>this.productForm.controls['prdt_attributes'];
      if (control && control.controls.length > 0) {
        this.loader.show();
        const formData = new FormData();
        formData.append('content', fileInput.target.files[0]);
        this.httpPrdtImageUploadSubs = this.productService.productImageUpload(formData).subscribe(res => {
          if (res) {
            if (key === 'cover_img') {
              control['controls'][index]['controls']['colour']['controls'][key].setValue(res);
            }
            if (key === 'media') {
              control['controls'][index]['controls']['colour']['controls'][key]['controls'][mediaIndex].setValue(res);
            }
            this.notify.success('Image Uploaded Successfully !!', '');
            this.loader.hide();
          } else {
            this.loader.hide();
          }
        }, (error) => {
          this.loader.hide();
          let body;
          if (error._body) {
            body = JSON.parse(error._body);
          }
          if (body.status === 406) {
            this.notify.error('Unable to Upload Image!!', body.message);
          } else {
            this.notify.error('Something went wrong', error.statusText);
          }
        })
      }
    } else {
      this.notify.error('Unable to Upload', 'Upload only image file');
    }
  }

  // addDynamicPrdtSizeForm(parentIndex) {
  //   (<FormArray>(<FormGroup>
  //     this.productForm.controls['prdt_attributes']['controls']
  //     [parentIndex]).controls.colour['controls']['size']).push(this.createPrdtAttrColourForm());
  // }

  // removeDynamicPrdtSizeForm(parentIndex, childIndex) {
  //   (<FormArray>(<FormGroup>
  //     this.productForm.controls['prdt_attributes']['controls']
  //     [parentIndex]).controls.colour['controls']['size']).removeAt(childIndex);
  // }

  setProductSize(event, sizeVal, i, j) {
    const control = <FormArray>this.productForm.controls['prdt_attributes'];
    if (event.target.checked) {
      control['controls'][i]['controls']['colour']['controls']['size']['controls'][j]['controls']['isSelected'].setValue(true);
      control['controls'][i]['controls']['colour']['controls']['size']['controls'][j]['controls']['quantity'].markAsTouched();
      control['controls'][i]['controls']['colour']['controls']['size']['controls'][j]['controls']['sizeValue'].markAsTouched();
    } else {
      control['controls'][i]['controls']['colour']['controls']['size']['controls'][j]['controls']['isSelected'].setValue(false);
    }
  }

  setColour(i, selItem) {
    if (selItem && selItem.value) {
      const control = <FormArray>this.productForm.controls['prdt_attributes'];
      const hexCodeIndex = this.frameColourList.map(function (x) { return x.parentColour; }).indexOf(selItem.value);
      if (hexCodeIndex > -1) {
        control['controls'][i]['controls']['colour']['controls']['_id'].setValue(this.frameColourList[hexCodeIndex]._id);
        control['controls'][i]['controls']['colour']['controls']['name'].setValue(this.frameColourList[hexCodeIndex].actualColour);
        control['controls'][i]['controls']['colour']['controls']['digitCode'].setValue(this.frameColourList[hexCodeIndex].XMCode);
      }
    }
  }

  validateFormArray() {
    // product_info form validation
    // const control = <FormArray>this.productForm.controls['product_info'];
    // if (control && control.controls.length > 0) {
    //   for (let i = 0; i < control.controls.length; i++) {
    //     control.at(i)['controls']['content']['controls']['key_name'].clearValidators();
    //     control.at(i)['controls']['content']['controls']['key_value'].clearValidators();
    //     control.at(i)['controls']['content']['controls']['key_name'].updateValueAndValidity();
    //     control.at(i)['controls']['content']['controls']['key_value'].updateValueAndValidity();
    //   }
    // }
    // frame_attributes form validation
    const fcontrol = <FormArray>this.productForm.controls['frame_attributes'];
    if (fcontrol && fcontrol.controls.length > 0) {
      for (let i = 0; i < fcontrol.controls.length; i++) {
        if (fcontrol.at(i)['controls']['key_name'] &&
          !fcontrol.at(i)['controls']['key_name'].valid) {
          fcontrol.at(i)['controls']['key_name'].markAsTouched();
        }
        if (fcontrol.at(i)['controls']['key_value'] &&
          !fcontrol.at(i)['controls']['key_value'].valid) {
          fcontrol.at(i)['controls']['key_value'].markAsTouched();
        }
      }
    }
    // prdt_attributes form validation
    const pMainControl = <FormArray>this.productForm.controls['prdt_attributes'];
    if (pMainControl && pMainControl.controls.length > 0) {
      for (let i = 0; i < pMainControl.controls.length; i++) {
        if (pMainControl.at(i)['controls']['colour']) {
          if (!pMainControl.at(i)['controls']['colour']['controls']['value'] ||
            !pMainControl.at(i)['controls']['colour']['controls']['value'].valid) {
            pMainControl.at(i)['controls']['colour']['controls']['value'].markAsTouched();
          }
          if (!pMainControl.at(i)['controls']['colour']['controls']['frameId'] ||
            !pMainControl.at(i)['controls']['colour']['controls']['frameId'].valid) {
            pMainControl.at(i)['controls']['colour']['controls']['frameId'].markAsTouched();
          }
          for (let j = 0; j < pMainControl.at(i)['controls']['colour']['controls']['size'].controls.length; j++) {
            const sizeControl = pMainControl.at(i)['controls']['colour']['controls']['size'].controls;
            const selSizeCount = sizeControl.filter((v) => (v.controls.isSelected.value === true)).length;
            if (selSizeCount <= 0) {
              return this.notify.error('Please select atleast one size !!', '');
            }
            if (sizeControl[j]['controls']['value'] &&
              !sizeControl[j]['controls']['value'].valid) {
              sizeControl[j]['controls']['value'].markAsTouched();
            }
            if (sizeControl[j]['controls']['value'] && !sizeControl[j]['controls']['quantity']) {
              sizeControl[j]['controls']['quantity'].markAsTouched();
            }
            if (sizeControl[j]['controls']['value'] && !sizeControl[j]['controls']['sizeValue']) {
              sizeControl[j]['controls']['sizeValue'].markAsTouched();
            }
            // const storeControl = pMainControl.at(i)['controls']['size']['controls']['colour'].controls;
            // if (sizeControl[j]['controls']['store'] &&
            // !sizeControl[j]['controls']['store'].valid) {
            //   sizeControl[j]['controls']['store'].markAsTouched();
            // }
          }
        }
      }
    }
  }

  validatePrdtInfo() {
    const control = <FormArray>this.productForm.controls['product_info'];
    if (control && control.controls.length > 0) {
      for (let i = 0; i < control.controls.length; i++) {
       control.at(i)['controls']['content']['controls']['key_name'].clearValidators();
       control.at(i)['controls']['content']['controls']['key_value'].clearValidators();
       control.at(i)['controls']['content']['controls']['key_name'].updateValueAndValidity();
       control.at(i)['controls']['content']['controls']['key_value'].updateValueAndValidity();
        if (control.at(i)['controls']['content']['controls']['key_name'].value  || control.at(i)['controls']['content']['controls']['key_value'].value) {
           if (!control.at(i)['controls']['content']['controls']['key_name'].value) {
             control.at(i)['controls']['content']['controls']['key_name'].markAsTouched();
             control.at(i)['controls']['content']['controls']['key_name'].setValidators(Validators.required);
             control.at(i)['controls']['content']['controls']['key_name'].updateValueAndValidity();
             this.commonService.validateAllFormFields(this.productForm);
             return false;
           }
          if (!control.at(i)['controls']['content']['controls']['key_value'].value) {
            control.at(i)['controls']['content']['controls']['key_value'].markAsTouched();
            control.at(i)['controls']['content']['controls']['key_value'].setValidators(Validators.required);
            control.at(i)['controls']['content']['controls']['key_value'].updateValueAndValidity();
            this.commonService.validateAllFormFields(this.productForm);
            return false;
          }
        }
      }
    }
  }

  createProduct() {
    const formData = new FormData();
    formData.append('name', this.productForm.value.name);
    formData.append('description', this.productForm.value.description);
    formData.append('gender', this.productForm.value.gender);
    formData.append('model', JSON.stringify(this.productForm.value.model));
    formData.append('category', JSON.stringify(this.productForm.value.category));
    formData.append('brand', JSON.stringify(this.productForm.value.brand));
    formData.append('pricing', JSON.stringify(this.productForm.value.pricing));
    formData.append('product_info', JSON.stringify(this.productForm.value.product_info));
    formData.append('frame_attributes', JSON.stringify(this.productForm.getRawValue().frame_attributes));
    formData.append('prdt_attributes', JSON.stringify(this.productForm.value.prdt_attributes));
    if (this.isLoggedInUserECP) {
      formData.append('createdEcpCustId', this.authService.getAuthDeatils()._id);
    }
    if (this.authService.isLoggedInUserFrameCollector()) {
      formData.append('createdFrameCollectorId', this.authService.getAuthDeatils()._id);
    }
    // product_info form validation
    this.validatePrdtInfo();

    if (!this.productForm.valid) {
      const target = document.querySelector('.ng-invalid');
      if (target) {
        target.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
      }
      this.validateFormArray();
      this.commonService.validateAllFormFields(this.productForm);
      return false;
    }
    if (this.productForm.value.prdt_attributes.length <= 0) {
      return this.notify.warn('Please fill atleast one product colour with size details !!', '');
    }
    // prdt_attributes form validation for store
    const pMainControl = <FormArray>this.productForm.controls['prdt_attributes'];
    if (pMainControl && pMainControl.controls.length > 0) {
      for (let i = 0; i < pMainControl.controls.length; i++) {
        if (pMainControl.at(i)['controls']['colour']['controls']['value'].value) {
          // size validation
          if (pMainControl['controls'][i]['controls']['colour']['controls']['size'].controls.length > 0) {
            for (let j = 0; j < pMainControl['controls'][i]['controls']['colour']['controls']['size'].controls.length; j++) {
              const sizeControl = pMainControl['controls'][i]['controls']['colour']['controls']['size'].controls;
              const selSizeCount = sizeControl.filter((v) => (v.controls.isSelected.value === true)).length;
              if (selSizeCount <= 0) {
                return this.notify.error('Please select atleast one size !!',
                  pMainControl['controls'][i].value.colour.value);
              }
            }
          }
          // if (pMainControl.at(i)['controls']['colour']['controls']['store'].controls.length <= 0) {
          //   return this.notify.error('Please select atleast one store for',
          //   pMainControl.at(i)['controls']['colour']['controls']['value'].value);
          // }
        }
      }
    }
    this.loader.show();
    this.httpCreatePrdtSubs = this.productService.createProduct(formData).subscribe(res => {
      this.loader.hide();
      if (res) {
        this.productForm.reset();
        this.notify.success('Product Created SuccessFully !!', '');
        if (this.isLoggedInUserAdmin) {
          this.router.navigate(['/admin/dashboard/product']);
        }
        if (this.isLoggedInUserECP) {
          this.router.navigate(['/ecpcust/dashboard/product']);
        }
      }
    }, (error) => {
      this.loader.hide();
      let body;
      if (error._body) {
        body = JSON.parse(error._body);
      }
      if (body.status === 406) {
        this.notify.error('Unable to Create Product!!', body.message);
      } else {
        this.notify.error('Something went wrong', error.statusText);
      }
    })
  }

  updateProduct() {
    // product_info form validation
    this.validatePrdtInfo();
 
    if (!this.productForm.valid) {
      const target = document.querySelector('.ng-invalid');
      if (target) {
        target.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'nearest' });
      }
      this.validateFormArray();
      this.commonService.validateAllFormFields(this.productForm);
      return false;
    }
    if (this.productForm.value.prdt_attributes.length <= 0) {
      return this.notify.warn('Please fill atleast one product colour with size details !!', '');
    }
    // prdt_attributes form validation for store
    const pMainControl = <FormArray>this.productForm.controls['prdt_attributes'];
    if (pMainControl && pMainControl.controls.length > 0) {
      for (let i = 0; i < pMainControl.controls.length; i++) {
        if (pMainControl.at(i)['controls']['colour']['controls']['value'].value) {
          // size validation
          if (pMainControl['controls'][i]['controls']['colour']['controls']['size'].controls.length > 0) {
            for (let j = 0; j < pMainControl['controls'][i]['controls']['colour']['controls']['size'].controls.length; j++) {
              const sizeControl = pMainControl['controls'][i]['controls']['colour']['controls']['size'].controls;
              const selSizeCount = sizeControl.filter((v) => (v.controls.isSelected.value === true)).length;
              if (selSizeCount <= 0) {
                return this.notify.error('Please select atleast one size !!',
                  pMainControl['controls'][i].value.colour.value);
              }
            }
          }
          // if (pMainControl.at(i)['controls']['colour']['controls']['store'].controls.length <= 0) {
          //   return this.notify.error('Please select atleast one store for',
          //   pMainControl.at(i)['controls']['colour']['controls']['value'].value);
          // }
        }
      }
    }
    this.loader.show();
    this.httpUpdateSelPrdtSubs = this.productService.updateSelProduct(this.selPrdtDetails.productId,
      this.productForm.getRawValue()).subscribe(res => {
        this.loader.hide();
        if (res) {
          this.productForm.reset();
          this.notify.success('Product Updated SuccessFully !!', '');
          if (this.isLoggedInUserAdmin) {
            this.router.navigate(['/admin/dashboard/product']);
          }
          if (this.isLoggedInUserECP) {
            this.router.navigate(['/ecpcust/dashboard/product']);
          }
        }
      }, (error) => {
        this.loader.hide();
        let body;
        if (error._body) {
          body = JSON.parse(error._body);
        }
        if (body.status === 406) {
          this.notify.error('Unable to Create Product!!', body.message);
        } else {
          this.notify.error('Something went wrong', error.statusText);
        }
      })
  }

  // FRAME INFO material,shape,type
  getFrameMaterialinfo() {
    this.frameInfoSubscription = this.productService.getFrameMaterial(this.payload).subscribe(res => {
      if (res) {
        this.frameMaterialList = res;
      }
    })
  }

  getFrameShapeinfo() {
    this.frameInfoSubscription = this.productService.getFrameShape(this.payload).subscribe(res => {
      if (res) {
        this.frameShapeList = res;
      }
    })
  }

  getFrameTypeinfo() {
    this.frameInfoSubscription = this.productService.getFrameType(this.payload).subscribe(res => {
      if (res) {
        this.frameTypeList = res;
      }
    })
  }

  getFrameSizeinfo() {
    this.frameInfoSubscription = this.productService.getFrameSizes().subscribe(res => {
      if (res) {
        this.prdtSizeList = res;
      }
    })
  }

  acceptNumOnly(evt) {
    return this.commonService.acceptNumOnly(evt);
  }

  navigateToPrdtListPage() {
    if (this.isLoggedInUserAdmin) {
      this.router.navigate(['/admin/dashboard/product']);
    } else if (this.isLoggedInUserECP) {
      this.router.navigate(['/ecpcust/dashboard/product']);
    }
  }

  ngOnDestroy() {
    if (this.httpGetAllCatSubs) {
      this.httpGetAllCatSubs.unsubscribe();
    }
    if (this.httpGetBrandsByCatSubs) {
      this.httpGetBrandsByCatSubs.unsubscribe();
    }
    if (this.httpCreatePrdtSubs) {
      this.httpCreatePrdtSubs.unsubscribe();
    }
    if (this.httpPrdtImageUploadSubs) {
      this.httpPrdtImageUploadSubs.unsubscribe();
    }
    if (this.httpGetAllUserSubs) {
      this.httpGetAllUserSubs.unsubscribe();
    }
    if (this.httpGetSelPrdtDetailsSubs) {
      this.httpGetSelPrdtDetailsSubs.unsubscribe();
    }
    if (this.httpUpdateSelPrdtSubs) {
      this.httpUpdateSelPrdtSubs.unsubscribe();
    }
    if (this.frameInfoSubscription) {
      this.frameInfoSubscription.unsubscribe();
    }
    if (this.httpGetAllFrameModel) {
      this.httpGetAllFrameModel.unsubscribe();
    }

  }
  nextPage(){
    // console.log(this.paramData);
     this.router.navigate(['/framecollector/dashboard/brand/create'], {state:this.paramData});
   }
 
   addModelAndColour(){
     this.router.navigate(['/framecollector/dashboard/frame-lookup'], {state:this.paramData});
   }
}
