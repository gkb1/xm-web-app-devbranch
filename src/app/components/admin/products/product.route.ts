import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'app/security/auth.guard';
import { CustomerMainComponent } from 'app/components/customer/customer-main/customer-main.component';
import { ProductsMainComponent } from 'app/components/admin/products/products-main/products-main.component';
import { ProductsListComponent } from 'app/components/admin/products/products-list/products-list.component';
import { ProductCreateUpdateComponent } from 'app/components/admin/products/product-create-update/product-create-update.component';
import { FcProductCreateComponent } from 'app/components/frame-collector/products/fc-product-create/fc-product-create.component';
import * as Constant from './../../../constants/app.constants';

const productRoutes: Routes = [
    // canActivate: [AuthGuard]
    {
        path: '', component: ProductsMainComponent,  children: [
            { path : '', component: ProductsListComponent },
            { path: 'create', 
                data: {
                    expectedRole: [ Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN ]
                },
                component: ProductCreateUpdateComponent,
            },
            { path: ':productId/:productName', component: ProductCreateUpdateComponent},
            { path: 'fcCreate', component: FcProductCreateComponent }
        ]
    },
   { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const ProductRoutingModule = RouterModule.forChild(productRoutes);
