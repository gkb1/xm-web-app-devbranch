import {Routes, RouterModule} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import {AuthGuard} from './security/auth.guard';
import { HomeComponent } from 'app/components/customer/home/home.component';
import { ProductListComponent } from 'app/components/customer/products/product-list/product-list.component';
import * as Constant from './constants/app.constants';
import { RoleGuardService } from 'app/security/role-guard.service';
import { ProductDetailComponent } from 'app/components/customer/products/product-detail/product-detail.component';

export const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'collections/:category', component: ProductListComponent},
  { path: 'collections/:category/:productId/:productName', component: ProductDetailComponent},  
  { path: 'my', loadChildren: './components/customer/customer.module#CustomerModule'},
  { path: 'auth', loadChildren: './components/authentication/authentication.module#AuthenticationModule'},
  { path: 'admin',
    canActivate: [AuthGuard, RoleGuardService],
    data: {
        expectedRole: [Constant.ROLE_SUPER_ADMIN, Constant.ROLE_ADMIN]
    },
    loadChildren: './components/admin/admin.module#AdminModule'
  },
  { path: 'ecpcust',
    canActivate: [AuthGuard, RoleGuardService],
    data: {
        expectedRole: [Constant.ROLE_ECP_CUSTOMER]
    },
    loadChildren: './components/admin/admin.module#AdminModule'
  },
  { path: 'framecollector',
    canActivate: [AuthGuard, RoleGuardService],
    data: {
        expectedRole: [Constant.ROLE_FRAME_COLLECTOR]
    },
    loadChildren: './components/frame-collector/frame-collector.module#FrameCollectorModule'
  },
  { path: 'framemoderator',
    canActivate: [AuthGuard, RoleGuardService],
    data: {
        expectedRole: [Constant.ROLE_FRAME_MODERATOR]
    },
    loadChildren: './components/frame-moderator/frame-moderator.module#FrameModeratorModule'
  },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

export const RoutesModule: ModuleWithProviders = RouterModule.forRoot(appRoutes);
