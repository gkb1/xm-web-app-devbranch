import { Auth } from './../models/auth';
import { AuthenticationService } from 'app/components/authentication/authentication.service';
import { decode } from 'jwt-decode';
import {Injectable} from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { UserRole } from 'app/models/userRole';
import * as AppUtils from '../utils/app.utils';

@Injectable()
export class RoleGuardService implements CanActivate {
    userRole: string;
  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {
    // this will be passed from the route config
    // on the data property
    const expectedRole = route.data.expectedRole;
    // this.user = JSON.parse(localStorage.getItem('user'));
    // this.authService.userRoles.subscribe(role => {
    //     this.userRole = role;
    // })
    const user = this.authService.getAuthDeatils();
    this.userRole = user.role;
    // decode the token to get its payload
    // const tokenPayload = decode(token);
    if (
        !this.authService.loggedIn() ||
        expectedRole.indexOf(this.userRole) < 0
    ) {
        // console.log(expectedRole.indexOf(this.userRole))
        this.router.navigate(['/']);
        return false;
    } else {
        return true;
    }
  }
}
