import {CanDeactivate} from '@angular/router';
import { Observable } from 'rxjs/Observable';

// Its sample for future.
// TODO: components need to implement
export interface ComponentCanDeactivate {
  canDeactivate: () => boolean | Observable<boolean>;
}

export class DeactiveGaurd implements CanDeactivate<ComponentCanDeactivate> {
  canDeactivate(component: ComponentCanDeactivate): Observable<boolean> | boolean {
    return component.canDeactivate ? component.canDeactivate() : true;
  }
}
