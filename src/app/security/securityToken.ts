import * as _ from 'lodash';

export class SecurityToken {
  val1: string;
  val2: string;
  val3: string;
  val4: string;

  constructor(token: {val1: string, val2: string, val3: string, val4: string}) {
    _.assignIn(this, token);
  }

  isEncoding(encoding: string): boolean {
    return this.val2
      && this.val2 === encoding;
  }
}
